#!/bin/bash

chmod -R 777 var/

bin/console cache:clear
bin/console doctrine:database:drop --force

rm -R web/upload/files/invoices/*
rm -R web/upload/files/share/*
rm -R web/upload/images/header/*
rm -R web/upload/images/links/*
rm -R web/upload/images/products/*
rm -R web/upload/images/texts/*

bin/console doctrine:database:create
bin/console doctrine:schema:update --force
bin/console doctrine:fixtures:load

cp -R src/AppBundle/DataFixtures/web/*  web/
cp -R src/AppBundle/DataFixtures/vendor/* vendor/

chmod -R 777 var/
chmod -R 777 web/
chmod 777 app/config/parameters.yml



