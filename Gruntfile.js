'use strict';

module.exports = function(grunt) {

  var options = {
    pkg: grunt.file.readJSON('package.json'),

    paths: {
      templates: 'src/AppBundle/Resources/views',
      src: 'src/AppBundle/Resources/public',
      dist: 'web',
      bower: 'bower_components',
      temp: '.tmp'
    },

    devUrl: 'localhost/BSnet/LoveCreativity'
  };

  require('time-grunt')(grunt);

  require('load-grunt-config')(grunt, { config: options } );
};
