'use strict';

module.exports = {

  all: [
    'src/AppBundle/Controller/**/*.php',
    'src/AppBundle/Entity/**/*.php',
    'src/AppBundle/Form/**/*.php',
    'src/AppBundle/Resources/**/*.php',
    'src/AppBundle/Service/**/*.php',
    'src/AppBundle/Twig/**/*.php'
  ],
  options: {
    bin: 'bin/phpcs',
    standard: 'PSR2'
  }

};
