'use strict';

module.exports = {

  functional: {
    testsuite: 'functional'
  },
  unit: {
    testsuite: 'unit'
  },
  options: {
    configuration: 'phpunit.xml.dist',
    bin: 'php -d zend.enable_gc=0 bin/phpunit',
    colors: true,
    execMaxBuffer: 1000 * 1024
  }

};
