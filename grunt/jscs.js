'use strict';

module.exports = {

  src: [
    '<%= paths.src %>/front/js/*.js',
    '<%= paths.src %>/back/js/*.js'
  ],
  options: {
    config: '.jscsrc'
  }

};
