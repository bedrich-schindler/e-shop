'use strict';

module.exports = {

  options: {
    paths: ['<%= paths.bower %>'],
    sourceMap: true
  },
  front: {
    files: {
      '<%= paths.dist %>/css/front.css': '<%= paths.src %>/front/less/front.less'
    }
  },
  admin: {
    files: {
      '<%= paths.dist %>/css/back.css': '<%= paths.src %>/back/less/back.less'
    }
  }

};
