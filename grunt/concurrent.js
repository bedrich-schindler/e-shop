'use strict';

module.exports = {

  'test-php': [
    'phplint',
    'phpcs',
    'phpunit:unit',
    'phpunit:functional'
  ]

};
