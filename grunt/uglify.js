'use strict';

module.exports = {

  front: {
    files: {
      '<%= paths.dist %>/js/front.min.js': ['<%= paths.dist %>/js/front.js']
    }
  },
  admin: {
    files: {
      '<%= paths.dist %>/js/back.min.js': ['<%= paths.dist %>/js/back.js']
    }
  }

};
