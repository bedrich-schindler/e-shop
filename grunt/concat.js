'use strict';

module.exports = {

  options: {
    separator: ';',
    sourceMap: true
  },
  front: {
    src: [
      '<%= paths.bower %>/jquery/dist/jquery.js',
      '<%= paths.bower %>/moment/min/moment.min.js',
      '<%= paths.bower %>/moment/locale/cs.js',
      '<%= paths.bower %>/bootstrap/dist/js/bootstrap.js',
      '<%= paths.bower %>/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
      '<%= paths.bower %>/synergic-ui/src/js/confirmation.js',
      '<%= paths.bower %>/synergic-ui/src/js/disable.js',
      '<%= paths.bower %>/synergic-ui/src/js/filterable.js',
      '<%= paths.bower %>/synergic-ui/src/js/sortable-table.js',
      '<%= paths.bower %>/jquery-stickytabs/jquery.stickytabs.js',
      '<%= paths.bower %>/fancybox/source/jquery.fancybox.js',
      '<%= paths.bower %>/jquery.cookie/jquery.cookie.js',
      '<%= paths.src %>/front/js/front.js'
    ],
    dest: '<%= paths.dist %>/js/front.js'
  },
  admin: {
    src: [
      '<%= paths.bower %>/jquery/dist/jquery.js',
      '<%= paths.bower %>/moment/min/moment.min.js',
      '<%= paths.bower %>/moment/locale/cs.js',
      '<%= paths.bower %>/bootstrap/dist/js/bootstrap.js',
      '<%= paths.bower %>/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
      '<%= paths.bower %>/synergic-ui/src/js/confirmation.js',
      '<%= paths.bower %>/synergic-ui/src/js/disable.js',
      '<%= paths.bower %>/synergic-ui/src/js/filterable.js',
      '<%= paths.bower %>/synergic-ui/src/js/sortable-table.js',
      '<%= paths.bower %>/jquery-stickytabs/jquery.stickytabs.js',
      '<%= paths.bower %>/jqueryui/jquery-ui.js',
      '<%= paths.src %>/back/js/back.js'
    ],
    dest: '<%= paths.dist %>/js/back.js'
  }

};
