'use strict';

module.exports = {

  options: {
    restructuring: false
  },
  front: {
    files: {
      '<%= paths.dist %>/css/front.min.css': '<%= paths.dist %>/css/front.css'
    }
  },
  admin: {
    files: {
      '<%= paths.dist %>/css/back.min.css': '<%= paths.dist %>/css/back.css'
    }
  }

};
