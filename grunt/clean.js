'use strict';

module.exports = {

  css: '<%= paths.dist %>/css/*',
  js: '<%= paths.dist %>/js/*',
  images: '<%= paths.dist %>/images/*',
  temp: '<%= paths.temp %>/*'

};
