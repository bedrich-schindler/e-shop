'use strict';

module.exports = {

  // Tests
  'test-js': [
    'jshint',
    'jscs'
  ],

  'test-php': [
    'concurrent:test-php'
  ],

  'test': [
    'test-js',
    'test-php'
  ],

  // Build
  'build-css': [
    'clean:css',
    'clean:temp',
    'less',
    'autoprefixer',
    'cssmin'
  ],

  'build-js': [
    'test-js',
    'clean:js',
    'clean:temp',
    'concat',
    'uglify'
  ],

  'build-images': [
    'clean:images',
    'svgmin',
    'copy:images'
  ],

  'build': [
    'build-css',
    'build-js',
    'build-images',
    'copy:fonts',
    'copy:vendor'
  ],

  // Development
  'dev': [
    'test',
    'build',
    'browserSync',
    'watch'
  ],

  // Default task
  'default': [
    'test',
    'build'
  ]

};
