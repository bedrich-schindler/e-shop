'use strict';

module.exports = {

  front: {
    files: [{
      expand: true,
      cwd: '<%= paths.src %>/front/images/',
      src: ['*.svg'],
      dest: '<%= paths.dist %>/images/front/'
    }]
  },
  admin: {
    files: [{
      expand: true,
      cwd: '<%= paths.bower %>/back/images/',
      src: ['*.svg'],
      dest: '<%= paths.dist %>/images/back/'
    }]
  }

};
