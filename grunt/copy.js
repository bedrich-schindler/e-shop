'use strict';

module.exports = {

  fonts: {
    files: [{
      expand: true,
      cwd: '<%= paths.bower %>/bootstrap/dist/fonts/',
      src: ['*'],
      dest: '<%= paths.dist %>/fonts/'
    }]
  },
  images: {
    files: [{
      expand: true,
      cwd: '<%= paths.src %>/front/images/',
      src: ['**/*'],
      dest: '<%= paths.dist %>/images/'
    }]
  },
  vendor: {
    files: [{
      expand: true,
      cwd: '<%= paths.bower %>/ckeditor',
      src: ['**/*'],
      dest: '<%= paths.dist %>/vendor/ckeditor/'
    }, {
      expand: true,
      cwd: '<%= paths.bower %>/fancybox/source',
      src: ['**/*'],
      dest: '<%= paths.dist %>/vendor/fancybox/'
    }]
  }

};
