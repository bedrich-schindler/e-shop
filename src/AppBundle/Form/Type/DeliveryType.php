<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeliveryType extends AbstractType
{
    private $deliveryData;

    public function __construct($deliveryData)
    {
        $this->deliveryData = $deliveryData;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fee', 'number', [
                'label' => $this->deliveryData[$builder->getName()]->getMethod()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Delivery',
        ));
    }

    public function getName()
    {
        return 'appbundle_delivery';
    }
}
