<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SortingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('data', 'hidden')
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function getName()
    {
        return 'appbundle_sorting';
    }
}
