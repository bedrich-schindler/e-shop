<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GlobalConfigurationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paymentCollection', CollectionType::class, [
                'entry_type' => new PaymentType($builder->getData()->getPaymentCollection()),
                'label' => false,
                'entry_options' => [
                    'label' => false
                ]
            ])
            ->add('deliveryCollection', CollectionType::class, [
                'entry_type' => new DeliveryType($builder->getData()->getDeliveryCollection()),
                'label' => false,
                'entry_options' => [
                    'label' => false
                ]
            ])
            ->add('invoiceName', 'text', [
                'label' => 'Celé jméno na faktuře'
            ])
            ->add('invoiceStreet', 'text', [
                'label' => 'Ulice na faktuře'
            ])
            ->add('invoiceCity', 'text', [
                'label' => 'Město na faktuře'
            ])
            ->add('invoiceZipCode', 'number', [
                'label' => 'PSČ na faktuře'
            ])
            ->add('invoiceIc', 'text', [
                'label' => 'IČO na faktuře'
            ])
            ->add('invoiceBank', 'text', [
                'label' => 'Banka na faktuře'
            ])
            ->add('invoicePhone', 'text', [
                'label' => 'Telefon na faktuře'
            ])
            ->add('invoiceEmail', 'text', [
                'label' => 'Email na faktuře'
            ])
            ->add('invoiceText', 'textarea', [
                'label' => 'Text na faktuře',
                'required' => false
            ])
            ->add('headerImageFile', 'file', [
                'label' => 'Úvodní obrázek'
            ])
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\GlobalConfiguration',
        ));
    }

    public function getName()
    {
        return 'appbundle_global_configuration';
    }
}
