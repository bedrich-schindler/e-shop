<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DiscountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', 'text', [
                'label' => 'Slevový kód'
            ])
            ->add('discount', 'number', [
                'label' => 'Sleva'
            ])
            ->add('activeFrom', 'datetime', [
                'label' => 'Aktivní od',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm',
                'required' => false
            ])
            ->add('activeUntil', 'datetime', [
                'label' => 'Aktivní do',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd HH:mm',
                'required' => false
            ])
            ->add('maximumUse', 'number', [
                'label' => 'Limit použití',
                'required' => false
            ])
            ->add('currentUse', 'number', [
                'label' => 'Dočasné použití',
                'disabled' => true,
                'required' => false
            ]);

        if ($options['edit_action']) {
            $builder
                ->add('writtenOn', 'datetime', [
                    'label' => 'Vytvořeno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('writtenBy', 'entity', [
                    'label' => 'Vytvořil',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedOn', 'datetime', [
                    'label' => 'Upraveno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedBy', 'entity', [
                    'label' => 'Upravil',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ]);
        }

        $builder
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'edit_action' => false
            ]
        );
    }

    public function getName()
    {
        return 'appbundle_discount';
    }
}
