<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [
                'label' => 'Název',
                'attr' => [
                    'data-alias' => 'source'
                ]
            ])
            ->add('alias', 'text', [
                'label' => 'Alias',
                'attr' => [
                    'data-alias' => 'target'
                ]
            ])
            ->add('description', 'textarea', [
                'label' => 'Popis',
                'attr' => [
                    'class' => 'js-ckeditor',
                ]
            ]);

        if ($options['edit_action']) {
            $builder
                ->add('writtenOn', 'datetime', [
                    'label' => 'Vytvořeno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('writtenBy', 'entity', [
                    'label' => 'Vytvořil',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedOn', 'datetime', [
                    'label' => 'Upraveno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedBy', 'entity', [
                    'label' => 'Upravil',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ]);
        }

        $builder
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'edit_action' => false
            ]
        );
    }

    public function getName()
    {
        return 'appbundle_product_category';
    }
}
