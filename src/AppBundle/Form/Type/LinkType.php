<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LinkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', [
                'label' => 'Název'
            ])
            ->add('link', 'text', [
                'label' => 'Odkaz'
            ])
            ->add('file', 'file', [
                'label' => 'Obrázek'
            ]);

        if ($options['edit_action']) {
            $builder
                ->add('writtenOn', 'datetime', [
                    'label' => 'Napsáno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('writtenBy', 'entity', [
                    'label' => 'Napsal',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedOn', 'datetime', [
                    'label' => 'Upraveno',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('editedBy', 'entity', [
                    'label' => 'Upravil',
                    'class' => 'AppBundle:User',
                    'property' => 'getFullname',
                    'disabled' => true,
                    'required' => false
                ]);
        }

        $builder
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'validation_groups' => ['Default', 'FileRequiredAction'],
                'edit_action' => false
            ]
        );
    }

    public function getName()
    {
        return 'appbundle_link';
    }
}
