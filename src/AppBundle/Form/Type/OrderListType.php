<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OrderListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('deliveryFirstname', 'text', [
                'label' => 'Jméno'
            ])
            ->add('deliveryLastname', 'text', [
                'label' => 'Příjmení'
            ])
            ->add('deliveryStreet', 'text', [
                'label' => 'Ulice'
            ])
            ->add('deliveryCity', 'text', [
                'label' => 'Město'
            ])
            ->add('deliveryZipCode', 'text', [
                'label' => 'PSČ'
            ])
            ->add('deliveryEmail', 'text', [
                'label' => 'Email'
            ])
            ->add('deliveryPhone', 'text', [
                'label' => 'Telefonní číslo'
            ])
            ->add('remarks', 'textarea', [
                'label' => 'Poznámka',
                'required' => false
            ]);

        if ($options['front_action']) {
            $builder
                ->add('billingDifferent', 'checkbox', [
                    'label' => 'Zaškrtněte, pokud se fakturační údaje liší od dodací adresy'
                ]);
        } else {
            $builder
                ->add('billingDifferent', 'choice', [
                    'label' => 'Odlišné od dodacích údajů',
                    'choices'  => [
                        true => 'Ano',
                        false => 'Ne'
                    ]
                ]);
        }

        $builder
            ->add('billingFirstname', 'text', [
                'label' => 'Jméno'
            ])
            ->add('billingLastname', 'text', [
                'label' => 'Příjmení'
            ])
            ->add('billingStreet', 'text', [
                'label' => 'Ulice'
            ])
            ->add('billingCity', 'text', [
                'label' => 'Město'
            ])
            ->add('billingZipCode', 'text', [
                'label' => 'PSČ'
            ])
            ->add('billingEmail', 'text', [
                'label' => 'Email'
            ])
            ->add('billingPhone', 'text', [
                'label' => 'Telefonní číslo'
            ]);

        if ($options['front_action']) {
            $builder
                ->add('delivery', 'entity', [
                    'label' => 'Doprava',
                    'class' => 'AppBundle:Delivery',
                    'data_class' => 'AppBundle\Entity\Delivery',
                    'property' => 'method',
                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false
                ])
                ->add('deliveryDetail', 'choice', [
                    'label' => 'Pobočka uloženky',
                    'required' => false,
                    'choices' => $this->getUlozenkaBranches()
                ])
                ->add('payment', 'entity', [
                    'label' => 'Platba',
                    'class' => 'AppBundle:Payment',
                    'data_class' => 'AppBundle\Entity\Payment',
                    'property' => 'method',
                    'multiple' => false,
                    'expanded' => true,
                    'mapped' => false
                ])
                ->add('discount', 'text', [
                    'required' => false,
                    'mapped' => false
                ])
                ->add('newsletter', 'checkbox', [
                    'label' => 'Souhlasím se zasíláním reklamních newsletterů',
                    'required' => false
                ])
                ->add('termsAndConditions', 'checkbox', [
                    'label' => 'Souhlasím s obchodními podmínkami'
                ])
                ->add('add', 'submit', [
                    'label' => 'Odeslat objednávku',
                    'disabled' => true
                ]);
        } else {
            $builder
                ->add('discount', 'entity', [
                    'label' => 'Sleva',
                    'class' => 'AppBundle:Discount',
                    'property' => 'code',
                    'required' => false
                ])
                ->add('delivery', 'entity', [
                    'label' => 'Doprava',
                    'class' => 'AppBundle:Delivery',
                    'property' => 'method'
                ])
                ->add('deliveryDetail', 'choice', [
                    'label' => 'Pobočka uloženky',
                    'required' => false,
                    'choices' => $this->getUlozenkaBranches()
                ])
                ->add('payment', 'entity', [
                    'label' => 'Platba',
                    'class' => 'AppBundle:Payment',
                    'property' => 'method'
                ])
                ->add('active', 'choice', [
                    'label' => 'Aktivní',
                    'choices'  => [
                        true => 'Ano',
                        false => 'Ne'
                    ],
                    'disabled' => true
                ])
                ->add('status', 'choice', [
                    'label' => 'Stav',
                    'choices'  => [
                        'Created' => 'Vytvořeno',
                        'Received' => 'Přijato',
                        'Sent' => 'Odesláno',
                        'Finished' => 'Dokončeno',
                        'Canceled' => 'Stornováno'
                    ],
                    'required' => !$options['edit_action'],
                    'disabled' => true
                ])
                ->add('add', 'submit', [
                    'label' => 'Uložit'
                ]);
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'front_action' => false,
                'edit_action' => false
            ]
        );
    }

    private function getUlozenkaBranches()
    {
        $uri = 'https://api.ulozenka.cz/v2/branches?shopId=8722';
        $file = file_get_contents($uri);
        $json = json_decode($file);
        $branches = [];

        foreach ($json->data as $branch) {
            if ($branch->country === 'CZE') {
                $branches[$branch->name] = $branch->name;
            }
        }

        return $branches;
    }

    public function getName()
    {
        return 'appbundle_order_list';
    }
}
