<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file', [
                'label' => 'Soubor',
                'required' => true
            ])
            ->add('add', 'submit', [
                'label' => 'Nahrát soubor'
            ]);
    }

    public function getName()
    {
        return 'appbundle_file_upload';
    }
}
