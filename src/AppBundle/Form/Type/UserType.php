<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text', [
                'label' => 'Jméno'
            ])
            ->add('lastname', 'text', [
                'label' => 'Příjmení'
            ])
            ->add('email', 'email', [
                'label' => 'Email'
            ])
            ->add('username', 'text', [
                'label' => 'Uživatelské jméno',
                'disabled' => $options['edit_action'] || $options['account_action']
            ])
            ->add('plainPassword', 'repeated', [
                'type' => 'password',
                'first_options' => [
                    'label' => 'Heslo'
                ],
                'second_options' => [
                    'label' => 'Heslo znovu'
                ],
                'invalid_message' => 'Hesla se musí shodovat.',
                'required' => !$options['edit_action'] && !$options['account_action']
            ])
            ->add('groups', 'entity', [
                'label' => 'Skupina uživatelů',
                'class' => 'AppBundle:UserGroup',
                'property' => 'name',
                'disabled' => !$options['edit_action'] && $options['account_action']
            ]);

        if ($options['edit_action']) {
            $builder
                ->add('enabled', 'choice', [
                'label' => 'Stav účtu',
                'choices'  => [
                    true => 'Povolen',
                    false => 'Zakázán'
                ]
                ])
                ->add('lastLogin', 'datetime', [
                    'label' => 'Poslední přihlášení',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ])
                ->add('registeredAt', 'datetime', [
                    'label' => 'Registrace',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'disabled' => true,
                    'required' => false
                ]);
        }

        $builder
            ->add('add', 'submit', [
                'label' => 'Uložit'
            ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'validation_groups' => ['Default', 'PasswordRequiredAction'],
                'account_action' => false,
                'edit_action' => false
            ]
        );
    }

    public function getName()
    {
        return 'app_user';
    }
}
