<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', [
                'attr' => [
                    'placeholder' => '@'
                ]
            ])
            ->add('save', 'submit', [
                'label' => 'přihlásit'
            ]);
    }
    public function getName()
    {
        return 'appbundle_newsletter';
    }
}
