<?php

namespace AppBundle\Service;

use AppBundle\Entity\OrderItem;
use AppBundle\Entity\OrderList;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class OrderService
 * @package AppBundle\Service
 */
class OrderService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;
    /**
     * @var ProductService
     */
    private $ps;
    /**
     * @var DiscountService
     */
    private $ds;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     * @param ProductService $ps
     * @param DiscountService $ds
     */
    public function __construct(EntityManager $em, TokenStorage $ts, ProductService $ps, DiscountService $ds)
    {
        $this->em = $em;
        $this->ts = $ts;
        $this->ps = $ps;
        $this->ds = $ds;
    }

    /**
     * Find all order lists
     *
     * @return \AppBundle\Entity\OrderList[]|null
     */
    public function findOrdersAll()
    {
        return $this->em->getRepository('AppBundle:OrderList')->findAll();
    }

    /**
     * Find all order lists with status created
     *
     * @return \AppBundle\Entity\OrderList[]|null
     */
    public function findOrdersCreated()
    {
        return $this->em->getRepository('AppBundle:OrderList')->findBy(['status' => 'Created']);
    }

    /**
     * Find all order lists with status received
     *
     * @return \AppBundle\Entity\OrderList[]|null
     */
    public function findOrdersReceived()
    {
        return $this->em->getRepository('AppBundle:OrderList')->findBy(['status' => 'Received']);
    }

    /**
     * Find all order lists with status sent
     *
     * @return \AppBundle\Entity\OrderList[]|null
     */
    public function findOrdersSent()
    {
        return $this->em->getRepository('AppBundle:OrderList')->findBy(['status' => 'Sent']);
    }

    /**
     * Find all order lists with status finished
     *
     * @return \AppBundle\Entity\OrderList[]|null
     */
    public function findOrdersFinished()
    {
        return $this->em->getRepository('AppBundle:OrderList')->findBy(['status' => 'Finished']);
    }

    /**
     * Find order list by id
     *
     * @param $id
     * @return OrderList|null
     */
    public function findOrdersById($id)
    {
        return $this->em->getRepository('AppBundle:OrderList')->find($id);
    }

    /**
     * Find order by order number
     *
     * @param $orderNumber
     * @return OrderList|null
     */
    public function findOrderByNumber($orderNumber)
    {
        $id = substr($orderNumber, 5);

        return $this->em->getRepository('AppBundle:OrderList')->find($id);
    }

    /**
     * Find order item of order list
     *
     * @param OrderList $orderList
     * @return \AppBundle\Entity\OrderItem[]|null
     */
    public function findOrderItemsByOrder(OrderList $orderList)
    {
        return $this->em->getRepository('AppBundle:OrderItem')->findBy(['orderList' => $orderList]);
    }

    /**
     * Initialize new instance of order list
     *
     * @return OrderList
     */
    public function initialize()
    {
        return new OrderList();
    }

    /**
     * Add new order list
     *
     * @param OrderList $orderList
     * @return OrderList
     */
    public function add(OrderList $orderList)
    {
        $orderList->setOrderCreatedOn(new \DateTime());
        $orderList = $this->fillBilling($orderList);

        if ($orderList->getOrderDiscountItem()) {
            $orderList->getOrderDiscountItem()->getDiscountId()->increaseCurrentUse(1);
        }

        $this->em->persist($orderList);
        $this->em->flush();

        return $orderList;
    }

    /**
     * Edit order list
     *
     * @param OrderList $orderList
     * @param OrderList $oldOrderList
     * @return OrderList
     */
    public function edit(OrderList $orderList, OrderList $oldOrderList)
    {
        $orderList->setHandledBy($this->ts->getToken()->getUser());
        $orderList->setHandledOn(new \DateTime());
        $orderList = $this->fillBilling($orderList);

        if ($orderList->getOrderDiscountItem() !== $oldOrderList->getOrderDiscountItem()) {
            if ($oldOrderList->getOrderDiscountItem() && $oldOrderList->getOrderDiscountItem()->getDiscountId()) {
                $oldDiscount = $oldOrderList->getOrderDiscountItem()->getDiscountId()->decreaseCurrentUse(1);
                $this->em->persist($oldDiscount);
            }
            if ($orderList->getOrderDiscountItem() && $orderList->getOrderDiscountItem()->getDiscountId()) {
                $orderList->getOrderDiscountItem()->getDiscountId()->increaseCurrentUse(1);
            }
        }

        $this->em->persist($orderList);
        $this->em->flush();

        return $orderList;
    }

    /**
     * Add item list to order list
     *
     * @param OrderList $orderList
     * @param ArrayCollection $orderItemList
     */
    public function addItemList(OrderList $orderList, ArrayCollection $orderItemList)
    {
        foreach ($orderItemList as $orderItem) {
            $orderItem->setOrderList($orderList);

            if ($orderItem->getQuantity() > 0) {
                $product = $orderItem->getProduct()->decreaseQuantity($orderItem->getQuantity());

                $this->em->persist($product);
                $this->em->persist($orderItem);
            }
        }

        $this->em->flush();
    }

    public function createItemList($orderCookieObject)
    {
        $orderItemList = new ArrayCollection();

        foreach ($orderCookieObject->products as $productIndex => $productObject) {
            if ($productObject !== null && isset($productObject->id)) {
                $product = $this->ps->findById($productObject->id);

                if ($product) {
                    $orderItem = new OrderItem();
                    $orderItem->setProduct($product);
                    if (is_numeric($productObject->value)) {
                        if ($orderItem->getProduct()->getQuantity() > $productObject->value) {
                            $orderItem->setQuantity($productObject->value);
                        } else {
                            $orderItem->setQuantity($orderItem->getProduct()->getQuantity());
                        }
                    } else {
                        $orderItem->setQuantity(0);
                    }

                    $orderItemList->add($orderItem);
                }
            }
        }

        return $orderItemList;
    }

    /**
     * Load delivery from cookie and return object of delivery
     *
     * @param $orderCookieObject
     * @return \AppBundle\Entity\Delivery|null
     */
    public function loadDelivery($orderCookieObject)
    {
        if (isset($orderCookieObject->{'appbundle_order_list[delivery]'})) {
            $delivery = $this->em->getRepository('AppBundle:Delivery')
                ->find($orderCookieObject->{'appbundle_order_list[delivery]'});
            return $delivery;
        }
        return null;
    }

    /**
     * Load payment from cookie and return object of payment
     *
     * @param $orderCookieObject
     * @return \AppBundle\Entity\Payment|null
     */
    public function loadPayment($orderCookieObject)
    {
        if (isset($orderCookieObject->{'appbundle_order_list[payment]'})) {
            $payment = $this->em->getRepository('AppBundle:Payment')
                ->find($orderCookieObject->{'appbundle_order_list[payment]'});
            return $payment;
        }
        return null;
    }

    /**
     * Load discount from cookie and return object of discount
     *
     * @param $orderCookieObject
     * @return \AppBundle\Entity\Discount|null
     */
    public function loadDiscount($orderCookieObject)
    {
        if (isset($orderCookieObject->{'appbundle_order_list[discount]'})) {
            $discount = $this->ds->findValidbyId($orderCookieObject->{'appbundle_order_list[discount]'});
            return $discount;
        }
        return null;
    }

    public function handleAjaxRecalculate($orderCookieObject)
    {
        $orderItemList = $this->createItemList($orderCookieObject);

        $products = [];
        $productsTotalPrice = 0;
        $orderTotalPrice = 0;

        foreach ($orderItemList as $orderItem) {
            $products[$orderItem->getProduct()->getId()] = $orderItem->getTotalPrice();
            $productsTotalPrice += $orderItem->getTotalPrice();
            $orderTotalPrice += $orderItem->getTotalPrice();
        }

        $discount = $this->loadDiscount($orderCookieObject);
        if ($discount) {
            $orderTotalPrice = $orderTotalPrice * ((100 - $discount->getDiscount()) / 100);
        }

        $delivery = $this->loadDelivery($orderCookieObject);
        if ($delivery) {
            $orderTotalPrice += $delivery->getFee();
        }

        $payment = $this->loadPayment($orderCookieObject);
        if ($payment) {
            $orderTotalPrice += $payment->getFee();
        }

        $data = '{';
        $data .= '"products": ' . json_encode($products) . ',';
        $data .= '"productsTotalPrice": ' . $productsTotalPrice . ',';
        $data .= '"orderTotalPrice": ' . $orderTotalPrice . '';
        $data .= '}';

        return $data;
    }


    /**
     * Fill billing fields if billing information are not different from delivery information
     *
     * @param OrderList $orderList
     * @return OrderList
     */
    public function fillBilling(OrderList $orderList)
    {
        if (!$orderList->getBillingDifferent()) {
            $orderList->setBillingFirstname($orderList->getDeliveryFirstname());
            $orderList->setBillingLastname($orderList->getDeliveryLastname());
            $orderList->setBillingStreet($orderList->getDeliveryStreet());
            $orderList->setBillingCity($orderList->getDeliveryCity());
            $orderList->setBillingZipCode($orderList->getDeliveryZipCode());
            $orderList->setBillingEmail($orderList->getDeliveryEmail());
            $orderList->setBillingPhone($orderList->getDeliveryPhone());
        }

        return $orderList;
    }

    /**
     * Set order list status to received
     *
     * @param OrderList $orderList
     * @return OrderList
     */
    public function setStatusReceived(OrderList $orderList)
    {
        $orderList->setHandledBy($this->ts->getToken()->getUser());
        $orderList->setHandledOn(new \DateTime());
        $orderList->setOrderReceivedOn(new \DateTime());
        $orderList->setStatus('Received');

        $this->em->persist($orderList);
        $this->em->flush();

        return $orderList;
    }

    /**
     * Set order list status to sent
     *
     * @param OrderList $orderList
     * @return OrderList
     */
    public function setStatusSent(OrderList $orderList)
    {
        $orderList->setHandledBy($this->ts->getToken()->getUser());
        $orderList->setHandledOn(new \DateTime());
        $orderList->setActive(false);
        $orderList->setOrderSentOn(new \DateTime());
        $orderList->setStatus('Sent');

        $this->em->persist($orderList);
        $this->em->flush();

        return $orderList;
    }

    /**
     * Set order list status to finished
     *
     * @param OrderList $orderList
     * @return OrderList
     */
    public function setStatusFinished(OrderList $orderList)
    {
        $orderList->setHandledBy($this->ts->getToken()->getUser());
        $orderList->setHandledOn(new \DateTime());
        $orderList->setActive(false);
        $orderList->setOrderFinishedOn(new \DateTime());
        $orderList->setStatus('Finished');

        $this->em->persist($orderList);
        $this->em->flush();

        return $orderList;
    }

    /**
     * Set order list status to canceled
     *
     * @param OrderList $orderList
     * @param bool|false $emailCancel
     * @return OrderList
     */
    public function setStatusCanceled(OrderList $orderList, $emailCancel = false)
    {
        if ($orderList->getStatus() != 'Canceled') {
            if ($this->ts->getToken()->getUser() && $this->ts->getToken()->getUsername() != 'anon.') {
                $orderList->setHandledBy($this->ts->getToken()->getUser());
                $orderList->setHandledOn(new \DateTime());
            }
            if ($emailCancel) {
                $orderList->setEmailCanceledOn(new \DateTime());
            }

            if ($orderList->getOrderDiscountItem()) {
                if ($orderList->getOrderDiscountItem()->getDiscountId()) {
                    $orderList->getOrderDiscountItem()->getDiscountId()->decreaseCurrentUse(1);
                }
            }

            $orderList->setActive(false);
            $orderList->setOrderCanceledOn(new \DateTime());
            $orderList->setStatus('Canceled');

            foreach ($orderList->getOrderItem() as $orderItem) {
                if ($orderItem->getQuantity() > 0 || $orderItem->getProduct()) {
                    if ($orderItem->getProduct()) {
                        $product = $orderItem->getProduct()->increaseQuantity($orderItem->getQuantity());
                        $this->em->persist($product);
                    }
                }
            }

            $this->em->persist($orderList);
            $this->em->flush();
        }

        return $orderList;
    }

    /**
     * Encode email to secret string
     *
     * @see http://php.net/manual/en/function.mcrypt-encrypt.php Code used from first example
     * @param $orderNumber
     * @return string
     */
    public function encodeOrderNumber($orderNumber)
    {
        $key = pack('H*', 'bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $orderNumber, MCRYPT_MODE_CBC, $iv);
        $ciphertext = $iv . $ciphertext;
        $ciphertext_base64 = base64_encode($ciphertext);

        $ciphertext_base64 = strtr($ciphertext_base64, "+/", "-_");

        return $ciphertext_base64;
    }

    /**
     * Decode email from secret string
     *
     * @see http://php.net/manual/en/function.mcrypt-encrypt.php Code used from first example
     * @param $ciphertext_base64
     * @return int|string
     */
    public function decodeOrderNumber($ciphertext_base64)
    {
        $ciphertext_base64 = strtr($ciphertext_base64, "-_", "+/");

        $key = pack('H*', 'bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $ciphertext_dec = base64_decode($ciphertext_base64);

        $iv_dec = substr($ciphertext_dec, 0, $iv_size);
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);

        if ($ciphertext_dec !== false) {
            $orderNumber = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
        } else {
            $orderNumber = 0;
        }

        return $orderNumber;
    }

    /**
     * Get opened order lists of last month (order lists which are not canceled)
     *
     * @return OrderList[]|null
     */
    public function orderOpenedThisMonth()
    {
        $qb = $this->em->getRepository('AppBundle:OrderList')
            ->createQueryBuilder('o');

        return $qb->select('o')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->gte('o.orderCreatedOn', ':orderCreatedOn'),
                    $qb->expr()->neq('o.status', ':status')
                )
            )
            ->setParameter('orderCreatedOn', (new \DateTime())->modify('-1 month')->format('Y-m-d'))
            ->setParameter('status', 'Canceled')
            ->getQuery()
            ->getResult();
    }

    /**
     * Get amount of order lists of last month
     *
     * @return integer
     */
    public function ordersOpenedCountThisMonth()
    {
        return count($this->orderOpenedThisMonth());
    }

    /**
     * Get total turnover of order lists of last month
     *
     * @return integer
     */
    public function ordersTotalTurnoverThisMonth()
    {
        $turnover = 0;

        foreach ($this->orderOpenedThisMonth() as $order) {
            $turnover += $order->getTotalPrice();
        }

        return $turnover;
    }

    /**
     * Get average turnover of order lists of last month
     *
     * @return float|integer
     */
    public function ordersAverageTurnoverThisMonth()
    {
        if ($this->ordersOpenedCountThisMonth() !== 0) {
            return $this->ordersTotalTurnoverThisMonth() / $this->ordersOpenedCountThisMonth();
        } else {
            return 0;
        }
    }

    /**
     * Get opened order lists (order lists which are not canceled)
     *
     * @return OrderList[]|null
     */
    public function orderOpened()
    {
        $qb = $this->em->getRepository('AppBundle:OrderList')
            ->createQueryBuilder('o');

        return $qb->select('o')
            ->where($qb->expr()->neq('o.status', ':status'))
            ->setParameter('status', 'Canceled')
            ->getQuery()
            ->getResult();
    }

    /**
     * Get amount of order lists
     *
     * @return integer
     */
    public function ordersOpenedCount()
    {
        return count($this->orderOpened());
    }

    /**
     * Get total turnover of order lists
     *
     * @return integer
     */
    public function ordersTotalTurnover()
    {
        $turnover = 0;

        foreach ($this->orderOpened() as $order) {
            $turnover += $order->getTotalPrice();
        }

        return $turnover;
    }

    /**
     * Get average turnover of order lists
     *
     * @return float|integer
     */
    public function ordersAverageTurnover()
    {
        if ($this->ordersOpenedCount() !== 0) {
            return $this->ordersTotalTurnover() / $this->ordersOpenedCount();
        } else {
            return 0;
        }
    }
}
