<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Entity\UserManager;
use FOS\UserBundle\Util\CanonicalizerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class UserService
 * @package AppBundle\Service
 */
class UserService extends UserManager
{
    /**
     * @param EncoderFactoryInterface $encoderFactory
     * @param CanonicalizerInterface $usernameCanonicalizer
     * @param CanonicalizerInterface $emailCanonicalizer
     * @param EntityManager $objectManager
     * @param string $user
     */
    public function __construct(
        EncoderFactoryInterface $encoderFactory,
        CanonicalizerInterface $usernameCanonicalizer,
        CanonicalizerInterface $emailCanonicalizer,
        EntityManager $objectManager,
        $user
    ) {
        parent::__construct(
            $encoderFactory,
            $usernameCanonicalizer,
            $emailCanonicalizer,
            $objectManager,
            $user
        );
    }
}
