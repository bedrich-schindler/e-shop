<?php

namespace AppBundle\Service;

use AppBundle\Entity\Text;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class TextService
 * @package AppBundle\Service
 */
class TextService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all texts
     *
     * @return \AppBundle\Entity\Text[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Text')->findAll();
    }

    /**
     * Find text by id
     *
     * @param $id
     * @return Text|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Text')->find($id);
    }

    /**
     * Initialize new instance of text
     *
     * @return Text
     */
    public function initialize()
    {
        return new Text();
    }

    /**
     * Add new text
     *
     * @param Text $text
     * @return Text
     */
    public function add(Text $text)
    {
        $text->setWrittenBy($this->ts->getToken()->getUser());
        $text->setWrittenOn(new \DateTime());

        $this->em->persist($text);
        $this->em->flush();

        return $text;
    }

    /**
     * Edit existing text
     *
     * @param Text $text
     * @return Text
     */
    public function edit(Text $text)
    {
        $text->setEditedBy($this->ts->getToken()->getUser());
        $text->setEditedOn(new \DateTime());

        $this->em->persist($text);
        $this->em->flush();

        return $text;
    }

    /**
     * Remove existing text
     *
     * @param Text $text
     */
    public function remove(Text $text)
    {
        $this->em->remove($text);
        $this->em->flush();
    }
}
