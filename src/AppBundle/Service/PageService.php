<?php

namespace AppBundle\Service;

use AppBundle\Entity\Page;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class PageService
 * @package AppBundle\Service
 */
class PageService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all pages
     *
     * @return \AppBundle\Entity\Page[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Page')->findAll();
    }

    /**
     * Find page by id
     *
     * @param $id
     * @return Page|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Page')->find($id);
    }

    /**
     * Find page by alias
     *
     * @param $alias
     * @return Page|null
     */
    public function findByAlias($alias)
    {
        return $this->em->getRepository('AppBundle:Page')->findOneBy(['alias' => $alias]);
    }

    /**
     * Initialize new instance of page
     *
     * @return Page
     */
    public function initialize()
    {
        return new Page();
    }

    /**
     * Add new page
     *
     * @param Page $page
     * @return Page
     */
    public function add(Page $page)
    {
        $page->setWrittenBy($this->ts->getToken()->getUser());
        $page->setWrittenOn(new \DateTime());

        $this->em->persist($page);
        $this->em->flush();

        return $page;
    }

    /**
     * Edit existing page
     *
     * @param Page $page
     * @return Page
     */
    public function edit(Page $page)
    {
        $page->setEditedBy($this->ts->getToken()->getUser());
        $page->setEditedOn(new \DateTime());

        $this->em->persist($page);
        $this->em->flush();

        return $page;
    }

    /**
     * Remove existing page
     *
     * @param Page $page
     */
    public function remove(Page $page)
    {
        $this->em->remove($page);
        $this->em->flush();
    }
}
