<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class ProductService
 * @package AppBundle\Service
 */
class ProductService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all products
     *
     * @return \AppBundle\Entity\Product[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Product')->findAll();
    }

    /**
     * Find all favourite products
     *
     * @return \AppBundle\Entity\Product[]|null
     */
    public function findAllFavourite()
    {
        return $this->em->getRepository('AppBundle:Product')->findBy(['favourite' => true]);
    }

    /**
     * Find product by id
     *
     * @param $id
     * @return Product|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Product')->find($id);
    }

    /**
     * Initialize new instance of product
     *
     * @return Product
     */
    public function initialize()
    {
        return new Product();
    }

    /**
     * Add new product
     *
     * @param Product $product
     * @return Product
     */
    public function add(Product $product)
    {
        $product->setWrittenBy($this->ts->getToken()->getUser());
        $product->setWrittenOn(new \DateTime());

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * Edit existing product
     *
     * @param Product $product
     * @return Product
     */
    public function edit(Product $product)
    {
        $product->setEditedBy($this->ts->getToken()->getUser());
        $product->setEditedOn(new \DateTime());

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * Mark existing product as favourite
     *
     * @param Product $product
     * @return Product
     */
    public function markFavourite(Product $product)
    {
        $product->setFavourite(true);

        $product->setEditedBy($this->ts->getToken()->getUser());
        $product->setEditedOn(new \DateTime());

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * Unmark existing product as favourite
     *
     * @param Product $product
     * @return Product
     */
    public function unmarkFavourite(Product $product)
    {
        $product->setFavourite(false);

        $product->setEditedBy($this->ts->getToken()->getUser());
        $product->setEditedOn(new \DateTime());

        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * Remove existing product
     *
     * @param Product $product
     */
    public function remove(Product $product)
    {
        $this->em->remove($product);
        $this->em->flush();
    }
}
