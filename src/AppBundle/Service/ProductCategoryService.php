<?php

namespace AppBundle\Service;

use AppBundle\Entity\ProductCategory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class ProductCategoryService
 * @package AppBundle\Service
 */
class ProductCategoryService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all product categories
     *
     * @return \AppBundle\Entity\ProductCategory[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:ProductCategory')->findAll();
    }

    /**
     * Find all product categories sorted by sorting table
     *
     * \AppBundle\Entity\ProductCategory[]|null
     */
    public function findAllSorted()
    {
        return $this->em->getRepository('AppBundle:ProductCategory')
            ->createQueryBuilder('p')
            ->join('p.sorting', 's')
            ->addSelect('s')
            ->orderBy('s.sort')
            ->getQuery()
            ->getResult();
    }

    /**
     * Find product category by id
     *
     * @param $id
     * @return ProductCategory|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:ProductCategory')->find($id);
    }

    /**
     * Initialize new instance of product category
     *
     * @return ProductCategory
     */
    public function initialize()
    {
        return new ProductCategory();
    }

    /**
     * Add new product category
     *
     * @param ProductCategory $productCategory
     * @return ProductCategory
     */
    public function add(ProductCategory $productCategory)
    {
        $productCategory->setWrittenBy($this->ts->getToken()->getUser());
        $productCategory->setWrittenOn(new \DateTime());

        $this->em->persist($productCategory);
        $this->em->flush();

        return $productCategory;
    }

    /**
     * Edit existing product category
     *
     * @param ProductCategory $productCategory
     * @return ProductCategory
     */
    public function edit(ProductCategory $productCategory)
    {
        $productCategory->setEditedBy($this->ts->getToken()->getUser());
        $productCategory->setEditedOn(new \DateTime());

        $this->em->persist($productCategory);
        $this->em->flush();

        return $productCategory;
    }

    /**
     * Remove existing product category
     *
     * @param ProductCategory $productCategory
     */
    public function remove(ProductCategory $productCategory)
    {
        $this->em->remove($productCategory);
        $this->em->flush();
    }
}
