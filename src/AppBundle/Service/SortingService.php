<?php

namespace AppBundle\Service;

use AppBundle\Entity\ProductCategory;
use AppBundle\Entity\Sorting;
use AppBundle\Entity\Text;
use Doctrine\ORM\EntityManager;

/**
 * Class SortingService
 * @package AppBundle\Service
 */
class SortingService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Find all sorted product categories and text
     *
     * @return \AppBundle\Entity\ProductCategory[]|\AppBundle\Entity\Text[]|mixed[]
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Sorting')
            ->createQueryBuilder('s')
            ->select('s')
            ->leftJoin('s.text', 't')
            ->addSelect('t')
            ->leftJoin('s.productCategory', 'p')
            ->addSelect('p')
            ->orderBy('s.sort', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Add new text to sorting table
     *
     * @param Text $text
     * @return Sorting
     */
    public function addText(Text $text)
    {
        $sorting = new Sorting();
        $sorting->setSort($this->nextSortNumber());
        $sorting->setText($text);

        $this->em->persist($sorting);
        $this->em->flush();

        return $sorting;
    }

    /**
     * Add new text to sorting table
     *
     * @param ProductCategory $productCategory
     * @return Sorting
     */
    public function addProductCategory(ProductCategory $productCategory)
    {
        $sorting = new Sorting();
        $sorting->setSort($this->nextSortNumber());
        $sorting->setProductCategory($productCategory);

        $this->em->persist($sorting);
        $this->em->flush();

        return $sorting;
    }

    /**
     * Change sorting list
     *
     * @param $sorting
     */
    public function changeSorting($sorting)
    {
        $results = $this->em->getRepository('AppBundle:Sorting')->findAll();

        foreach ($results as $result) {
            if ($sorting[$result->getId()]) {
                $result->setSort($sorting[$result->getId()]);
                $this->em->persist($result);
            }
        }

        $this->em->flush();
    }

    /**
     * Get next sort number
     *
     * @return integer
     */
    private function nextSortNumber()
    {
        $result = $this->em->getRepository('AppBundle:Sorting')
            ->createQueryBuilder('s')
            ->select('MAX(s.sort) AS maxSortNumber')
            ->getQuery()
            ->getScalarResult();

        if (!$result[0]['maxSortNumber']) {
            return 1;
        } else {
            return $result[0]['maxSortNumber'] + 1;
        }
    }
}
