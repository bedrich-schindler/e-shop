<?php

namespace AppBundle\Service;

use AppBundle\Entity\Link;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class LinkService
 * @package AppBundle\Service
 */
class LinkService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all links
     *
     * @return \AppBundle\Entity\Link[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Link')->findAll();
    }

    /**
     * Find link by id
     *
     * @param $id
     * @return Link|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Link')->find($id);
    }

    /**
     * Initialize new instance of link
     *
     * @return Link
     */
    public function initialize()
    {
        return new Link();
    }

    /**
     * Add new link
     *
     * @param Link $link
     * @return Link
     */
    public function add(Link $link)
    {
        $link->setWrittenBy($this->ts->getToken()->getUser());
        $link->setWrittenOn(new \DateTime());

        $this->em->persist($link);
        $this->em->flush();

        return $link;
    }

    /**
     * Edit existing link
     *
     * @param Link $link
     * @return Link
     */
    public function edit(Link $link)
    {
        $link->setEditedBy($this->ts->getToken()->getUser());
        $link->setEditedOn(new \DateTime());

        $this->em->persist($link);
        $this->em->flush();

        return $link;
    }

    /**
     * Remove existing link
     *
     * @param Link $link
     */
    public function remove(Link $link)
    {
        $this->em->remove($link);
        $this->em->flush();
    }
}
