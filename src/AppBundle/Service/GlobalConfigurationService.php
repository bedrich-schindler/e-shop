<?php

namespace AppBundle\Service;

use AppBundle\Entity\GlobalConfiguration;
use Doctrine\ORM\EntityManager;

/**
 * Class GlobalConfigurationService
 * @package AppBundle\Service
 */
class GlobalConfigurationService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Initialize new instance of global configuration
     *
     * @return GlobalConfiguration
     */
    public function initialize()
    {
        return new GlobalConfiguration();
    }

    /**
     * Get global configuration
     *
     * @return GlobalConfiguration|null
     */
    public function get()
    {
        // Use id 1 because of the only one global configuration
        return $this->em->getRepository('AppBundle:GlobalConfiguration')->find(1);
    }

    /**
     * Set global configuration
     *
     * @param GlobalConfiguration $globalConfiguration
     * @return GlobalConfiguration
     */
    public function set(GlobalConfiguration $globalConfiguration)
    {
        // Use id 1 because of the only one global configuration
        $globalConfiguration->setId(1);
        $this->em->persist($globalConfiguration);
        $this->em->flush();

        return $globalConfiguration;
    }

    /**
     * Get collection of payments
     *
     * @return \AppBundle\Entity\Payment[]|null
     */
    public function getPaymentCollection()
    {
        return $this->em->getRepository('AppBundle:Payment')->findAll();
    }

    /**
     * Get collection of deliveries
     *
     * @return \AppBundle\Entity\Delivery[]|null
     */
    public function getDeliveryCollection()
    {
        return $this->em->getRepository('AppBundle:Delivery')->findAll();
    }
}
