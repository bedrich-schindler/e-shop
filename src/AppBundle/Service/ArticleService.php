<?php

namespace AppBundle\Service;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class ArticleService
 * @package AppBundle\Service
 */
class ArticleService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all articles
     *
     * @return \AppBundle\Entity\Article[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Article')->findAll();
    }

    /**
     * Find article which is marked as default
     *
     * @return Article|null
     */
    public function findDefault()
    {
        return $this->em->getRepository('AppBundle:Article')->findOneBy(['isDefault' => true]);
    }

    /**
     * Find article by id
     *
     * @param $id
     * @return Article|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Article')->find($id);
    }

    /**
     * Initialize new instance of article
     *
     * @return Article
     */
    public function initialize()
    {
        return new Article();
    }

    /**
     * Add new article
     *
     * @param Article $article
     * @return Article
     */
    public function add(Article $article)
    {
        $article->setWrittenBy($this->ts->getToken()->getUser());
        $article->setWrittenOn(new \DateTime());

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    /**
     * Edit existing article
     *
     * @param Article $article
     * @return Article
     */
    public function edit(Article $article)
    {
        $article->setEditedBy($this->ts->getToken()->getUser());
        $article->setEditedOn(new \DateTime());

        $this->em->persist($article);
        $this->em->flush();

        return $article;
    }

    /**
     * Mark existing article as default and unmark another default articles
     *
     * @param Article $article
     */
    public function markDefault(Article $article)
    {
        // TODO: Change only default articles instead of all articles; Use editedBy/On
        $this->em->createQueryBuilder()
            ->update('AppBundle:Article', 'a')
            ->set('a.isDefault', 'false')
            ->getQuery()
            ->execute();

        $article->setIsDefault(true);

        $this->em->persist($article);
        $this->em->flush();
    }

    /**
     * Unmark existing article as default
     *
     * @param Article $article
     */
    public function unmarkDefault(Article $article)
    {
        // TODO:  Use editedBy/On
        $article->setIsDefault(false);

        $this->em->persist($article);
        $this->em->flush();
    }

    /**
     * Remove existing article
     *
     * @param Article $article
     */
    public function remove(Article $article)
    {
        $this->em->remove($article);
        $this->em->flush();
    }
}
