<?php

namespace AppBundle\Service;

use AppBundle\Entity\Newsletter;
use Doctrine\ORM\EntityManager;

/**
 * Class NewsletterService
 * @package AppBundle\Service
 */
class NewsletterService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Find all newsletter addresses
     *
     * @return \AppBundle\Entity\Newsletter[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Newsletter')->findAll();
    }

    /**
     * Find newsletter address by id
     *
     * @param $id
     * @return Newsletter|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Newsletter')->find($id);
    }

    /**
     * Find newsletter address by email
     *
     * @param $email
     * @return Newsletter|null
     */
    public function findByEmail($email)
    {
        return $this->em->getRepository('AppBundle:Newsletter')->findOneBy(['email' => $email]);
    }

    /**
     * Initialize new instance of newsletter address
     *
     * @return Newsletter
     */
    public function initialize()
    {
        return new Newsletter();
    }

    /**
     * Add new newsletter address
     *
     * @param Newsletter $newsletter
     * @return Newsletter
     */
    public function add(Newsletter $newsletter)
    {
        $newsletter->setAddedOn(new \DateTime());

        $this->em->persist($newsletter);
        $this->em->flush();

        return $newsletter;
    }

    /**
     * Remove existing newsletter address
     *
     * @param Newsletter $newsletter
     */
    public function remove(Newsletter $newsletter)
    {
        $this->em->remove($newsletter);
        $this->em->flush();
    }

    /**
     * Encode email to secret string
     *
     * @see http://php.net/manual/en/function.mcrypt-encrypt.php Code used from first example
     * @param $email
     * @return string
     */
    public function encodeEmail($email)
    {
        $key = pack('H*', 'bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);

        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $email, MCRYPT_MODE_CBC, $iv);
        $ciphertext = $iv . $ciphertext;
        $ciphertext_base64 = base64_encode($ciphertext);

        $ciphertext_base64 = strtr($ciphertext_base64, "+/", "-_");

        return $ciphertext_base64;
    }

    /**
     * Decode email from secret string
     *
     * @see http://php.net/manual/en/function.mcrypt-encrypt.php Code used from first example
     * @param $ciphertext_base64
     * @return int|string
     */
    public function decodeEmail($ciphertext_base64)
    {
        $ciphertext_base64 = strtr($ciphertext_base64, "-_", "+/");

        $key = pack('H*', 'bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3');

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

        $ciphertext_dec = base64_decode($ciphertext_base64);

        $iv_dec = substr($ciphertext_dec, 0, $iv_size);
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);

        if ($ciphertext_dec !== false) {
            $email = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);
        } else {
            $email = 0;
        }

        return $email;
    }
}
