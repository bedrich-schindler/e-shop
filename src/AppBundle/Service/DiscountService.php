<?php

namespace AppBundle\Service;

use AppBundle\Entity\Discount;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class DiscountService
 * @package AppBundle\Service
 */
class DiscountService
{
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var TokenStorage
     */
    private $ts;

    /**
     * Constructor
     *
     * @param EntityManager $em
     * @param TokenStorage $ts
     */
    public function __construct(EntityManager $em, TokenStorage $ts)
    {
        $this->em = $em;
        $this->ts = $ts;
    }

    /**
     * Find all discounts
     *
     * @return \AppBundle\Entity\Discount[]|null
     */
    public function findAll()
    {
        return $this->em->getRepository('AppBundle:Discount')->findAll();
    }

    /**
     * Find discount by id
     *
     * @param $id
     * @return Discount|null
     */
    public function findById($id)
    {
        return $this->em->getRepository('AppBundle:Discount')->find($id);
    }

    /**
     * Find discount by discount code
     *
     * @param $code
     * @return Discount|null
     */
    public function findByCode($code)
    {
        return $this->em->getRepository('AppBundle:Discount')->findOneBy(['code' => $code]);
    }

    /**
     * Find valid and discount by id
     *
     * @param $id
     * @return Discount|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findValidById($id)
    {
        $qb = $this->em->getRepository('AppBundle:Discount')
            ->createQueryBuilder('d');

        return $qb->select('d')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('d.id', ':id'),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.maximumUse'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.maximumUse'),
                            $qb->expr()->gt('d.maximumUse', 'd.currentUse')
                        )
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.activeFrom'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.activeFrom'),
                            $qb->expr()->lt('d.activeFrom', ':now')
                        )
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.activeUntil'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.activeUntil'),
                            $qb->expr()->gt('d.activeUntil', ':now')
                        )
                    )
                )
            )
            ->setParameter('id', $id)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Find valid and discount by discount code
     *
     * @param $code
     * @return Discount|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findValidByCode($code)
    {
        $qb = $this->em->getRepository('AppBundle:Discount')
            ->createQueryBuilder('d');

        return $qb->select('d')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('d.code', ':code'),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.maximumUse'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.maximumUse'),
                            $qb->expr()->gt('d.maximumUse', 'd.currentUse')
                        )
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.activeFrom'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.activeFrom'),
                            $qb->expr()->lt('d.activeFrom', ':now')
                        )
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->isNull('d.activeUntil'),
                        $qb->expr()->andX(
                            $qb->expr()->isNotNull('d.activeUntil'),
                            $qb->expr()->gt('d.activeUntil', ':now')
                        )
                    )
                )
            )
            ->setParameter('code', $code)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Discount Initialize new instance of discount
     */
    public function initialize()
    {
        return new Discount();
    }

    /**
     * Add new discount
     *
     * @param Discount $discount
     * @return Discount
     */
    public function add(Discount $discount)
    {
        $discount->setWrittenBy($this->ts->getToken()->getUser());
        $discount->setWrittenOn(new \DateTime());

        $this->em->persist($discount);
        $this->em->flush();

        return $discount;
    }

    /**
     * Edit existing discount
     *
     * @param Discount $discount
     * @return Discount
     */
    public function edit(Discount $discount)
    {
        $discount->setEditedBy($this->ts->getToken()->getUser());
        $discount->setEditedOn(new \DateTime());

        $this->em->persist($discount);
        $this->em->flush();

        return $discount;
    }

    /**
     * Remove existing discount
     *
     * @param Discount $discount
     */
    public function remove(Discount $discount)
    {
        $this->em->remove($discount);
        $this->em->flush();
    }

    /**
     * Check if discount with entered code exists and return json response if discount is valid
     *
     * @param $discountCode
     * @return string
     */
    public function handleAjaxDiscount($discountCode)
    {
        $discountExists = 'false';
        $discountValue = 0;
        $discountId = 'null';

        if ($discountCode === '') {
            $discountExists = 'null';
        } else {
            $discount = $this->findValidByCode($discountCode);

            if ($discount) {
                $discountExists = 'true';
                $discountId = $discount->getId();
                $discountValue = $discount->getDiscount();
            }
        }

        $data = '{';
        $data .= '"discountExists": ' . $discountExists . ',';
        $data .= '"discountId": ' . $discountId . ',';
        $data .= '"discount": ' . $discountValue . '';
        $data .= '}';

        return $data;
    }
}
