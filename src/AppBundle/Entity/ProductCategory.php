<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ProductCategory
 * @package AppBundle\Entity
 */
class ProductCategory
{
    /**
     * @var integer Id of product category
     */
    protected $id;
    /**
     * @var string Title of product category
     */
    protected $title;
    /**
     * @var string Alias of product category
     */
    protected $alias;
    /**
     * @var string Description of product category
     */
    protected $description;
    /**
     * @var Sorting Sorting of product categories and texts
     */
    protected $sorting;
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection Products in product category
     */
    protected $products;
    /**
     * @var \DateTime Date and time when product category was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which product category was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when product category was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which product category was edited by
     */
    protected $editedBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ProductCategory
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ProductCategory
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProductCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sorting
     *
     * @param Sorting $sorting
     * @return ProductCategory
     */
    public function setSorting(Sorting $sorting = null)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get sorting
     *
     * @return Sorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Add products
     *
     * @param Product $products
     * @return ProductCategory
     */
    public function addProduct(Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param Product $products
     */
    public function removeProduct(Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set date and time when product category was written on
     *
     * @param \DateTime $writtenOn
     * @return ProductCategory
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when product category was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which product category was written by
     *
     * @param \DateTime $editedOn
     * @return ProductCategory
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which product category was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when product category was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return ProductCategory
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when product category was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which product category was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return ProductCategory
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which product category was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }
}
