<?php

namespace AppBundle\Entity;

use Gregwar\Image\Image;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Product
 * @package AppBundle\Entity
 */
class Product
{
    /**
     * @var integer Id of product
     */
    protected $id;
    /**
     * @var string Title of product
     */
    protected $title;
    /**
     * @var string Alias of product
     */
    protected $alias;
    /**
     * @var string Description of product
     */
    protected $description;
    /**
     * @var integer Price of product
     */
    protected $price;
    /**
     * @var integer Quantity of product
     */
    protected $quantity;
    /**
     * @var integer Discount of product
     */
    protected $discount;
    /**
     * @var integer Count (amount) of pieces of product
     */
    protected $count;
    /**
     * @var CountText Original count text entity - Text suffix to count (amount) of pieces of product
     */
    protected $countText;
    /**
     * @var \DateTime Date and time when product was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which product was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when product was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which product was edited by
     */
    protected $editedBy;
    /**
     * @var \Doctrine\Common\Collections\ArrayCollection Category in which product is
     */
    protected $category;
    /**
     * @var bool This product is marked as favourite
     */
    protected $favourite;
    /**
     * @var UploadedFile Product image file during upload
     */
    protected $file;
    /**
     * @var string Extension of product image
     */
    protected $extension;
    /**
     * @var string Temporary path to product image file during upload
     */
    protected $temp;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
        $this->favourite = false;
    }

    /**
     * Get id of product
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of product
     *
     * @param string $title
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of product
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias of product
     *
     * @param string $alias
     * @return Product
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias of product
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set description of product
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description of product
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price of product
     *
     * @param integer $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price of product
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get total price of product
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        if ($this->getDiscount()) {
            return $this->getPrice() * ((100 - $this->getDiscount()) / 100);
        } else {
            return $this->getPrice();
        }
    }

    /**
     * Set quantity of product
     *
     * @param integer $quantity
     * @return Product
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Increase quantity of product
     *
     * @param integer $quantity
     * @return Product
     */
    public function increaseQuantity($quantity)
    {
        $this->quantity += $quantity;

        return $this;
    }

    /**
     * Decrease quantity of product
     *
     * @param integer $quantity
     * @return Product
     */
    public function decreaseQuantity($quantity)
    {
        $this->quantity -= $quantity;

        return $this;
    }

    /**
     * Get quantity of product
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set discount of product
     *
     * @param integer $discount
     * @return Product
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount of product
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set count (amount) of pieces of product
     *
     * @param integer $count
     * @return Product
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count (amount) of pieces of product
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }


    /**
     * Set countText
     *
     * @param CountText $countText
     * @return Product
     */
    public function setCountText(CountText $countText)
    {
        $this->countText = $countText;

        return $this;
    }

    /**
     * Get countText
     *
     * @return CountText
     */
    public function getCountText()
    {
        return $this->countText;
    }

    /**
     * Set date and time when product was written on
     *
     * @param \DateTime $writtenOn
     * @return Product
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when product was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which product was written by
     *
     * @param \DateTime $editedOn
     * @return Product
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which product was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when product was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Product
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when product was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which product was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Product
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which product was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Add category in which product is
     *
     * @param ProductCategory $category
     * @return Product
     */
    public function addCategory(ProductCategory $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category in which product is
     *
     * @param ProductCategory $category
     */
    public function removeCategory(ProductCategory $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Set category in which product is
     *
     * @param ProductCategory $category
     * @return Product
     */
    public function setCategory(ProductCategory $category)
    {
        // Set only first category - Collection is used because of future extensibility
        $this->category = [];
        $this->category[] = $category;

        return $this;
    }

    /**
     * Get category in which product is
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        // Get only first category - Collection is used because of future extensibility
        return $this->category[0];
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->extension = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Guess extension of file.
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $this->extension = $this->getFile()->guessExtension();
        }
    }

    /**
     * Upload product image file
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->id . '.' . $this->getFile()->guessExtension()
        );

        Image::open($this->getAbsolutePath())
            ->resize(400, 400)
            ->save($this->getAbsolutePath());

        $this->setFile(null);
    }

    /**
     * Store filename of product image file before file is removed
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * Remove product image file when this entity is removed
     */
    public function removeUpload()
    {
        if (isset($this->temp)) {
            try {
                unlink($this->temp);
            } catch (ContextErrorException $e) {
                // Image does not exists
            }
        }
    }

    /**
     * Get absolute web path
     *
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadRootDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get web path
     *
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get root upload directory
     *
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'upload/images/products';
    }

    /**
     * Set extension of product image file
     *
     * @param string $extension
     * @return Product
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension of product image file
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set if this product is marked as favourite
     *
     * @param boolean $favourite
     * @return Product
     */
    public function setFavourite($favourite)
    {
        $this->favourite = $favourite;

        return $this;
    }

    /**
     * Get if this product is marked as favourite
     *
     * @return boolean
     */
    public function getFavourite()
    {
        return $this->favourite;
    }

    /**
     * @return string Get values of important attributes in json format
     */
    public function getJson()
    {
        $json = [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'alias' => $this->getAlias(),
            'description' => $this->getDescription(),
            'price' => $this->getTotalPrice(),
            'quantity' => $this->getQuantity(),
            'discount' => $this->getDiscount(),
            'extension' => $this->getExtension()
        ];

        return json_encode($json);
    }
}
