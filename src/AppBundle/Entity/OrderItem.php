<?php

namespace AppBundle\Entity;

/**
 * Class OrderItem
 * @package AppBundle\Entity
 */
class OrderItem
{
    /**
     * @var integer Id of order item
     */
    protected $id;
    /**
     * @var OrderList Order list of order item
     */
    protected $orderList;
    /**
     * @var Product Original product entity
     */
    protected $product;
    /**
     * @var string Title of product in order item
     */
    protected $title;
    /**
     * @var integer Price of product in order item
     */
    protected $price;
    /**
     * @var integer Quantity of product in order item
     */
    protected $quantity;
    /**
     * @var integer Discount of product in order item
     */
    protected $discount;

    /**
     * Get id of order item
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order list of order item
     *
     * @param OrderList $orderList
     * @return OrderItem
     */
    public function setOrderList(OrderList $orderList = null)
    {
        $this->orderList = $orderList;

        return $this;
    }

    /**
     * Get order list of order item
     *
     * @return OrderList
     */
    public function getOrderList()
    {
        return $this->orderList;
    }

    /**
     * Set original product entity and copy order item parameters from original product
     *
     * @param Product $product
     * @return OrderItem
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        $this->title = $this->product->getTitle();
        $this->price = $this->product->getPrice();
        $this->discount = $this->product->getDiscount();

        return $this;
    }

    /**
     * Get original product entity
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set title of product in order item
     *
     * @param string $title
     * @return OrderItem
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of product in order item
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set price of product in order item
     *
     * @param integer $price
     * @return OrderItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price of product in order item
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity of product in order item
     *
     * @param integer $quantity
     * @return OrderItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity of product in order item
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set discount of product in order item
     *
     * @param integer $discount
     * @return OrderItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount of product in order item
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Get total price of order item
     *
     * @return integer
     */
    public function getTotalPrice()
    {
        return $this->getPrice() * $this->quantity;
    }
}
