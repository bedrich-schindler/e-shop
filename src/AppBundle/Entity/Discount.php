<?php

namespace AppBundle\Entity;

/**
 * Class Discount
 * @package AppBundle\Entity
 */
class Discount
{
    /**
     * @var integer Id of discount
     */
    protected $id;
    /**
     * @var string Code of discount
     */
    protected $code;
    /**
     * @var integer Discount of discount
     */
    protected $discount;
    /**
     * @var \DateTime Date and time when discount is active from
     */
    protected $activeFrom;
    /**
     * @var \DateTime Date and time when discount is active until
     */
    protected $activeUntil;
    /**
     * @var integer Maximum use of discount
     */
    protected $maximumUse;
    /**
     * @var integer Current user of discount
     */
    protected $currentUse = 0;
    /**
     * @var \DateTime Date and time when discount was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which discount was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when discount was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which discount was edited by
     */
    protected $editedBy;

    /**
     * Get id of discount
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code of discount
     *
     * @param string $code
     * @return Discount
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code of discount
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set discount of discount
     *
     * @param integer $discount
     * @return Discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount of discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set fate and time when discount is active from
     *
     * @param \DateTime $activeFrom
     * @return Discount
     */
    public function setActiveFrom($activeFrom)
    {
        $this->activeFrom = $activeFrom;

        return $this;
    }

    /**
     * Get date and time when discount is active from
     *
     * @return \DateTime
     */
    public function getActiveFrom()
    {
        return $this->activeFrom;
    }

    /**
     * Set date and time when discount is active until
     *
     * @param \DateTime $activeUntil
     * @return Discount
     */
    public function setActiveUntil($activeUntil)
    {
        $this->activeUntil = $activeUntil;

        return $this;
    }

    /**
     * Get date and time when discount is active until
     *
     * @return \DateTime
     */
    public function getActiveUntil()
    {
        return $this->activeUntil;
    }

    /**
     * Set maximum use of discount
     *
     * @param integer $maximumUse
     * @return Discount
     */
    public function setMaximumUse($maximumUse)
    {
        $this->maximumUse = $maximumUse;

        return $this;
    }

    /**
     * Get maximum use of discount
     *
     * @return integer
     */
    public function getMaximumUse()
    {
        return $this->maximumUse;
    }

    /**
     * Set current user of discount
     *
     * @param integer $currentUse
     * @return Discount
     */
    public function setCurrentUse($currentUse)
    {
        $this->currentUse = $currentUse;

        return $this;
    }

    /**
     * Get current user of discount
     *
     * @return integer
     */
    public function getCurrentUse()
    {
        return $this->currentUse;
    }

    /**
     * Increase current use of discount
     *
     * @param integer $quantity
     * @return Discount
     */
    public function increaseCurrentUse($quantity)
    {
        $this->currentUse += $quantity;

        return $this;
    }

    /**
     * Decrease current use of discount
     *
     * @param integer $quantity
     * @return Discount
     */
    public function decreaseCurrentUse($quantity)
    {
        $this->currentUse -= $quantity;

        return $this;
    }

    /**
     * Set date and time when discount was written on
     *
     * @param \DateTime $writtenOn
     * @return Discount
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when discount was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which discount was written by
     *
     * @param \DateTime $editedOn
     * @return Discount
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which discount was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when discount was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Discount
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when discount was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which discount was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Discount
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which discount was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }
}
