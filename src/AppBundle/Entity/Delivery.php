<?php

namespace AppBundle\Entity;

/**
 * Class Delivery
 * @package AppBundle\Entity
 */
class Delivery
{
    /**
     * @var integer Id of delivery
     */
    protected $id;
    /**
     * @var string Method title of delivery
     */
    protected $method;
    /**
     * @var string Method detail of delivery (Unused yet)
     */
    protected $methodDetail;
    /**
     * @var string Format of delivery (Acceptable - ulozenka|null)
     */
    protected $format;
    /**
     * @var integer Fee of delivery
     */
    protected $fee;

    /**
     * Get id of delivery
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method name of delivery
     *
     * @param string $method
     * @return Delivery
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method name of delivery
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set method detail of delivery
     *
     * @param string $methodDetail
     * @return Delivery
     */
    public function setMethodDetail($methodDetail)
    {
        $this->methodDetail = $methodDetail;

        return $this;
    }

    /**
     * Get method detail of delivery
     *
     * @return string
     */
    public function getMethodDetail()
    {
        return $this->methodDetail;
    }

    /**
     * Set format of delivery (Acceptable - ulozenka|null)
     *
     * @param string $format
     * @return Delivery
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format of delivery (Acceptable - ulozenka|null)
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set fee of delivery
     *
     * @param integer $fee
     * @return Delivery
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee of delivery
     *
     * @return integer
     */
    public function getFee()
    {
        return $this->fee;
    }
}
