<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class UploadFile
 * @package AppBundle\Entity
 */
class UploadFile
{
    /**
     * @var UploadedFile File during upload
     */
    protected $file;

    /**
     * @return UploadedFile Get file
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Upload file
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFile()->getClientOriginalName()
        );
    }

    /**
     * Get root upload directory
     *
     * @return string
     */
    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    public function getUploadDir()
    {
        return 'upload/files/share/';
    }
}
