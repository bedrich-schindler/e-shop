<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

/**
 * Class OrderList
 * @package AppBundle\Entity
 */
class OrderList
{
    /**
     * @var integer Id
     */
    protected $id;

    /**
     * @var ArrayCollection Collection of order items
     */
    protected $orderItem;

    /**
     * @var Delivery Original delivery entity - used for frontend interaction
     */
    protected $delivery;
    /**
     * @var OrderDeliveryItem Original order delivery item entity
     */
    protected $orderDeliveryItem;
    /**
     * @var Payment Original payment entity - used for frontend interaction
     */
    protected $payment;
    /**
     * @var OrderPaymentItem Original order payment item entity
     */
    protected $orderPaymentItem;
    /**
     * @var string Delivery detail - typically branch store
     */
    protected $deliveryDetail;
    /**
     * @var Discount Original discount entity - used for frontend interaction
     */
    protected $discount;
    /**
     * @var OrderDiscountItem Original order discount item entity
     */
    protected $orderDiscountItem;

    /**
     * @var string Delivery email
     */
    protected $deliveryEmail;
    /**
     * @var string Delivery first name
     */
    protected $deliveryFirstname;
    /**
     * @var string Delivery last name
     */
    protected $deliveryLastname;
    /**
     * @var string Delivery phone
     */
    protected $deliveryPhone;
    /**
     * @var string Delivery street
     */
    protected $deliveryStreet;
    /**
     * @var string Delivery city
     */
    protected $deliveryCity;
    /**
     * @var integer Delivery zip code
     */
    protected $deliveryZipCode;
    /**
     * @var string Delivery street
     */

    /**
     * @var bool Billing information are different from delivery information
     */
    protected $billingDifferent;
    /**
     * @var string Billing email
     */
    protected $billingEmail;
    /**
     * @var string Billing first name
     */
    protected $billingFirstname;
    /**
     * @var string Billing last name
     */
    protected $billingLastname;
    /**
     * @var string Billing phone
     */
    protected $billingPhone;
    /**
     * @var string Billing street
     */
    protected $billingStreet;
    /**
     * @var string Billing city
     */
    protected $billingCity;
    /**
     * @var integer Billing zip code
     */
    protected $billingZipCode;

    /**
     * @var string Remarks
     */
    protected $remarks;

    /**
     * @var \DateTime Date and time when order was created on
     */
    protected $orderCreatedOn;
    /**
     * @var \DateTime Date and time when order was received on
     */
    protected $orderReceivedOn;
    /**
     * @var \DateTime Date and time when order was sent on
     */
    protected $orderSentOn;
    /**
     * @var \DateTime Date and time when order was finished on
     */
    protected $orderFinishedOn;
    /**
     * @var \DateTime Date and time when order was canceled on
     */
    protected $orderCanceledOn;
    /**
     * @var \DateTime Date and time when order was cancel by email on
     */
    protected $emailCanceledOn;

    /**
     * @var \AppBundle\Entity\User User which order was handled by
     */
    protected $handledBy;
    /**
     * @var \DateTime Date and time when order was handled on
     */
    protected $handledOn;

    /**
     * @var bool This order list can be changed by customer
     */
    protected $active;
    /**
     * @var string Status of order (Acceptable enum values - Created|Received|Sent|Finished|Canceled)
     */
    protected $status;
    /**
     * @var bool Customer accepted terms and conditions
     */
    protected $termsAndConditions;
    /**
     * @var bool Customer accepted newsletter subscription
     */
    protected $newsletter;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderItem = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orderCreatedOn = new \DateTime();
        $this->active = true;
        $this->status = 'Created';
        $this->termsAndConditions = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get order number
     *
     * @return integer
     */
    public function getOrderNumber()
    {
        return $this->orderCreatedOn->format('y') . '000' . $this->id;
    }

    /**
     * Set collection of order items
     *
     * @param ArrayCollection $orderItems
     * @return OrderList
     */
    public function setOrderItem(ArrayCollection $orderItems)
    {
        $this->orderItem = $orderItems;

        return $this;
    }

    /**
     * Get collection of order items
     *
     * @return ArrayCollection
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Add order item to collection of order items
     *
     * @param OrderItem $orderItem
     * @return OrderList
     */
    public function addOrderItem(OrderItem $orderItem)
    {
        $this->orderItem[] = $orderItem;

        return $this;
    }

    /**
     * Reset collection of order items
     *
     * @return OrderList
     */
    public function resetOrderItem()
    {
        $this->orderItem = null;

        return $this;
    }

    /**
     * Remove order item from collection of order items
     *
     * @param OrderItem $orderItem
     */
    public function removeOrderItem(OrderItem $orderItem)
    {
        $this->orderItem->removeElement($orderItem);
    }

    /**
     * Set original delivery entity - used for frontend interaction
     *
     * @param Delivery $delivery
     * @return OrderList
     */
    public function setDelivery(Delivery $delivery = null)
    {
        $this->delivery = $delivery;

        if ($delivery) {
            if ($this->orderDeliveryItem) {
                $this->orderDeliveryItem->setMethod($delivery->getMethod());
                $this->orderDeliveryItem->setFormat($delivery->getFormat());
                $this->orderDeliveryItem->setFee($delivery->getFee());
                $this->orderDeliveryItem->setDeliveryId($delivery);
            } else {
                $this->orderDeliveryItem = new OrderDeliveryItem();
                $this->orderDeliveryItem->setMethod($delivery->getMethod());
                $this->orderDeliveryItem->setFormat($delivery->getFormat());
                $this->orderDeliveryItem->setFee($delivery->getFee());
                $this->orderDeliveryItem->setDeliveryId($delivery);
            }
        }

        return $this;
    }

    /**
     * Get original delivery entity - used for frontend interaction
     *
     * @return Delivery
     */
    public function getDelivery()
    {
        if ($this->orderDeliveryItem) {
            // Backend
            if ($this->orderDeliveryItem->getDeliveryId()) {
                // If original delivery is still available
                $this->delivery = $this->orderDeliveryItem->getDeliveryId();

                return $this->delivery;
            } else {
                // If original delivery is removed
                return $this->delivery;
            }
        } else {
            // Frontend
            return $this->delivery;
        }
    }

    /**
     * Set original order delivery item entity
     *
     * @param OrderDeliveryItem $orderDeliveryItem
     * @return OrderList
     */
    public function setOrderDeliveryItem(OrderDeliveryItem $orderDeliveryItem = null)
    {
        $this->orderDeliveryItem = $orderDeliveryItem;

        return $this;
    }

    /**
     * Get original order delivery item entity
     *
     * @return \AppBundle\Entity\OrderDeliveryItem
     */
    public function getOrderDeliveryItem()
    {
        return $this->orderDeliveryItem;
    }

    /**
     * Set delivery detail - typically branch store
     *
     * @param string $deliveryDetail
     * @return OrderList
     */
    public function setDeliveryDetail($deliveryDetail)
    {
        $this->deliveryDetail = $deliveryDetail;

        return $this;
    }

    /**
     * Get delivery detail - typically branch store
     *
     * @return string
     */
    public function getDeliveryDetail()
    {
        return $this->deliveryDetail;
    }

    /**
     * Set original payment entity - used for frontend interaction
     *
     * @param Payment $payment
     * @return OrderList
     */
    public function setPayment(Payment $payment = null)
    {
        $this->payment = $payment;

        if ($payment) {
            if ($this->orderPaymentItem) {
                $this->orderPaymentItem->setMethod($payment->getMethod());
                $this->orderPaymentItem->setFee($payment->getFee());
                $this->orderPaymentItem->setPaymentId($payment);
            } else {
                $this->orderPaymentItem = new OrderPaymentItem();
                $this->orderPaymentItem->setMethod($payment->getMethod());
                $this->orderPaymentItem->setFee($payment->getFee());
                $this->orderPaymentItem->setPaymentId($payment);
            }
        }

        return $this;
    }

    /**
     * Get original payment entity - used for frontend interaction
     *
     * @return Payment
     */
    public function getPayment()
    {
        if ($this->orderPaymentItem) {
            // Backend
            if ($this->orderPaymentItem->getPaymentId()) {
                // If original payment is still available
                $this->payment = $this->orderPaymentItem->getPaymentId();

                return $this->payment;
            } else {
                // If original payment is removed
                return $this->payment;
            }
        } else {
            // Frontend
            return $this->payment;
        }
    }

    /**
     * Set original order payment item entity
     *
     * @param OrderPaymentItem $orderPaymentItem
     * @return OrderList
     */
    public function setOrderPaymentItem(OrderPaymentItem $orderPaymentItem = null)
    {
        $this->orderPaymentItem = $orderPaymentItem;

        return $this;
    }

    /**
     * Get original order payment item entity
     *
     * @return OrderPaymentItem
     */
    public function getOrderPaymentItem()
    {
        return $this->orderPaymentItem;
    }

    /**
     * Set original discount entity - used for frontend interaction
     *
     * @param Discount $discount
     * @return OrderList
     */
    public function setDiscount(Discount $discount = null)
    {
        $this->discount = $discount;

        if ($discount) {
            if ($this->orderDiscountItem) {
                $this->orderDiscountItem->setCode($discount->getCode());
                $this->orderDiscountItem->setDiscount($discount->getDiscount());
                $this->orderDiscountItem->setDiscountId($discount);
            } else {
                $this->orderDiscountItem = new OrderDiscountItem();
                $this->orderDiscountItem->setCode($discount->getCode());
                $this->orderDiscountItem->setDiscount($discount->getDiscount());
                $this->orderDiscountItem->setDiscountId($discount);
            }
        } else {
            $this->orderDiscountItem = null;
        }

        return $this;
    }

    /**
     * Get original discount entity - used for frontend interaction
     *
     * @return Discount
     */
    public function getDiscount()
    {
        if ($this->orderDiscountItem) {
            // Backend
            if ($this->orderDiscountItem->getDiscountId()) {
                // If original discount is still available
                $this->discount = $this->orderDiscountItem->getDiscountId();

                return $this->discount;
            } else {
                // If original discount is removed
                return $this->discount;
            }
        } else {
            // Frontend
            return $this->discount;
        }
    }

    /**
     * Set orderDiscountItem Original order discount item entity
     *
     * @param OrderDiscountItem $orderDiscountItem
     * @return OrderList
     */
    public function setOrderDiscountItem(OrderDiscountItem $orderDiscountItem = null)
    {
        $this->orderDiscountItem = $orderDiscountItem;

        return $this;
    }

    /**
     * Get orderDiscountItem Original order discount item entity
     *
     * @return OrderDiscountItem
     */
    public function getOrderDiscountItem()
    {
        return $this->orderDiscountItem;
    }

    /**
     * Set delivery emai
     *
     * @param string $deliveryEmail
     * @return OrderList
     */
    public function setDeliveryEmail($deliveryEmail)
    {
        $this->deliveryEmail = $deliveryEmail;

        return $this;
    }

    /**
     * Get delivery emai
     *
     * @return string
     */
    public function getDeliveryEmail()
    {
        return $this->deliveryEmail;
    }

    /**
     * Set delivery first name
     *
     * @param string $deliveryFirstname
     * @return OrderList
     */
    public function setDeliveryFirstname($deliveryFirstname)
    {
        $this->deliveryFirstname = $deliveryFirstname;

        return $this;
    }

    /**
     * Get delivery first name
     *
     * @return string
     */
    public function getDeliveryFirstname()
    {
        return $this->deliveryFirstname;
    }

    /**
     * Set delivery last name
     *
     * @param string $deliveryLastname
     * @return OrderList
     */
    public function setDeliveryLastname($deliveryLastname)
    {
        $this->deliveryLastname = $deliveryLastname;

        return $this;
    }

    /**
     * Get delivery last name
     *
     * @return string
     */
    public function getDeliveryLastname()
    {
        return $this->deliveryLastname;
    }

    /**
     * Set delivery phone
     *
     * @param string $deliveryPhone
     * @return OrderList
     */
    public function setDeliveryPhone($deliveryPhone)
    {
        $this->deliveryPhone = $deliveryPhone;

        return $this;
    }

    /**
     * Get delivery phone
     *
     * @return string
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * Set delivery street
     *
     * @param string $deliveryStreet
     * @return OrderList
     */
    public function setDeliveryStreet($deliveryStreet)
    {
        $this->deliveryStreet = $deliveryStreet;

        return $this;
    }

    /**
     * Get delivery street
     *
     * @return string
     */
    public function getDeliveryStreet()
    {
        return $this->deliveryStreet;
    }

    /**
     * Set delivery city
     *
     * @param string $deliveryCity
     * @return OrderList
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    /**
     * Get delivery city
     *
     * @return string
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * Set delivery zip code
     *
     * @param string $deliveryZipCode
     * @return OrderList
     */
    public function setDeliveryZipCode($deliveryZipCode)
    {
        $this->deliveryZipCode = $deliveryZipCode;

        return $this;
    }

    /**
     * Get delivery zip code
     *
     * @return string
     */
    public function getDeliveryZipCode()
    {
        return $this->deliveryZipCode;
    }

    /**
     * Set  if billing information are different from delivery information
     *
     * @param boolean $billingDifferent
     * @return OrderList
     */
    public function setBillingDifferent($billingDifferent)
    {
        $this->billingDifferent = $billingDifferent;

        return $this;
    }

    /**
     * Get if billing information are different from delivery information
     *
     * @return boolean
     */
    public function getBillingDifferent()
    {
        return $this->billingDifferent;
    }

    /**
     * Set billing email
     *
     * @param string $billingEmail
     * @return OrderList
     */
    public function setBillingEmail($billingEmail)
    {
        $this->billingEmail = $billingEmail;

        return $this;
    }

    /**
     * Get billing email
     *
     * @return string
     */
    public function getBillingEmail()
    {
        return $this->billingEmail;
    }

    /**
     * Set billing firs tname
     *
     * @param string $billingFirstname
     * @return OrderList
     */
    public function setBillingFirstname($billingFirstname)
    {
        $this->billingFirstname = $billingFirstname;

        return $this;
    }

    /**
     * Get billing first name
     *
     * @return string
     */
    public function getBillingFirstname()
    {
        return $this->billingFirstname;
    }

    /**
     * Set billing last name
     *
     * @param string $billingLastname
     * @return OrderList
     */
    public function setBillingLastname($billingLastname)
    {
        $this->billingLastname = $billingLastname;

        return $this;
    }

    /**
     * Get billing last name
     *
     * @return string
     */
    public function getBillingLastname()
    {
        return $this->billingLastname;
    }

    /**
     * Set billing phone
     *
     * @param string $billingPhone
     * @return OrderList
     */
    public function setBillingPhone($billingPhone)
    {
        $this->billingPhone = $billingPhone;

        return $this;
    }

    /**
     * Get billing phone
     *
     * @return string
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }

    /**
     * Set billing street
     *
     * @param string $billingStreet
     * @return OrderList
     */
    public function setBillingStreet($billingStreet)
    {
        $this->billingStreet = $billingStreet;

        return $this;
    }

    /**
     * Get billing street
     *
     * @return string
     */
    public function getBillingStreet()
    {
        return $this->billingStreet;
    }

    /**
     * Set billing city
     *
     * @param string $billingCity
     * @return OrderList
     */
    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;

        return $this;
    }

    /**
     * Get billing city
     *
     * @return string
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * Set billing zip code
     *
     * @param string $billingZipCode
     * @return OrderList
     */
    public function setBillingZipCode($billingZipCode)
    {
        $this->billingZipCode = $billingZipCode;

        return $this;
    }

    /**
     * Get billing zip code
     *
     * @return string
     */
    public function getBillingZipCode()
    {
        return $this->billingZipCode;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     * @return OrderList
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }
    
    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set date and time when order was created on
     *
     * @param \DateTime $orderCreatedOn
     * @return OrderList
     */
    public function setOrderCreatedOn($orderCreatedOn)
    {
        $this->orderCreatedOn = $orderCreatedOn;

        return $this;
    }

    /**
     * Get date and time when order was created on
     *
     * @return \DateTime
     */
    public function getOrderCreatedOn()
    {
        return $this->orderCreatedOn;
    }

    /**
     * Set date and time when order was received on
     *
     * @param \DateTime $orderReceivedOn
     * @return OrderList
     */
    public function setOrderReceivedOn($orderReceivedOn)
    {
        $this->orderReceivedOn = $orderReceivedOn;

        return $this;
    }

    /**
     * Get date and time when order was received on
     *
     * @return \DateTime
     */
    public function getOrderReceivedOn()
    {
        return $this->orderReceivedOn;
    }

    /**
     * Set date and time when order was sent on
     *
     * @param \DateTime $orderSentOn
     * @return OrderList
     */
    public function setOrderSentOn($orderSentOn)
    {
        $this->orderSentOn = $orderSentOn;

        return $this;
    }

    /**
     * Get date and time when order was sent on
     *
     * @return \DateTime
     */
    public function getOrderSentOn()
    {
        return $this->orderSentOn;
    }

    /**
     * Set date and time when order was finished on
     *
     * @param \DateTime $orderFinishedOn
     * @return OrderList
     */
    public function setOrderFinishedOn($orderFinishedOn)
    {
        $this->orderFinishedOn = $orderFinishedOn;

        return $this;
    }

    /**
     * Get date and time when order was finished on
     *
     * @return \DateTime
     */
    public function getOrderFinishedOn()
    {
        return $this->orderFinishedOn;
    }

    /**
     * Set date and time when order was canceled on
     *
     * @param \DateTime $orderCanceledOn
     * @return OrderList
     */
    public function setOrderCanceledOn($orderCanceledOn)
    {
        $this->orderCanceledOn = $orderCanceledOn;

        return $this;
    }

    /**
     * Get date and time when order was canceled on
     *
     * @return \DateTime
     */
    public function getOrderCanceledOn()
    {
        return $this->orderCanceledOn;
    }

    /**
     * Set date and time when order was received by email on
     *
     * @param \DateTime $emailCanceledOn
     * @return OrderList
     */
    public function setEmailCanceledOn($emailCanceledOn)
    {
        $this->emailCanceledOn = $emailCanceledOn;

        return $this;
    }

    /**
     * Get date and time when order was received by email on
     *
     * @return \DateTime
     */
    public function getEmailCanceledOn()
    {
        return $this->emailCanceledOn;
    }

    /**
     * Set user which order was handled by
     *
     * @param \AppBundle\Entity\User $handledBy
     * @return OrderList
     */
    public function setHandledBy(\AppBundle\Entity\User $handledBy = null)
    {
        $this->handledBy = $handledBy;

        return $this;
    }

    /**
     * Get user which order was handled by
     *
     * @return \AppBundle\Entity\User
     */
    public function getHandledBy()
    {
        return $this->handledBy;
    }

    /**
     * Set date and time when order was received on
     *
     * @param \DateTime $handledOn
     * @return OrderList
     */
    public function setHandledOn($handledOn)
    {
        $this->handledOn = $handledOn;

        return $this;
    }

    /**
     * Get date and time when order was received on
     *
     * @return \DateTime
     */
    public function getHandledOn()
    {
        return $this->handledOn;
    }

    /**
     * Set if this order list can be changed by customer
     *
     * @param boolean $active
     * @return OrderList
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get if his order list can be changed by customer
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set status of order (Acceptable enum values - Created|Received|Sent|Finished|Canceled)
     *
     * @param string $status
     * @return OrderList
     */
    public function setStatus($status)
    {
        $enum = ['Created', 'Received', 'Sent', 'Finished', 'Canceled'];

        if (!in_array($status, $enum)) {
            throw new InvalidArgumentException($status . ' is not valid status of order list');
        }

        $this->status = $status;

        return $this;
    }

    /**
     * Get status of order (Acceptable enum values - Created|Received|Sent|Finished|Canceled)
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set if customer accepted terms and conditions
     *
     * @param boolean $termsAndConditions
     * @return OrderList
     */
    public function setTermsAndConditions($termsAndConditions)
    {
        $this->termsAndConditions = $termsAndConditions;

        return $this;
    }

    /**
     * Get if customer accepted terms and conditions
     *
     * @return boolean
     */
    public function getTermsAndConditions()
    {
        return $this->termsAndConditions;
    }

    /**
     * Set if customer accepted newsletter subscription
     *
     * @param string $newsletter
     * @return OrderList
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    /**
     * Get if customer accepted newsletter subscription
     *
     * @return string
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * Get total price (including discount)
     *
     * @return int
     */
    public function getTotalPrice()
    {
        if ($this->orderDiscountItem) {
            $totalPrice = $this->getProductsTotalPrice() * ((100 - $this->orderDiscountItem->getDiscount()) / 100);

            if (isset($this->orderDeliveryItem)) {
                $totalPrice += $this->orderDeliveryItem->getFee();
            }
            if (isset($this->orderPaymentItem)) {
                $totalPrice += $this->orderPaymentItem->getFee();
            }

            return $totalPrice;
        } else {
            return $this->getTotalPriceWithoutDiscount();
        }
    }

    /**
     * Get total price without discount
     *
     * @return int
     */
    public function getTotalPriceWithoutDiscount()
    {
        $totalPrice = $this->getProductsTotalPrice();

        if (isset($this->orderDeliveryItem)) {
            $totalPrice += $this->orderDeliveryItem->getFee();
        }
        if (isset($this->orderPaymentItem)) {
            $totalPrice += $this->orderPaymentItem->getFee();
        }

        return $totalPrice;
    }

    /**
     * Get discount price from total price
     *
     * @return int
     */
    public function getDiscountPriceFromTotal()
    {
        return $this->getTotalPriceWithoutDiscount() - $this->getTotalPrice();
    }

    /**
     * Get total price of products only
     *
     * @return int
     */
    public function getProductsTotalPrice()
    {
        $totalPrice = 0;

        foreach ($this->orderItem as $orderItem) {
            $totalPrice += $orderItem->getTotalPrice();
        }

        return $totalPrice;
    }
}
