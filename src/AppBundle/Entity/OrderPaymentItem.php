<?php

namespace AppBundle\Entity;

/**
 * Class OrderPaymentItem
 * @package AppBundle\Entity
 */
class OrderPaymentItem
{
    /**
     * @var integer Id of order payment item
     */
    protected $id;
    /**
     * @var string Method title of order payment item
     */
    protected $method;
    /**
     * @var integer Fee of order payment item
     */
    protected $fee;
    /**
     * @var Payment Original payment entity
     */
    protected $paymentId;

    /**
     * Get id of order payment id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method title of order payment item
     *
     * @param string $method
     * @return Payment
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method title of order payment item
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set fee of order payment item
     *
     * @param integer $fee
     * @return Payment
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee of order payment item
     *
     * @return integer
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set original payment entity
     *
     * @param Payment $paymentId
     * @return OrderDiscountItem
     */
    public function setPaymentId(Payment $paymentId = null)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get original payment entity
     *
     * @return Payment
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }
}
