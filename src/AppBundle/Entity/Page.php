<?php

namespace AppBundle\Entity;

/**
 * Class Page
 * @package AppBundle\Entity
 */
class Page
{
    /**
     * @var integer Id of page
     */
    protected $id;
    /**
     * @var string Alias of page
     */
    protected $alias;
    /**
     * @var string Title of page
     */
    protected $title;
    /**
     * @var string Content of page
     */
    protected $content;
    /**
     * @var \DateTime Date and time when page was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which page was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when page was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which page was edited by
     */
    protected $editedBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Page
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Page
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Page
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set date and time when page was written on
     *
     * @param \DateTime $writtenOn
     * @return Page
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when page was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which page was written by
     *
     * @param \DateTime $editedOn
     * @return Page
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which page was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when page was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Page
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when page was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which page was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Page
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which page was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }
}
