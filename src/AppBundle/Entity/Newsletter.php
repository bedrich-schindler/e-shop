<?php

namespace AppBundle\Entity;

/**
 * Class Newsletter
 * @package AppBundle\Entity
 */
class Newsletter
{
    /**
     * @var integer Id of newsletter
     */
    protected $id;
    /**
     * @var string Email of newsletter
     */
    protected $email;
    /**
     * @var \DateTime Date and time when newsletter was added on
     */
    protected $addedOn;

    /**
     * Get id of newsletter
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email of newsletter
     *
     * @param string $email
     * @return Newsletter
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email of newsletter
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set date and time when newsletter was added on
     *
     * @param \DateTime $addedOn
     * @return Newsletter
     */
    public function setAddedOn($addedOn)
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    /**
     * Get date and time when newsletter was added on
     *
     * @return \DateTime
     */
    public function getAddedOn()
    {
        return $this->addedOn;
    }
}
