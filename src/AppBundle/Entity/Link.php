<?php

namespace AppBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Link
 * @package AppBundle\Entity
 */
class Link
{
    /**
     * @var integer Id of link
     */
    protected $id;
    /**
     * @var string Title of link
     */
    protected $title;
    /**
     * @var string Link (URL) of link
     */
    protected $link;
    /**
     * @var \DateTime Date and time when article was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which article was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when article was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which article was edited by
     */
    protected $editedBy;
    /**
     * @var UploadedFile Link image file during upload
     */
    protected $file;
    /**
     * @var string Extension of link image
     */
    protected $extension;
    /**
     * @var string Temporary path to link image file during upload
     */
    protected $temp;

    /**
     * Get id of article
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of article
     *
     * @param string $title
     * @return Link
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of article
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set link (URL) of article
     *
     * @param string $link
     * @return Link
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link (URL) of article
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set date and time when link was written on
     *
     * @param \DateTime $writtenOn
     * @return Link
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when link was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which link was written by
     *
     * @param \DateTime $editedOn
     * @return Link
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which link was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when link was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Link
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when link was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which link was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Link
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which link was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->extension = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Guess extension of file.
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $this->extension = $this->getFile()->guessExtension();
        }
    }

    /**
     * Upload link image file
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->id . '.' . $this->getFile()->guessExtension()
        );

        $this->setFile(null);
    }

    /**
     * Store filename of link image file before file is removed
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * Remove link image file when this entity is removed
     */
    public function removeUpload()
    {
        if (isset($this->temp)) {
            unlink($this->temp);
        }
    }

    /**
     * Get absolute web path
     *
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadRootDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get web path
     *
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get root upload directory
     *
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'upload/images/links';
    }

    /**
     * Set extension of link image file
     *
     * @param string $extension
     * @return Link
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension of link image file
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }
}
