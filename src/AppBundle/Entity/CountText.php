<?php

namespace AppBundle\Entity;

/**
 * Class CountText
 * @package AppBundle\Entity
 */
class CountText
{
    /**
     * @var integer Id of count text
     */
    protected $id;
    /**
     * @var string Title of count text
     */
    protected $title;

    /**
     * Get id of count text
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of count text
     *
     * @param string $title
     * @return CountText
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of count text
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
