<?php

namespace AppBundle\Entity;

/**
 * Class OrderDiscountItem
 * @package AppBundle\Entity
 */
class OrderDiscountItem
{
    /**
     * @var integer Id of order discount item
     */
    protected $id;
    /**
     * @var string Code of order discount item
     */
    protected $code;
    /**
     * @var integer Discount of order discount item
     */
    protected $discount;
    /**
     * @var Discount Original discount entity
     */
    protected $discountId;

    /**
     * Get id of order discount item
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code of order discount item
     *
     * @param string $code
     * @return OrderDiscountItem
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code of order discount item
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set discount of order discount item
     *
     * @param integer $discount
     * @return OrderDiscountItem
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount of order discount item
     *
     * @return integer
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set original discount entity
     *
     * @param Discount $discountId
     * @return OrderDiscountItem
     */
    public function setDiscountId(Discount $discountId = null)
    {
        $this->discountId = $discountId;

        return $this;
    }

    /**
     * Get original discount entity
     *
     * @return Discount
     */
    public function getDiscountId()
    {
        return $this->discountId;
    }
}
