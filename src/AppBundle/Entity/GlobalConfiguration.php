<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class GlobalConfiguration
 * @package AppBundle\Entity
 */
class GlobalConfiguration
{
    /**
     * @var integer Id of global configuration.
     */
    protected $id ;
    /**
     * @var string Name shown on invoice
     */
    protected $invoiceName;
    /**
     * @var string Street shown on invoice
     */
    protected $invoiceStreet;
    /**
     * @var string City shown on invoice
     */
    protected $invoiceCity;
    /**
     * @var integer Zip code shown on invoice
     */
    protected $invoiceZipCode;
    /**
     * @var string IC shown on invoice
     */
    protected $invoiceIc;
    /**
     * @var string Bank number shown on invoice
     */
    protected $invoiceBank;
    /**
     * @var string Additional text shown on invoice
     */
    protected $invoiceText;
    /**
     * @var string Phone shown on invoice
     */
    protected $invoicePhone;
    /**
     * @var string Email shown on invoice
     */
    protected $invoiceEmail;
    /**
     * @var object Header image file variable which is used during header image file upload
     */
    protected $headerImageFile;
    /**
     * @var ArrayCollection Payment collection of all available payments
     */
    protected $paymentCollection;
    /**
     * @var ArrayCollection Delivery collection of all available deliveries
     */
    protected $deliveryCollection;

    /**
     * Global configuration constructor
     */
    public function __construct()
    {
        $this->paymentCollection = new ArrayCollection();
        $this->deliveryCollection = new ArrayCollection();
    }

    /**
     * Set id of global configuration.
     *
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id of global configuration.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name shown on invoice
     *
     * @param string $invoiceName
     * @return GlobalConfiguration
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get name shown on invoice
     *
     * @return string
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set street shown on invoice
     *
     * @param string $invoiceStreet
     * @return GlobalConfiguration
     */
    public function setInvoiceStreet($invoiceStreet)
    {
        $this->invoiceStreet = $invoiceStreet;

        return $this;
    }

    /**
     * Get street shown on invoice
     *
     * @return string
     */
    public function getInvoiceStreet()
    {
        return $this->invoiceStreet;
    }

    /**
     * Set city shown on invoice
     *
     * @param string $invoiceCity
     * @return GlobalConfiguration
     */
    public function setInvoiceCity($invoiceCity)
    {
        $this->invoiceCity = $invoiceCity;

        return $this;
    }

    /**
     * Get city shown on invoice
     *
     * @return string
     */
    public function getInvoiceCity()
    {
        return $this->invoiceCity;
    }

    /**
     * Set zip code shown on invoice
     *
     * @param integer $invoiceZipCode
     * @return GlobalConfiguration
     */
    public function setInvoiceZipCode($invoiceZipCode)
    {
        $this->invoiceZipCode = $invoiceZipCode;

        return $this;
    }

    /**
     * Get zip code shown on invoice
     *
     * @return integer
     */
    public function getInvoiceZipCode()
    {
        return $this->invoiceZipCode;
    }

    /**
     * Set IC shown on invoice
     *
     * @param string $invoiceIc
     * @return GlobalConfiguration
     */
    public function setInvoiceIc($invoiceIc)
    {
        $this->invoiceIc = $invoiceIc;

        return $this;
    }

    /**
     * Get IC shown on invoice
     *
     * @return string
     */
    public function getInvoiceIc()
    {
        return $this->invoiceIc;
    }

    /**
     * Set bank number shown on invoice
     *
     * @param string $invoiceBank
     * @return GlobalConfiguration
     */
    public function setInvoiceBank($invoiceBank)
    {
        $this->invoiceBank = $invoiceBank;

        return $this;
    }

    /**
     * Get bank number shown on invoice
     *
     * @return string
     */
    public function getInvoiceBank()
    {
        return $this->invoiceBank;
    }

    /**
     * Set additional text shown on invoice
     *
     * @param string $invoiceText
     * @return GlobalConfiguration
     */
    public function setInvoiceText($invoiceText)
    {
        $this->invoiceText = $invoiceText;

        return $this;
    }

    /**
     * Get additional text shown on invoice
     *
     * @return string
     */
    public function getInvoiceText()
    {
        return $this->invoiceText;
    }

    /**
     * Set phone shown on invoice
     *
     * @param string $invoicePhone
     * @return GlobalConfiguration
     */
    public function setInvoicePhone($invoicePhone)
    {
        $this->invoicePhone = $invoicePhone;

        return $this;
    }

    /**
     * Get phone shown on invoice
     *
     * @return string
     */
    public function getInvoicePhone()
    {
        return $this->invoicePhone;
    }

    /**
     * Set email shown on invoice
     *
     * @param string $invoiceEmail
     * @return GlobalConfiguration
     */
    public function setInvoiceEmail($invoiceEmail)
    {
        $this->invoiceEmail = $invoiceEmail;

        return $this;
    }

    /**
     * Get email shown on invoice
     *
     * @return string
     */
    public function getInvoiceEmail()
    {
        return $this->invoiceEmail;
    }

    /**
     * Get header image file variable content which is used during header image file upload.
     *
     * @return object
     */
    public function getHeaderImageFile()
    {
        return $this->headerImageFile;
    }

    /**
     * Set header image file variable content which is used during header image file upload.
     *
     * @param $headerImageFile
     */
    public function setHeaderImageFile($headerImageFile)
    {
        $this->headerImageFile = $headerImageFile;
    }

    /**
     * Upload header image file with specific name
     */
    public function uploadHeaderImageFile()
    {
        if (null === $this->getHeaderImageFile()) {
            return;
        }

        $this->getHeaderImageFile()->move(
            $this->getUploadRootDir(),
            'header.jpg'
        );
    }

    /**
     * Get absolute path of header image
     *
     * @return string
     */
    public function getHeaderImageAbsolutePath()
    {
        return $this->getUploadDir() . 'header.jpg';
    }

    /**
     * Get upload root directory
     *
     * @return string
     */
    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    public function getUploadDir()
    {
        return 'upload/images/header/';
    }

    /**
     * Get payment collection of all available payments
     *
     * @return ArrayCollection
     */
    public function getPaymentCollection()
    {
        return $this->paymentCollection;
    }

    /**
     * Set payment collection of all available payments
     *
     * @param array $paymentCollection
     * @return $this
     */
    public function setPaymentCollection($paymentCollection)
    {
        $this->paymentCollection = $paymentCollection;

        return $this;
    }

    /**
     * Add payment to payment collection of all available payments
     *
     * @param $payment
     * @return $this
     */
    public function addPaymentCollection($payment)
    {
        if (!$this->paymentCollection) {
            $this->paymentCollection = new ArrayCollection();
        }

        $this->paymentCollection->add($payment);

        return $this;
    }

    /**
     * Get delivery collection of all available deliveries
     *
     * @return ArrayCollection
     */
    public function getDeliveryCollection()
    {
        return $this->deliveryCollection;
    }

    /**
     * Set delivery collection of all available deliveries
     *
     * @param array $deliveryCollection
     * @return $this
     */
    public function setDeliveryCollection($deliveryCollection)
    {
        $this->deliveryCollection = $deliveryCollection;

        return $this;
    }

    /**
     * Add delivery to delivery collection of all available deliveries
     *
     * @param $delivery
     * @return $this
     */
    public function addDeliveryCollection($delivery)
    {
        if (!$this->deliveryCollection) {
            $this->deliveryCollection = new ArrayCollection();
        }

        $this->deliveryCollection->add($delivery);

        return $this;
    }
}
