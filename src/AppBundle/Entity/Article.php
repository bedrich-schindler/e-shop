<?php

namespace AppBundle\Entity;

/**
 * Class Article
 * @package AppBundle\Entity
 */
class Article
{
    /**
     * @var integer Id of article
     */
    protected $id;
    /**
     * @var string Title of article
     */
    protected $title;
    /**
     * @var string Content of article
     */
    protected $content;
    /**
     * @var bool This article is marked as default (The only article visible on the page)
     */
    protected $isDefault = false;
    /**
     * @var \DateTime Date and time when article was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which article was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when article was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which article was edited by
     */
    protected $editedBy;

    /**
     * Get id of article
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of article
     *
     * @param string $title
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of article
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content of article
     *
     * @param string $content
     * @return Article
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content of article
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set true true if this article is marked as default (The only article visible on the page)
     *
     * @param boolean $isDefault
     * @return Article
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * Get true if this article is marked as default (The only article visible on the page)
     *
     * @return boolean
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * Set date and time when article was written on
     *
     * @param \DateTime $writtenOn
     * @return Article
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when article was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which article was written by
     *
     * @param \DateTime $editedOn
     * @return Article
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which article was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when article was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Article
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when article was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which article was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Article
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which article was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }
}
