<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use FOS\UserBundle\Model\GroupInterface as GroupInterface;

/**
 * Class User
 * @package AppBundle\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer Id of user
     */
    protected $id;
    /**
     * @var string First name of user
     */
    protected $firstname;
    /**
     * @var string Last name of user
     */
    protected $lastname;
    /**
     * @var GroupInterface Groups of user
     */
    protected $groups;
    /**
     * @var \DateTime Date and time when user was registered at
     */
    protected $registeredAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles = ['ROLE_USER'];
        $this->enabled = true;
        $this->registeredAt = new \DateTime();
    }

    /**
     * Get id of user
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first name of user
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get first name of user
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set last name of user
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get last name of user
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Get full name of user
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * Set date and time when user was registered at
     *
     * @param \DateTime $registeredAt
     * @return User
     */
    public function setRegisteredAt($registeredAt)
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    /**
     * Get date and time when user was registered at
     *
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    /**
     * Add groups of user
     *
     * @param \FOS\UserBundle\Model\GroupInterface $groups
     * @return User
     */
    public function addGroup(GroupInterface $groups)
    {
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Remove groups of user
     *
     * @param \FOS\UserBundle\Model\GroupInterface $groups
     * @return null
     */
    public function removeGroup(GroupInterface $groups)
    {
        $this->groups->removeElement($groups);
    }

    /**
     * Set groups of user
     *
     * @param \FOS\UserBundle\Model\GroupInterface $groups
     * @return User
     */
    public function setGroups($groups)
    {
        $this->groups = [];
        $this->groups[] = $groups;

        return $this;
    }

    /**
     * Get groups of user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set roles of user
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles)
    {
        $this->roles = [];
        $this->roles[] = array_shift($this->groups->getRoles());

        return $this;
    }
}
