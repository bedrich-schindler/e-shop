<?php

namespace AppBundle\Entity;

/**
 * Class OrderDeliveryItem
 * @package AppBundle\Entity
 */
class OrderDeliveryItem
{
    /**
     * @var integer Id of order delivery item
     */
    protected $id;
    /**
     * @var string Method title of order delivery item
     */
    protected $method;
    /**
     * @var string Format of order delivery item (Acceptable - ulozenka|null)
     */
    protected $format;
    /**
     * @var integer Fee of order delivery item
     */
    protected $fee;
    /**
     * @var Delivery Original delivery entity
     */
    protected $deliveryId;

    /**
     * Get id of order delivery item
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method title of order delivery item
     *
     * @param string $method
     * @return Delivery
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method of of order delivery item
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set format of of order delivery item (Acceptable - ulozenka|null)
     *
     * @param string $format
     * @return Delivery
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format of of order delivery item (Acceptable - ulozenka|null)
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set fee of order delivery item
     *
     * @param integer $fee
     * @return Delivery
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee of order delivery item
     *
     * @return integer
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set original delivery entity
     *
     * @param Delivery $deliveryId
     * @return OrderDiscountItem
     */
    public function setDeliveryId(Delivery $deliveryId = null)
    {
        $this->deliveryId = $deliveryId;

        return $this;
    }

    /**
     * Get original delivery entity
     *
     * @return Delivery
     */
    public function getDeliveryId()
    {
        return $this->deliveryId;
    }
}
