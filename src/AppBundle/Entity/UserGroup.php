<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Entity\Group as BaseGroup;

/**
 * Class UserGroup
 * @package AppBundle\Entity
 */
class UserGroup extends BaseGroup
{
    /**
     * @var integer Id of user group
     */
    protected $id;

    /**
     * Constructor
     *
     * @param $name
     */
    public function __construct($name)
    {
        parent::__construct($name);
    }

    /**
     * Get id of user group
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
