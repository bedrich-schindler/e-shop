<?php

namespace AppBundle\Entity;

/**
 * Class Payment
 * @package AppBundle\Entity
 */
class Payment
{
    /**
     * @var integer Id of payment.
     */
    protected $id;
    /**
     * @var string Method title of payment
     */
    protected $method;
    /**
     * @var integer Fee of payment
     */
    protected $fee;

    /**
     * Get id of payment
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set method title of payment
     *
     * @param string $method
     * @return Payment
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method title of payment
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set fee of payment
     *
     * @param integer $fee
     * @return Payment
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee of payment
     *
     * @return integer
     */
    public function getFee()
    {
        return $this->fee;
    }
}
