<?php

namespace AppBundle\Entity;

use Gregwar\Image\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class Text
 * @package AppBundle\Entity
 */
class Text
{
    /**
     * @var integer Id of text
     */
    protected $id;
    /**
     * @var string Title of text (or image)
     */
    protected $title;
    /**
     * @var string Content of text
     */
    protected $content;
    /**
     * @var Sorting Original sorting entity
     */
    protected $sorting;
    /**
     * @var bool This text is of type text (true) or of type image (false)
     */
    protected $isText = true;
    /**
     * @var string Link (URL) of image link
     */
    protected $link;
    /**
     * @var \DateTime Date and time when text (or image) was written on
     */
    protected $writtenOn;
    /**
     * @var \AppBundle\Entity\User User which text (or image) was written by
     */
    protected $writtenBy;
    /**
     * @var  \DateTime Date and time when text (or image) was edited on
     */
    protected $editedOn;
    /**
     * @var \AppBundle\Entity\User User which text (or image) was edited by
     */
    protected $editedBy;
    /**
     * @var UploadedFile Image file during upload
     */
    protected $file;
    /**
     * @var string Extension of image
     */
    protected $extension;
    /**
     * @var string Temporary path to image file during upload
     */
    protected $temp;

    /**
     * Get id of text (or image)
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title of text (or image)
     *
     * @param string $title
     * @return Text
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title of text (or image)
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content of text
     *
     * @param string $content
     * @return Text
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content of text
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set original sorting entity
     *
     * @param Sorting $sorting
     * @return Text
     */
    public function setSorting(Sorting $sorting = null)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get original sorting entity
     *
     * @return Sorting
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Set if this text is of type text (true) or of type image (false)
     *
     * @param boolean $isText
     * @return Text
     */
    public function setIsText($isText)
    {
        $this->isText = $isText;

        return $this;
    }

    /**
     * Get if this text is of type text (true) or of type image (false)
     *
     * @return boolean
     */
    public function getIsText()
    {
        return $this->isText;
    }

    /**
     * Set link (URL) of image link
     *
     * @param string $link
     * @return Text
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link (URL) of image link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set date and time when text was written on
     *
     * @param \DateTime $writtenOn
     * @return Text
     */
    public function setWrittenOn($writtenOn)
    {
        $this->writtenOn = $writtenOn;

        return $this;
    }

    /**
     * Get date and time when text was written on
     *
     * @return \DateTime
     */
    public function getWrittenOn()
    {
        return $this->writtenOn;
    }

    /**
     * Set user which text was written by
     *
     * @param \DateTime $editedOn
     * @return Text
     */
    public function setEditedOn($editedOn)
    {
        $this->editedOn = $editedOn;

        return $this;
    }

    /**
     * Get user which text was written by
     *
     * @return \DateTime
     */
    public function getEditedOn()
    {
        return $this->editedOn;
    }

    /**
     * Set date and time when text was edited on
     *
     * @param \AppBundle\Entity\User $writtenBy
     * @return Text
     */
    public function setWrittenBy(\AppBundle\Entity\User $writtenBy)
    {
        $this->writtenBy = $writtenBy;

        return $this;
    }

    /**
     * Get date and time when text was edited on
     *
     * @return \AppBundle\Entity\User
     */
    public function getWrittenBy()
    {
        return $this->writtenBy;
    }

    /**
     * Set user which text was edited by
     *
     * @param \AppBundle\Entity\User $editedBy
     * @return Text
     */
    public function setEditedBy(\AppBundle\Entity\User $editedBy = null)
    {
        $this->editedBy = $editedBy;

        return $this;
    }

    /**
     * Get user which text was edited by
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditedBy()
    {
        return $this->editedBy;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (is_file($this->getAbsolutePath())) {
            // store the old name to delete after the update
            $this->temp = $this->getAbsolutePath();
        } else {
            $this->extension = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Handle change between text and image type
     */
    public function prePersist()
    {
        if ($this->isText) {
            $this->storeFilenameForRemove();
            $this->removeUpload();
            $this->setFile(null);
            $this->setExtension(null);
            $this->setLink(null);
        } else {
            $this->setContent(null);
        }
    }

    /**
     * Guess extension of file.
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            $this->extension = $this->getFile()->guessExtension();
        }
    }

    /**
     * Upload image file
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        if (isset($this->temp)) {
            unlink($this->temp);
            $this->temp = null;
        }

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->id . '.' . $this->getFile()->guessExtension()
        );

        Image::open($this->getAbsolutePath())
            ->resize(890)
            ->save($this->getAbsolutePath());

        $this->setFile(null);
    }

    /**
     * Store filename of image file before file is removed
     */
    public function storeFilenameForRemove()
    {
        $this->temp = $this->getAbsolutePath();
    }

    /**
     * Remove image file when this entity is removed
     */
    public function removeUpload()
    {
        if (isset($this->temp) && file_exists($this->temp)) {
            unlink($this->temp);
        }
    }

    /**
     * Get absolute web path
     *
     * @return null|string
     */
    public function getAbsolutePath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadRootDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get web path
     *
     * @return null|string
     */
    public function getWebPath()
    {
        return null === $this->extension
            ? null
            : $this->getUploadDir() . '/' . $this->id . '.' . $this->extension;
    }

    /**
     * Get root upload directory
     *
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    /**
     * Get upload directory
     *
     * @return string
     */
    protected function getUploadDir()
    {
        return 'upload/images/texts';
    }

    /**
     * Set extension of image
     *
     * @param string $extension
     * @return Product
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension of image
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }
}
