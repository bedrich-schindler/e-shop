<?php

namespace AppBundle\Entity;

/**
 * Class Sorting
 * @package AppBundle\Entity
 */
class Sorting
{
    /**
     * @var integer Id of sorting
     */
    protected $id;
    /**
     * @var integer Number of position in sorting list
     */
    protected $sort;
    /**
     * @var ProductCategory Original product category entity
     */
    protected $productCategory;
    /**
     * @var Text Original text entity
     */
    protected $text;

    /**
     * Get id of sorting
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number of position in sorting list
     *
     * @param integer $sort
     * @return Sorting
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get number of position in sorting list
     *
     * @return integer
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Set original product category entity
     *
     * @param ProductCategory $productCategory
     * @return Sorting
     */
    public function setProductCategory(ProductCategory $productCategory = null)
    {
        $this->productCategory = $productCategory;

        return $this;
    }

    /**
     * Get original product category entity
     *
     * @return ProductCategory
     */
    public function getProductCategory()
    {
        return $this->productCategory;
    }

    /**
     * Set original text entity
     *
     * @param Text $text
     * @return Sorting
     */
    public function setText(Text $text = null)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get original text entity
     *
     * @return  Text
     */
    public function getText()
    {
        return $this->text;
    }
}
