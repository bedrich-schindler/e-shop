<?php

namespace AppBundle\Twig;

class MenuItemExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            'getActiveMenuItem' => new \Twig_Function_Method($this, 'activeMenuItem')
        );
    }

    public function activeMenuItem($currentRoute, $mustStartWith, $generateClassAttribute = false)
    {
        if (strpos($currentRoute, $mustStartWith) === 0) {
            if ($generateClassAttribute) {
                return ' class=active';
            } else {
                return ' active';
            }
        } else {
            return null;
        }
    }

    public function getName()
    {
        return 'menu_item_extension';
    }
}
