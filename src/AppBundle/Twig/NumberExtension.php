<?php

namespace AppBundle\Twig;

class NumberExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            'zerofill' => new \Twig_Function_Method($this, 'zerofill')
        );
    }

    public function zerofill($number, $pad_length, $pad_string)
    {
        return str_pad($number, $pad_length, $pad_string, STR_PAD_LEFT);
    }

    public function getName()
    {
        return 'number_extension';
    }
}
