;(function($) {

  'use strict';

  $(document).ready(function() {
    /**
     * Eshop class
     */
    var Eshop = Eshop || {};

    /**
     * Cookie namespace of Eshop class
     */
    Eshop.cookie = {
      /**
       * Name of cookie
       */
      cookieName: 'order',
      /**
       * Path of cookie
       */
      cookiePath: '/',
      /**
       * Expiration of cookie
       */
      cookieExpiration: 7,
      /**
       * Event name of cookie
       */
      cookieEventName: 'cookieChange',
      /**
       * Name of permission cookie
       */
      cookiePermissionName: 'cookiePermission',

      /**
       * Autohide confirmation element
       */
      autohideCookiePermissionElement: function(element, button) {
        var _this = this;
        if (this.getCookiePermission()) {
          $(element).hide();
        } else {
          $(element).show();
          $(button).click(function() {
            _this.setCookiePermission(true);
            $(element).hide();
          });
        }
      },
      /**
       * Get confirmation status of using cookie
       */
      getCookiePermission: function() {
        var permission = $.cookie(this.cookiePermissionName);

        return !(permission === undefined || !permission);
      },
      /**
       * Set confirmation status of using cookie
       */
      setCookiePermission: function(allow) {
        $.cookie(
            this.cookiePermissionName,
            allow,
            { expires: 365, path: this.cookiePath }
        );
      },
      /**
       * Get order object
       */
      getOrder: function() {
        var order = $.cookie(this.cookieName);

        if (order === undefined) {
          order = {
            products: []
          };
        } else {
          order = $.parseJSON(order);
        }

        return order;
      },
      /**
       * Set order object
       */
      setOrder: function(order) {
        $.cookie(
            this.cookieName,
            JSON.stringify(order),
            { expires: this.cookieExpiration, path: this.cookiePath }
        );
        $(document).trigger(this.cookieEventName);
      },
      /**
       * Remove order object
       */
      removeOrder: function() {
        $.removeCookie(
            this.cookieName,
            { expires: this.cookieExpiration, path: this.cookiePath }
        );
        $(document).trigger(this.cookieEventName);
      },

      /**
       * Get amount of product from order
       */
      getOrderProductAmount: function(productId) {
        var order = this.getOrder();

        for (var i = 0, len = order.products.length; i < len; i++) {
          if (order.products[i].id === productId) {
            return order.products[i].value;
          }
        }

        return null;
      },
      /**
       * Set amount of product from order
       */
      setOrderProductAmount: function(productId, productValue) {
        var order = this.getOrder();

        for (var i = 0, len = order.products.length; i < len; i++) {
          if (order.products[i].id === productId) {
            order.products[i].value = productValue;
            this.setOrder(order);

            return;
          }
        }

        order.products[len] = {id: productId, value: productValue};
        this.setOrder(order);
      },

      /**
       * Remove product from order
       */
      removeOrderProduct: function(productId) {
        var order = this.getOrder();

        for (var i = 0, len = order.products.length; i < len; i++) {
          if (order.products[i].id === productId) {
            order.products.splice(i, 1);
            Eshop.cookie.setOrder(order);

            return;
          }
        }
      }
    };

    /**
    * Router namespace of Eshop class
    */
    Eshop.router = {
      /**
       * Enable javascript router
       */
      enable: function() {
        if (this.isCategory()) {
          this.scrollToCategory(this.getCategoryAlias());
        }

        if (this.isProduct()) {
          this.scrollToProduct(this.getProductAlias());
          this.showProduct(this.getProductAlias());
        }
      },

      /**
       * Check if path is right category anchor
       */
      isCategory: function() {
        var path = location.pathname;
        var pathParameters = path.split('/');
        var pathParametersCount = pathParameters.length;

        return pathParameters[pathParametersCount - 2] === 'kategorie';
      },
      /**
       * Check if path is right product anchor
       */
      isProduct: function() {
        var path = location.pathname;
        var pathParameters = path.split('/');
        var pathParametersCount = pathParameters.length;

        return pathParameters[pathParametersCount - 2] === 'produkt';
      },

      /**
       * Get category alias
       */
      getCategoryAlias: function() {
        var path = location.pathname;
        var pathParameters = path.split('/');
        var pathParametersCount = pathParameters.length;

        return pathParameters[pathParametersCount - 1];
      },
      /**
       * Get product alias
       */
      getProductAlias: function() {
        var path = location.pathname;
        var pathParameters = path.split('/');
        var pathParametersCount = pathParameters.length;

        return pathParameters[pathParametersCount - 1];
      },

      /**
       * Scroll to category anchor
       */
      scrollToCategory: function(alias) {
        $('html, body').animate({
          scrollTop: ($('#' + alias).offset().top - 30)
        }, 500);
      },
      /**
       * Scroll to product anchor
       */
      scrollToProduct: function(alias) {
        $('html, body').animate({
          scrollTop: ($('#' + alias).offset().top - 25)
        }, 500);
      },

      /**
       * Show product modal window
       */
      showProduct: function(alias) {
        $('#' + alias).find('[data-show-product]:first').ready(function() {
          $('#' + alias).find('[data-show-product]:first').click();
        });
      }
    };

    // Enable frontend router
    Eshop.router.enable();

    // Check for accepted cookie permission
    Eshop.cookie.autohideCookiePermissionElement('#cookies-element', '#cookies-button');

    /**
     * To decimal format
     */
    function toDecimalFormat(value) {
      return value.toFixed(2).replace('.', ',');
    }

    /**
     * Layout - sticky footer
     */
    function stickyFooterResize() {
      var footerWidth = $('#footer').height();
      var footerMarginTop = 95;
      $('.has-footer-sticky').css('margin-bottom', footerWidth + footerMarginTop);
    }

    /**
     * Call function for resizing sticky footer on window load event
     */
    $(window).load(function() {
      stickyFooterResize();
    });

    /**
     * Call function for resizing sticky footer on window resize event
     */
    $(window).resize(function() {
      stickyFooterResize();
    });

    /**
     * Save data from field
     */
    $('*[data-save]').on('click keyup change', function() {
      var order = Eshop.cookie.getOrder();

      $('*[data-save]').each(function() {
        var $this = $(this);
        var name = $this.prop('name');
        var type = $this.prop('type');
        var value = null;

        if (type === 'checkbox') {
          value = $this.prop('checked');
        } else {
          value = $this.val();
        }

        order[name] = { t: type, v: value };
      });

      Eshop.cookie.setOrder(order);
    });

    /**
    * Load data to field
    */
    var order = Eshop.cookie.getOrder();

    $('*[data-save]').each(function() {
      var $this = $(this);
      var name = $this.prop('name');

      if (order[name] !== undefined) {
        var type = order[name].t;
        var value = order[name].v;

        if (type === 'checkbox') {
          $('*[name="' + name + '"]').prop('checked', value);
        } else {
          $('*[name="' + name + '"]').val(value);
        }
      }
    });

    /**
     * Show number of products in basket
     */
    $(document).on('ready cookieChange', function() {
      $('#basket-products-count').text(function() {
        var order = $.cookie('order');
        var orderProductsCount = 0;
        if (order !== undefined) {
          order = $.parseJSON(order);

          $.each(order.products, function(index, value) {
            if (value.value !== null) {
              orderProductsCount++;
            }
          });

          if (orderProductsCount === 1) {
            return orderProductsCount + ' položka';
          } else if (orderProductsCount > 1 && orderProductsCount < 5) {
            return orderProductsCount + ' položky';
          } else {
            return orderProductsCount + ' položek';
          }
        } else {
          return '0 položek';
        }
      });
    });

    /**
     * Show product detail modal window
     */
    $('[data-show-product]').click(function() {
      var parent = $(this);
      var data = parent.data();

      /**
       * Load product data from database using ajax
       */
      $.post($('base').attr('href') + 'ajax/get-product/' + data.showProduct, function(data) {
        var response = $.parseJSON(data);

        // Code bellow fill fields in product detail modal window
        $('[data-show-product-attr="title"]').text(response.title);
        $('[data-show-product-attr="description"]').html(response.description);

        $('[data-show-product-attr="price"]').text(toDecimalFormat(response.price));
        $('[data-show-product-attr="quantity"]').text(response.quantity);

        $('[data-show-product-attr="total-quantity"]').val(Math.min(Eshop.cookie.getOrderProductAmount(response.id), response.quantity));
        $('[data-show-product-attr="total-price"]').text(toDecimalFormat(Eshop.cookie.getOrderProductAmount(response.id) * response.price));

        var defaultImageSrc = $('[data-default-image-src]').data('defaultImageSrc');
        $('[data-show-product-attr="image"]').attr('src',  defaultImageSrc + response.id + '.' + response.extension);

        $('[data-show-product-attr="discount"]').text(response.discount);

        if (response.discount === 0) {
          $('[data-show-product-attr="discount-block"]').hide();
        } else {
          $('[data-show-product-attr="discount-block"]').show();
        }

        /**
         * Check amount of opened product
         */
        $('[data-show-product-attr="total-quantity"]').off();
        $('[data-show-product-attr="total-quantity"]').on('click keyup', function() {
          var child = $(this);
          var value = child.val();

          // Skip if entered amount is not a number
          if (!$.isNumeric(value)) {
            return;
          }

          // Round amount if it not an integer
          if (value % 1 !== 0) {
            value = Math.floor(value);
            $('[data-show-product-attr="total-quantity"]').val(value);
          }

          if (value > response.quantity) {
            // If entered amount is higher than maximum amount, set maximum amount
            Eshop.cookie.setOrderProductAmount(response.id, response.quantity);
            child.val(response.quantity);
          } else if (value <= 0) {
            // If amount of product equals or is less than 0, remove product from basket
            Eshop.cookie.removeOrderProduct(response.id);
            child.val(0);
          } else {
            // Set entered amount of product
            Eshop.cookie.setOrderProductAmount(response.id, value);
          }

          // Update total price of the product
          $('[data-show-product-attr="total-price"]').text(toDecimalFormat(Eshop.cookie.getOrderProductAmount(response.id) * response.price));
        });
      });
    });

    /**
     * Remove product
     */
    $('*[data-product-remove]').click(function() {
      var $this = $(this);
      var id = $this.data('product-remove');

      // Remove product from basket
      Eshop.cookie.removeOrderProduct(id);

      $this.parent().parent().remove();
      $('*[data-order-recalculate]').click();

      // Count amout of products in basket
      var orderProductsCount = 0;
      $.each(Eshop.cookie.getOrder().products, function(index, value) {
        if (value.value !== null) {
          orderProductsCount++;
        }
      });

      // If there is no product in basket then refresh page (basket form will be hidden)
      if (orderProductsCount === 0) {
        location.reload();
      }
    });

    /**
     *  Send order (prevents multiple click)
     */
    $('#appbundle_order_list_add').click(function() {
      $('#appbundle_order_list_add').attr('disabled', true);
      $('form[name="appbundle_order_list"]').submit();
    });

    /**
     *  Cancel order
     */
    $('#appbundle_order_list_cancel').click(function() {
      Eshop.cookie.removeOrder();
      location.reload();
    });

    /**
     * Disable element function
     */
    $('*[data-disable]').click(function() {
      var $this = $(this);
      var checkbox = $($this.data('disable'));
      var checkboxValue = $this.prop('checked');

      if (checkboxValue) {
        checkbox.prop('disabled', false);
      } else {
        checkbox.prop('disabled', true);
      }
    });
    /**
    * Disable element on document ready
    */
    $('*[data-disable]').each(function() {
      var $this = $(this);
      var checkbox = $($this.data('disable'));
      var checkboxValue = $this.prop('checked');

      if (checkboxValue) {
        checkbox.prop('disabled', false);
      } else {
        checkbox.prop('disabled', true);
      }
    });

    /**
     * Checkbox
     */
    $('*[data-checkbox]').click(function() {
      var $this = $(this);
      var checkbox = $($this.data('checkbox'));
      var checkboxValue = checkbox.prop('checked');

      checkbox.click();
      if (checkboxValue) {
        $this.removeClass('checked');
      } else {
        $this.addClass('checked');
      }
    });
    /**
     * Checkbox on document ready
     */
    $('*[data-checkbox]').each(function() {
      var $this = $(this);
      var checkbox = $($this.data('checkbox'));
      var checkboxValue = checkbox.prop('checked');

      $this.css('display', 'inline-block');
      checkbox.css('display', 'none');

      if (checkboxValue) {
        $this.addClass('checked');
      }
    });

    /**
     * Load radio on document ready
     */
    $.each(Eshop.cookie.getOrder(), function(key, value) {
      $('*[name="' + key + '"][data-id="' + value + '"]').prop('checked', true);
    });

    /**
     * Radio
     */
    $('*[data-radio]').click(function() {
      var $this = $(this);
      var radio = $($this.data('radio'));

      radio.click();

      $('*[data-radio]').each(function() {
        var child = $(this);
        var childRadio = $(child.data('radio'));
        var childRadioValue = childRadio.prop('checked');

        if (childRadioValue) {
          child.addClass('checked');
        } else {
          child.removeClass('checked');
        }
      });
    });
    /**
     * Radio on document ready
     */
    $('*[data-radio]').each(function() {
      var $this = $(this);
      var radio = $($this.data('radio'));
      var radioValue = radio.prop('checked');

      $this.css('display', 'inline-block');
      radio.css('display', 'none');

      if (radioValue) {
        $this.addClass('checked');
      }
    });

    /**
     * Auto hiding
     */
    function autoHiding(element)
    {
      var id = element.data('hiding-element');

      if (element.is(':checkbox')) {
        if (!element.prop('checked')) {
          $('*[data-hiding-true="' + id + '"]').show();
          $('*[data-hiding-false="' + id + '"]').hide();
        } else {
          $('*[data-hiding-false="' + id + '"]').show();
          $('*[data-hiding-true="' + id + '"]').hide();
        }
      } else {
        if (element.val() !== '0') {
          $('*[data-hiding-true="' + id + '"]').show();
          $('*[data-hiding-false="' + id + '"]').hide();
        } else {
          $('*[data-hiding-false="' + id + '"]').show();
          $('*[data-hiding-true="' + id + '"]').hide();
        }
      }
    }
    /**
     * Auto hiding on load
     */
    (function() {
      var autoHidingElement = $('*[data-hiding-element]');

      autoHiding(autoHidingElement);
      autoHidingElement.on('click change', function() {
        autoHiding($(this));
      });
    })();

    /**
     * Order data filler
     */
    $('*[type="submit"]').click(function() {
      $('*[data-order-filler]').each(function() {
        var $this = $(this);
        var filler = $($this.data('order-filler'));

        if (!$('input[type="checkbox"][data-order-filler-check]').prop('checked')) {
          filler.val($this.val());
        }
      });
    });

    /**
     * Check if entered discount in basket is valid
     */
    $('*[data-discount]').on('keyup change', function() {
      var $this = $(this);

      $.post('nakupni-kosik/ajax/sleva/', { discountCode: $this.val()}, function(data) {
        var response = $.parseJSON(data);

        $this.data('discount', response.discountId);

        if (response.discountExists === null) {
          $this.removeClass();
        } else if (response.discountExists === true) {
          $this.removeClass();
          $this.addClass('fill-success');
        } else {
          $this.removeClass();
          $this.addClass('fill-error');
        }

        $this.click();
      });
    });

    /**
     * Check on window load if loaded discount from cookie is valid
     */
    $('*[data-discount]').keyup();

    /**
     * Change basket items and recalculate basket
     */
    $('*[data-order-recalculate]').on('click keyup', function() {
      var $this = $(this);
      var name = $this.data('order-recalculate');
      var value = $this.val();

      if ($this.is(':radio')) {
        // Change delivery or payment on radio button selection change
        var id = $this.data('id');

        var order = Eshop.cookie.getOrder();
        order[name] = id;
        Eshop.cookie.setOrder(order);
      } else {
        if (name === 'product') {
          // Change amount of product in basket
          if (!$.isNumeric($this.val())) {
            return;
          }

          // Skip if entered amount is not a number
          if ($this.val() % 1 !== 0) {
            $this.val(Math.floor($this.val()));
          }

          // Product details
          var productId = $this.data('product-id');
          var productAvailable = $this.data('product-available');

          if (value > productAvailable) {
            // If entered amount is higher than maximum amount, set maximum amount
            Eshop.cookie.setOrderProductAmount(productId, productAvailable);
            $this.val(productAvailable);
          } else if (value <= 0) {
            // If amount of product equals or is less than 0, remove product from basket
            Eshop.cookie.removeOrderProduct(productId);
            $this.parent().parent().remove();
          } else {
            // Set entered amount of product
            Eshop.cookie.setOrderProductAmount(productId, value);
          }
        } else {
          // Change discount in basket
          var order2 = Eshop.cookie.getOrder();
          order2[name] = $this.data('discount');
          Eshop.cookie.setOrder(order2);
        }
      }

      /**
       * Recalculate basket using ajax
       */
      $.post('nakupni-kosik/ajax/prepocitat/', function(data) {
        var response = $.parseJSON(data);
        $('#products-total-price').text(toDecimalFormat(response.productsTotalPrice));
        $('#order-total-price').text(toDecimalFormat(response.orderTotalPrice));
        $.each(response.products, function(index, value) {
          $('[data-product-total-price="' + index + '"]').text(toDecimalFormat(value));
        });
      });
    });
  });
})(jQuery);
