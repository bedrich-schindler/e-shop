<?php

namespace AppBundle\Controller\Back;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class NewsletterController
 * @package AppBundle\Controller\Back
 */
class NewsletterController extends Controller
{
    /**
     * Export existing newsletter addresses
     *
     * @return StreamedResponse
     */
    public function exportAction()
    {
        $response = new StreamedResponse();
        $response->setCallback(function () {
            $newsletterManager = $this->get('app.newsletter');
            $newsletters = $newsletterManager->findAll();

            $handle = fopen('php://output', 'w+');

            foreach ($newsletters as $newsletter) {
                fputcsv($handle, [
                    $newsletter->getEmail(),
                    $newsletter->getAddedOn()->format('Y-m-d H:i'),
                    $newsletterManager->encodeEmail($newsletter->getEmail())
                ]);
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="newsletters.csv"');

        return $response;
    }
}
