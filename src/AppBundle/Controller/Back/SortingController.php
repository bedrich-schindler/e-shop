<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\SortingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class SortingController
 * @package AppBundle\Controller\Back
 */
class SortingController extends Controller
{
    /**
     * List sorting table
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function listAction(Request $request)
    {
        $sortingManager = $this->get('app.sorting');
        $sorting = $sortingManager->findAll();

        $form = $this->createForm(
            new SortingType()
        );

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = json_decode($form->getData()['data']);
            $sortingManager->changeSorting($data);

            $this->addFlash('success', 'Řazení bylo úspěsně uloženo.');
            return $this->redirectToRoute('back_sorting_list');
        }

        return $this->render(
            'AppBundle:Back\Sorting:list.html.twig',
            [
                'form' => $form->createView(),
                'sorting' => $sorting
            ]
        );
    }
}
