<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\LinkType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class LinkController
 * @package AppBundle\Controller\Back
 */
class LinkController extends Controller
{
    /**
     * List all links
     *
     * @return Response
     */
    public function listAction()
    {
        $linkManager = $this->get('app.link');
        $links = $linkManager->findAll();

        return $this->render(
            'AppBundle:Back\Link:list.html.twig',
            ['links' => $links]
        );
    }

    /**
     * Add new link
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $linkManager = $this->get('app.link');
        $link = $linkManager->initialize();

        $form = $this->createForm(
            new LinkType(),
            $link
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $linkManager->add($link);

            $this->addFlash('success', 'Odkaz byl úspěšně přidán.');
            return $this->redirectToRoute('back_link_list');
        }

        return $this->render(
            'AppBundle:Back\Link:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing link
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $linkManager = $this->get('app.link');
        $link = $linkManager->findById($id);

        if (!$link) {
            $this->addFlash('error', 'Odkaz nebyl nalezen.');
            return $this->redirectToRoute('back_link_list');
        }

        $form = $this->createForm(
            new LinkType(),
            $link,
            [
                'validation_groups' => ['Default'],
                'edit_action' => true
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $linkManager->edit($link);

            $this->addFlash('success', 'Odkaz byl úspěšně upraven.');
            return $this->redirectToRoute('back_link_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Link:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing link
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $linkManager = $this->get('app.link');
        $link = $linkManager->findById($id);

        if (!$link) {
            $this->addFlash('error', 'Odkaz nebyl nalezen.');
            return $this->redirectToRoute('back_link_list');
        }

        $linkManager->remove($link);

        $this->addFlash('success', 'Odkaz byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_link_list');
    }
}
