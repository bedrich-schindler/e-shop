<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\PageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PageController
 * @package AppBundle\Controller\Back
 */
class PageController extends Controller
{
    /**
     * List all pages
     *
     * @return Response
     */
    public function listAction()
    {
        $pageManager = $this->get('app.page');
        $pages = $pageManager->findAll();

        return $this->render(
            'AppBundle:Back\Page:list.html.twig',
            ['pages' => $pages]
        );
    }

    /**
     * Add new page
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $pageManager = $this->get('app.page');
        $page = $pageManager->initialize();

        $form = $this->createForm(
            new PageType(),
            $page
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pageManager->add($page);

            $this->addFlash('success', 'Stránka byla úspěšně přidána.');
            return $this->redirectToRoute('back_page_list');
        }

        return $this->render(
            'AppBundle:Back\Page:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing page
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $pageManager = $this->get('app.page');
        $page = $pageManager->findById($id);

        if (!$page) {
            $this->addFlash('error', 'Stránka nebyla nalezena.');
            return $this->redirectToRoute('back_page_list');
        }

        $form = $this->createForm(
            new PageType(),
            $page,
            ['edit_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pageManager->edit($page);

            $this->addFlash('success', 'Stránka byla úspěšně upravena.');
            return $this->redirectToRoute('back_page_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Page:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing page
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $pageManager = $this->get('app.page');
        $page = $pageManager->findById($id);

        if (!$page) {
            $this->addFlash('error', 'Stránka nebyla nalezena.');
            return $this->redirectToRoute('back_page_list');
        }

        $pageManager->remove($page);

        $this->addFlash('success', 'Stránka byla úspěšně odstraněna.');
        return $this->redirectToRoute('back_page_list');
    }
}
