<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\DiscountType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class DiscountController
 * @package AppBundle\Controller\Back
 */
class DiscountController extends Controller
{
    /**
     * List all discounts
     *
     * @return Response
     */
    public function listAction()
    {
        $discountManager = $this->get('app.discount');
        $discounts = $discountManager->findAll();

        return $this->render(
            'AppBundle:Back\Discount:list.html.twig',
            ['discounts' => $discounts]
        );
    }

    /**
     * Add new discount
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $discountManager = $this->get('app.discount');
        $discount= $discountManager->initialize();

        $form = $this->createForm(
            new DiscountType(),
            $discount
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $discountManager->add($discount);

            $this->addFlash('success', 'Slevový kód byl úspěšně přidán.');
            return $this->redirectToRoute('back_discount_list');
        }

        return $this->render(
            'AppBundle:Back\Discount:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing discount
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $discountManager = $this->get('app.discount');
        $discount = $discountManager->findById($id);

        if (!$discount) {
            $this->addFlash('error', 'Slevový kód nebyl nalezen.');
            return $this->redirectToRoute('back_discount_list');
        }

        $form = $this->createForm(
            new DiscountType(),
            $discount,
            ['edit_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $discountManager->edit($discount);

            $this->addFlash('success', 'Slevový kód byl úspěšně upraven.');
            return $this->redirectToRoute('back_discount_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Discount:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing discount
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $discountManager = $this->get('app.discount');
        $discount = $discountManager->findById($id);

        if (!$discount) {
            $this->addFlash('error', 'Slevový kód nebyl nalezen.');
            return $this->redirectToRoute('back_discount_list');
        }

        $discountManager->remove($discount);

        $this->addFlash('success', 'Slevový kód byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_discount_list');
    }
}
