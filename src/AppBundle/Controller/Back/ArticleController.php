<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\ArticleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ArticleController
 * @package AppBundle\Controller\Back
 */
class ArticleController extends Controller
{
    /**
     * List all articles
     *
     * @return Response
     */
    public function listAction()
    {
        $articleManager = $this->get('app.article');
        $articles = $articleManager->findAll();

        return $this->render(
            'AppBundle:Back\Article:list.html.twig',
            ['articles' => $articles]
        );
    }

    /**
     * Add new article
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $articleManager = $this->get('app.article');
        $article = $articleManager->initialize();

        $form = $this->createForm(
            new ArticleType(),
            $article
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->add($article);

            $this->addFlash('success', 'Článek byl úspěšně přidán.');
            return $this->redirectToRoute('back_article_list');
        }

        return $this->render(
            'AppBundle:Back\Article:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing article
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $articleManager = $this->get('app.article');
        $article = $articleManager->findById($id);

        if (!$article) {
            $this->addFlash('error', 'Článek nebyl nalezen.');
            return $this->redirectToRoute('back_article_list');
        }

        $form = $this->createForm(
            new ArticleType(),
            $article,
            ['edit_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleManager->edit($article);

            $this->addFlash('success', 'Článek byl úspěšně upraven.');
            return $this->redirectToRoute('back_article_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Article:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Mark existing article as default and unmark another default articles
     *
     * @param $id
     * @return RedirectResponse
     */
    public function markDefaultAction($id)
    {
        $articleManager = $this->get('app.article');
        $article = $articleManager->findById($id);

        if (!$article) {
            $this->addFlash('error', 'Článek nebyl nalezen.');
            return $this->redirectToRoute('back_article_list');
        }

        $articleManager->markDefault($article);

        $this->addFlash('success', 'Článek byl označen jako hlavní.');
        return $this->redirectToRoute('back_article_list');
    }

    /**
     * Unmark existing article as default
     *
     * @param $id
     * @return RedirectResponse
     */
    public function unmarkDefaultAction($id)
    {
        $articleManager = $this->get('app.article');
        $article = $articleManager->findById($id);

        if (!$article) {
            $this->addFlash('error', 'Článek nebyl nalezen.');
            return $this->redirectToRoute('back_article_list');
        }

        $articleManager->unmarkDefault($article);

        $this->addFlash('success', 'Článek byl označen jako normální.');
        return $this->redirectToRoute('back_article_list');
    }

    /**
     * Remove existing article
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $articleManager = $this->get('app.article');
        $article = $articleManager->findById($id);

        if (!$article) {
            $this->addFlash('error', 'Článek nebyl nalezen.');
            return $this->redirectToRoute('back_article_list');
        }

        $articleManager->remove($article);

        $this->addFlash('success', 'Článek byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_article_list');

    }
}
