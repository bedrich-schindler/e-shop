<?php

namespace AppBundle\Controller\Back;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DashboardController
 * @package AppBundle\Controller\Back
 */
class DashboardController extends Controller
{
    /**
     * Show dashboard
     *
     * @return Response
     */
    public function dashboardAction()
    {
        $orderManager = $this->get('app.order');
        $createdOrders = $orderManager->findOrdersCreated();
        $receivedOrders = $orderManager->findOrdersReceived();
        $sentOrders = $orderManager->findOrdersSent();
        $finishedOrders = $orderManager->findOrdersFinished();

        return $this->render(
            'AppBundle:Back\Dashboard:dashboard.html.twig',
            [
                'createdOrders' => $createdOrders,
                'receivedOrders' => $receivedOrders,
                'sentOrders' => $sentOrders,
                'finishedOrders' => $finishedOrders ,
                'ordersCountThisMonth' => $orderManager->ordersOpenedCountThisMonth(),
                'ordersTotalTurnoverThisMonth' => $orderManager->ordersTotalTurnoverThisMonth(),
                'ordersAverageTurnoverThisMonth' => $orderManager->ordersAverageTurnoverThisMonth(),
                'ordersCount' => $orderManager->ordersOpenedCount(),
                'ordersTotalTurnover' => $orderManager->ordersTotalTurnover(),
                'ordersAverageTurnover' => $orderManager->ordersAverageTurnover()
            ]
        );
    }
}
