<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ProductController
 * @package AppBundle\Controller\Back
 */
class ProductController extends Controller
{
    /**
     * List all product categories
     *
     * @return Response
     */
    public function listAction()
    {
        $productManager = $this->get('app.product');
        $products = $productManager->findAll();

        return $this->render(
            'AppBundle:Back\Product:list.html.twig',
            ['products' => $products]
        );
    }

    /**
     * Add new product category
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $productManager = $this->get('app.product');
        $product = $productManager->initialize();

        $form = $this->createForm(
            new ProductType(),
            $product
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productManager->add($product);

            $this->addFlash('success', 'Produkt byl úspěšně přidán.');
            return $this->redirectToRoute('back_product_list');
        }

        return $this->render(
            'AppBundle:Back\Product:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing category
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $productManager = $this->get('app.product');
        $product = $productManager->findById($id);

        if (!$product) {
            $this->addFlash('error', 'Produkt nebyl nalezen.');
            return $this->redirectToRoute('back_product_list');
        }

        $form = $this->createForm(
            new ProductType(),
            $product,
            [
                'validation_groups' => ['Default'],
                'edit_action' => true
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productManager->edit($product);

            $this->addFlash('success', 'Produkt byl úspěšně upraven.');
            return $this->redirectToRoute('back_product_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Product:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Mark existing product as favourite
     *
     * @param $id
     * @return RedirectResponse
     */
    public function markFavouriteAction($id)
    {
        $productManager = $this->get('app.product');
        $product = $productManager->findById($id);

        if (!$product) {
            $this->addFlash('error', 'Produkt nebyl nalezen.');
            return $this->redirectToRoute('back_product_list');
        }

        $productManager->markFavourite($product);

        $this->addFlash('success', 'Produkt byl označen jako oblíbený.');
        return $this->redirectToRoute('back_product_list');
    }

    /**
     * Mark existing product as favourite
     *
     * @param $id
     * @return RedirectResponse
     */
    public function unmarkFavouriteAction($id)
    {
        $productManager = $this->get('app.product');
        $product = $productManager->findById($id);

        if (!$product) {
            $this->addFlash('error', 'Produkt nebyl nalezen.');
            return $this->redirectToRoute('back_product_list');
        }

        $productManager->unmarkFavourite($product);

        $this->addFlash('success', 'Produkt byl odznačen jako oblíbený.');
        return $this->redirectToRoute('back_product_list');
    }

    /**
     * Remove existing product category
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $productManager = $this->get('app.product');
        $product = $productManager->findById($id);

        if (!$product) {
            $this->addFlash('error', 'Produkt nebyl nalezen.');
            return $this->redirectToRoute('back_product_list');
        }

        $productManager->remove($product);

        $this->addFlash('success', 'Produkt byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_product_list');
    }
}
