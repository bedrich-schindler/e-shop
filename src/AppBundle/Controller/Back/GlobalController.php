<?php

namespace AppBundle\Controller\Back;

use AppBundle\Entity\UploadFile;
use AppBundle\Form\Type\GlobalConfigurationType;
use AppBundle\Form\Type\UploadFileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class GlobalController
 * @package AppBundle\Controller\Back
 */
class GlobalController extends Controller
{
    /**
     * Edit global settings
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function globalAction(Request $request)
    {
        $globalConfigurationManager = $this->get('app.global_configuration');
        $globalConfiguration = $globalConfigurationManager->get();

        if ($globalConfiguration == null) {
            $globalConfiguration = $globalConfigurationManager->initialize();
        }

        foreach ($globalConfigurationManager->getPaymentCollection() as $value) {
            $globalConfiguration->addPaymentCollection($value);
        }

        foreach ($globalConfigurationManager->getDeliveryCollection() as $value) {
            $globalConfiguration->addDeliveryCollection($value);
        }

        $form = $this->createForm(
            new GlobalConfigurationType(),
            $globalConfiguration
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $globalConfiguration->uploadHeaderImageFile();
            $globalConfigurationManager->set($globalConfiguration);

            $this->addFlash('success', 'Globální konfigurace byla úspěšně upravena.');
            return $this->redirectToRoute('back_global');
        }

        return $this->render(
            'AppBundle:Back\Global:global.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Upload file to shared folder
     *
     * @param Request $request
     * @return Response
     */
    public function fileUploadAction(Request $request)
    {
        $uploadFile = new UploadFile();

        $uploadFileForm = $this->createForm(
            new UploadFileType(),
            $uploadFile
        );
        $uploadFileForm->handleRequest($request);

        if ($uploadFileForm->isValid()) {
            $uploadFile->upload();

            $this->addFlash(
                'success',
                'Soubor byl úspěšně nahrán a je dostupný na '
                . $this->container->getParameter('base_url')
                . 'upload/files/share/'
                . $uploadFile->getFile()->getClientOriginalName()
            );
        }

        return $this->render(
            'AppBundle:Back\Global:fileUpload.html.twig',
            ['uploadFileForm' => $uploadFileForm->createView()]
        );
    }
}
