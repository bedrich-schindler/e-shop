<?php

namespace AppBundle\Controller\Back;

use AppBundle\Entity\OrderList;
use AppBundle\Form\Type\OrderListType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class OrderController
 * @package AppBundle\Controller\Back
 */
class OrderController extends Controller
{
    /**
     * List all order lists
     *
     * @return Response
     */
    public function listAction()
    {
        $orderManager = $this->get('app.order');
        $orders = $orderManager->findOrdersAll();

        return $this->render(
            'AppBundle:Back\Order:list.html.twig',
            ['orders' => $orders]
        );
    }

    /**
     * Show existing order list
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function showAction(Request $request, $id)
    {
        $orderManager = $this->get('app.order');
        $order = $orderManager->findOrdersById($id);

        if (!$order) {
            $this->addFlash('error', 'Objednávka nebyla nalezena.');
            return $this->redirectToRoute('back_order_list');
        }

        $oldOrderList = clone $order;
        $orderItems = $orderManager->findOrderItemsByOrder($order);

        $form = $this->createForm(
            new OrderListType(),
            $order,
            ['edit_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($order->getOrderDeliveryItem()->getMethod() !== 'Uloženka') {
                $order->setDeliveryDetail(null);
            }

            $orderManager->edit($order, $oldOrderList);

            if ($order->getStatus() === 'Sent' || $order->getStatus() === 'Finished') {
                $this->generateInvoice($order);
            }

            $this->addFlash('success', 'Objednávka byla úspěsně upravena.');
            return $this->redirectToRoute('back_order_show', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Order:show.html.twig',
            [
                'form' => $form->createView(),
                'orderItems' => $orderItems
            ]
        );
    }

    /**
     * Show invoice
     *
     * @param $orderNumber
     * @return BinaryFileResponse|RedirectResponse|Response
     */
    public function invoiceAction($orderNumber)
    {
        $orderManager = $this->get('app.order');
        $order = $orderManager->findOrderByNumber($orderNumber);

        if (!$order) {
            $this->addFlash('error', 'Objednávka nebyla nalezena.');
            return $this->redirectToRoute('back_order_list');
        }

        if ($order->getStatus() === 'Sent' || $order->getStatus() === 'Finished') {
            return $this->generateInvoice($order, false);
        } else {
            $this->addFlash('error', 'Faktura nebyla nalezena.');
            return $this->redirectToRoute('back_order_list');
        }
    }

    /**
     * Generate invoice
     *
     * @param OrderList $order
     * @param bool|true $new
     * @return BinaryFileResponse|Response
     */
    private function generateInvoice(OrderList $order, $new = true)
    {
        if ($new) {
            $globalConfigurationManager = $this->get('app.global_configuration');
            $globalConfiguration = $globalConfigurationManager->get();

            $html = $this->render(
                'AppBundle:Back\Order:invoice.html.twig',
                [
                    'order' => $order,
                    'invoiceName' => $globalConfiguration->getInvoiceName(),
                    'invoiceStreet' => $globalConfiguration->getInvoiceStreet(),
                    'invoiceCity' => $globalConfiguration->getInvoiceCity(),
                    'invoiceZipCode' => $globalConfiguration->getInvoiceZipCode(),
                    'invoiceIc' => $globalConfiguration->getInvoiceIc(),
                    'invoiceText' => $globalConfiguration->getInvoiceText(),
                    'invoiceBank' => $globalConfiguration->getInvoiceBank(),
                    'invoicePhone' => $globalConfiguration->getInvoicePhone(),
                    'invoiceEmail' => $globalConfiguration->getInvoiceEmail()
                ]
            )->getContent();

            $mpdfManager = $this->get('tfox.mpdfport');

            $mpdf = $mpdfManager->getMpdf();
            $mpdf->WriteHTML($html);
            $mpdf->Output('upload/files/invoices/' . $order->getOrderNumber(). '.pdf', 'F');
            $mpdfManager->generatePdf($html);

            return $mpdfManager->generatePdfResponse($html);
        } else {
            return new BinaryFileResponse('upload/files/invoices/' . $order->getOrderNumber(). '.pdf');
        }
    }

    /**
     * Change status of order list
     *
     * @param $id
     * @param $status
     * @return RedirectResponse
     */
    public function changeStatusAction($id, $status)
    {
        $orderManager = $this->get('app.order');
        $order = $orderManager->findOrdersById($id);
        if (!$order) {
            $this->addFlash('error', 'Objednávka nebyla nalezena.');
            return $this->redirectToRoute('back_order_list');
        }

        if ($order->getStatus() == 'Created' && $status == 'Received') {
            $orderManager->setStatusReceived($order);
        } elseif ($order->getStatus() == 'Received' && $status == 'Sent') {
            $this->generateInvoice($order);
            $orderManager->setStatusSent($order);

            $message = \Swift_Message::newInstance()
                ->setSubject('Expedice objednávky: ' . $order->getOrderNumber())
                ->setFrom(
                    $this->container->getParameter('mailer_from'),
                    $this->container->getParameter('mailer_from_name')
                )
                ->setTo($order->getDeliveryEmail())
                ->setCc($this->container->getParameter('mailer_from'))
                ->setBody(
                    $this->renderView(
                        'AppBundle:General\Email:orderExpedition.txt.twig',
                        ['order' => $order]
                    ),
                    'text/plain'
                );

            $attachPath = 'upload/files/invoices/' . $order->getOrderNumber(). '.pdf';
            $message->attach(\Swift_Attachment::fromPath($attachPath));

            $this->get('mailer')->send($message);

        } elseif ($order->getStatus() == 'Sent' && $status == 'Finished') {
            $orderManager->setStatusFinished($order);
        } elseif ($status == 'Canceled') {
            $orderManager->setStatusCanceled($order);
        } else {
            $this->addFlash('error', 'Změnit stav na zadaný stav není možné.');
            return $this->redirectToRoute('back_order_show', ['id' => $id]);
        }

        $this->addFlash('success', 'Stav byl úspěšně změněn.');
        return $this->redirectToRoute('back_order_show', ['id' => $id]);

    }

    /**
     * Format date and time for invoice export
     *
     * @param $input
     * @return string
     */
    private function dateFill(\DateTime $input)
    {
        if ($input === null) {
            return '';
        } else {
            return $input->format('Y-m-d H:i');
        }
    }

    /**
     * Export all order lists
     *
     * @return StreamedResponse
     */
    public function exportAction()
    {
        $response = new StreamedResponse();
        $response->setCallback(function () {

            $orderManager = $this->get('app.order');
            $orders = $orderManager->findOrdersAll();

            $handle = fopen('php://output', 'w+');

            foreach ($orders as $order) {
                $discount = 'NULL';
                if ($order->getOrderDiscountItem()) {
                    $discount = $order->getOrderDiscountItem()->getCode();
                }

                $orderItems = '[';
                foreach ($order->getOrderItem() as $orderItem) {
                    $orderItems .= $orderItem->getTitle() . '(' . $orderItem->getTotalPrice() .' Kč), ';
                }
                $orderItems .= ']';

                fputcsv($handle, [
                    $order->getId(),
                    $order->getOrderNumber(),
                    $order->getdeliveryFirstname(),
                    $order->getdeliveryLastname(),
                    $order->getdeliveryStreet(),
                    $order->getdeliveryCity(),
                    $order->getdeliveryZipCode(),
                    $order->getdeliveryEmail(),
                    $order->getdeliveryPhone(),
                    $order->getBillingFirstname(),
                    $order->getBillingLastname(),
                    $order->getBillingStreet(),
                    $order->getBillingCity(),
                    $order->getBillingZipCode(),
                    $order->getBillingEmail(),
                    $order->getBillingPhone(),
                    $discount,
                    $order->getOrderPaymentItem()->getMethod(),
                    $order->getOrderDeliveryItem()->getMethod(),
                    $order->getDeliveryDetail(),
                    $this->dateFill($order->getOrderCreatedOn()),
                    $this->dateFill($order->getOrderReceivedOn()),
                    $this->dateFill($order->getOrderSentOn()),
                    $this->dateFill($order->getOrderFinishedOn()),
                    $this->dateFill($order->getOrderCanceledOn()),
                    $this->dateFill($order->getEmailCanceledOn()),
                    $order->getStatus(),
                    $order->getTotalPriceWithoutDiscount(),
                    $order->getTotalPrice(),
                    $orderItems
                ]);
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="orders.csv"');

        return $response;
    }
}
