<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\ProductCategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ProductCategoryController
 * @package AppBundle\Controller\Back
 */
class ProductCategoryController extends Controller
{
    /**
     * List all product categories
     *
     * @return Response
     */
    public function listAction()
    {
        $productCategoryManager = $this->get('app.product_category');
        $productCategories = $productCategoryManager->findAll();

        return $this->render(
            'AppBundle:Back\ProductCategory:list.html.twig',
            ['productCategories' => $productCategories]
        );
    }

    /**
     * Add new product category
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $productCategoryManager = $this->get('app.product_category');
        $productCategory = $productCategoryManager->initialize();

        $form = $this->createForm(
            new ProductCategoryType(),
            $productCategory
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productCategoryManager->add($productCategory);

            $sortingManager = $this->get('app.sorting');
            $sortingManager->addProductCategory($productCategory);

            $this->addFlash('success', 'Kategorie produktů byla úspěšně přidána.');
            return $this->redirectToRoute('back_product_category_list');
        }

        return $this->render(
            'AppBundle:Back\ProductCategory:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing product category
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $productCategoryManager = $this->get('app.product_category');
        $productCategory = $productCategoryManager->findById($id);

        if (!$productCategory) {
            $this->addFlash('error', 'Kategorie produktů nebyla nalezena.');
            return $this->redirectToRoute('back_product_category_list');
        }

        $form = $this->createForm(
            new ProductCategoryType(),
            $productCategory,
            ['edit_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productCategoryManager->edit($productCategory);

            $this->addFlash('success', 'Kategorie produktů byla úspěšně upravena.');
            return $this->redirectToRoute('back_product_category_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\ProductCategory:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing product category
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeAction($id)
    {
        $productCategoryManager = $this->get('app.product_category');
        $productCategory = $productCategoryManager->findById($id);

        if (!$productCategory) {
            $this->addFlash('error', 'Kategorie produktů nebyla nalezena.');
            return $this->redirectToRoute('back_product_category_list');
        }

        $productCategoryManager->remove($productCategory);

        $this->addFlash('success', 'Kategorie produktů byla úspěšně odstraněna.');
        return $this->redirectToRoute('back_product_category_list');
    }
}
