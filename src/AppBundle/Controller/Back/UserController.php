<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserController
 * @package AppBundle\Controller\Back
 */
class UserController extends Controller
{
    /**
     * @return Response
     */
    public function listAction()
    {
        $userManager = $this->container->get('app.user');
        $users = $userManager->findUsers();

        return $this->render(
            'AppBundle:Back\User:list.html.twig',
            ['users' => $users]
        );
    }

    /**
     * Add new user
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $userManager = $this->container->get('app.user');
        $user = $userManager->createUser();

        $form = $this->createForm(
            new UserType(),
            $user
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRegisteredAt(new \DateTime());
            $userManager->updateUser($user);

            $this->addFlash('success', 'Uživatel byl úspěšně přidán.');
            return $this->redirectToRoute('back_user_list');
        }

        return $this->render(
            'AppBundle:Back\User:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit account of currently logged user
     *
     * @param Request $request
     * @return Response
     */
    public function accountAction(Request $request)
    {
        $userManager = $this->container->get('app.user');
        $loggedUser = $this->get('security.token_storage')->getToken()->getUser()->getId();
        $user = $userManager->findUserBy(['id' => $loggedUser]);

        $form = $this->createForm(
            new UserType(),
            $user,
            [
                'account_action' => true,
                'validation_groups' => ['Default']
            ]
        );
        $form->get('groups')->setData($user->getGroups()[0]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            if ($plainPassword != null) {
                $user->setPlainPassword($plainPassword);
            }

            $userManager->updateUser($user);

            $this->addFlash('success', 'Účet byl úspěšně upraven.');
        }

        return $this->render(
            'AppBundle:Back\User:account.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Edit existing user
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $userManager = $this->container->get('app.user');
        $user = $userManager->findUserBy(['id' => $id]);

        if (!$user) {
            $this->addFlash('error', 'Uživatel se zadaným identifikátorem neexistuje.');
            return $this->redirectToRoute('back_user_list');
        }

        $form = $this->createForm(
            new UserType(),
            $user,
            [
                'edit_action' => true,
                'validation_groups' => ['Default']
            ]
        );
        $form->get('groups')->setData($user->getGroups()[0]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            if ($plainPassword != null) {
                $user->setPlainPassword($plainPassword);
            }

            $userManager->updateUser($user);

            $this->addFlash('success', 'Uživatel byl úspěšně upraven.');
        }

        return $this->render(
            'AppBundle:Back\User:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing user
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $userManager = $this->container->get('app.user');
        $user = $userManager->findUserBy(['id' => $id]);

        if (!$user) {
            $this->addFlash('error', 'Uživatel se zadaným identifikátorem neexistuje.');
            return $this->redirectToRoute('back_user_list');
        }

        $userManager->deleteUser($user);

        $this->addFlash('success', 'Uživatel byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_user_list');
    }
}
