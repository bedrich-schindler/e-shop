<?php

namespace AppBundle\Controller\Back;

use AppBundle\Form\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class TextController
 * @package AppBundle\Controller\Back
 */
class TextController extends Controller
{
    /**
     * List all texts
     *
     * @return Response
     */
    public function listAction()
    {
        $textManager = $this->get('app.text');
        $texts = $textManager->findAll();

        return $this->render(
            'AppBundle:Back\Text:list.html.twig',
            ['texts' => $texts]
        );
    }

    /**
     * Add new text
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function addAction(Request $request)
    {
        $textManager = $this->get('app.text');
        $text = $textManager->initialize();

        $form = $this->createForm(
            new TextType(),
            $text
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $textManager->add($text);

            $sortingManager = $this->get('app.sorting');
            $sortingManager->addText($text);

            $this->addFlash('success', 'Text byl úspěšně přidán.');
            return $this->redirectToRoute('back_text_list');
        }

        return $this->render(
            'AppBundle:Back\Text:add.html.twig',
            ['form' => $form->createView()]
        );
    }


    /**
     * Edit existing text
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, $id)
    {
        $textManager = $this->get('app.text');
        $text = $textManager->findById($id);

        if (!$text) {
            $this->addFlash('error', 'Text nebyl nalezen.');
            return $this->redirectToRoute('back_text_list');
        }

        $form = $this->createForm(
            new TextType(),
            $text,
            [
                'validation_groups' => ['Default'],
                'edit_action' => true
            ]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $textManager->edit($text);

            $this->addFlash('success', 'Text byl úspěšně upraven.');
            return $this->redirectToRoute('back_text_edit', ['id' => $id]);
        }

        return $this->render(
            'AppBundle:Back\Text:edit.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Remove existing text
     *
     * @param $id
     * @return RedirectResponse
     */
    public function removeAction($id)
    {
        $textManager = $this->get('app.text');
        $text = $textManager->findById($id);

        if (!$text) {
            $this->addFlash('error', 'Text nebyl nalezen.');
            return $this->redirectToRoute('back_text_list');
        }

        $textManager->remove($text);

        $this->addFlash('success', 'Text byl úspěšně odstraněn.');
        return $this->redirectToRoute('back_text_list');

    }
}
