<?php

namespace AppBundle\Controller\Front;

use AppBundle\Form\Type\OrderListType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class BasketController
 * @package AppBundle\Controller\Front
 */
class BasketController extends Controller
{
    /**
     * Show basket and handle sending of order list
     *
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function showAction(Request $request)
    {
        $orderManager = $this->get('app.order');
        $order = $orderManager->initialize();

        $orderItemList = new ArrayCollection();
        $orderCookie = $request->cookies->get('order');

        if ($orderCookie != null) {
            $orderCookieObject = json_decode($orderCookie);

            $orderItemList = $orderManager->createItemList($orderCookieObject);

            $order->setOrderItem($orderItemList);
            $order->setPayment($orderManager->loadPayment($orderCookieObject));
            $order->setDelivery($orderManager->loadDelivery($orderCookieObject));
            $order->setDiscount($orderManager->loadDiscount($orderCookieObject));
        }

        $form = $this->createForm(
            new OrderListType(),
            $order,
            ['front_action' => true]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $globalConfigurationManager = $this->get('app.global_configuration');
            $globalConfiguration = $globalConfigurationManager->get();

            $order->resetOrderItem();
            $orderManager->add($order);
            $orderManager->addItemList($order, $orderItemList);
            $order->setOrderItem($orderItemList);

            $orderNumberEncoded = $orderManager->encodeOrderNumber($order->getOrderNumber());

            $message = \Swift_Message::newInstance()
                ->setSubject('Potvrzení objednávky: ' . $order->getOrderNumber())
                ->setFrom(
                    $this->container->getParameter('mailer_from'),
                    $this->container->getParameter('mailer_from_name')
                )
                ->setTo($order->getDeliveryEmail())
                ->setCc($this->container->getParameter('mailer_from'))
                ->setBody(
                    $this->renderView(
                        'AppBundle:General\Email:orderReceipt.txt.twig',
                        [
                            'order' => $order,
                            'orderNumberEnc' => $orderNumberEncoded,
                            'invoiceBank' => $globalConfiguration->getInvoiceBank()
                        ]
                    ),
                    'text/plain'
                );
            $this->get('mailer')->send($message);

            $response = new Response();
            $response->headers->clearCookie('order');
            $response->send();

            if ($order->getNewsletter()) {
                $newsletterManager = $this->get('app.newsletter');
                $newsletter = $newsletterManager->initialize();
                $newsletter->setEmail($order->getDeliveryEmail());

                if (!$newsletterManager->findByEmail($order->getDeliveryEmail())) {
                    $newsletterManager->add($newsletter);
                }

                $message = \Swift_Message::newInstance()
                    ->setSubject('Odběr newsletteru')
                    ->setFrom(
                        $this->container->getParameter('mailer_from'),
                        $this->container->getParameter('mailer_from_name')
                    )
                    ->setTo($order->getDeliveryEmail())
                    ->setCc($this->container->getParameter('mailer_from'))
                    ->setBody(
                        $this->renderView(
                            'AppBundle:General\Email:newsletterConfirmation.txt.twig',
                            ['emailEnc' => $newsletterManager->encodeEmail($order->getDeliveryEmail())]
                        ),
                        'text/plain'
                    );
                $this->get('mailer')->send($message);
            }

            $response = new Response();
            $cookie = new Cookie('orderFinished', $order->getOrderNumber());
            $response->headers->setCookie($cookie);
            $response->send();

            return $this->redirectToRoute('front_basket_sent');
        }

        return $this->render(
            'AppBundle:Front\Basket:basket.html.twig',
            [
                'form' => $form->createView(),
                'orderItemList' => $orderItemList
            ]
        );
    }

    /**
     * Show page when order list was sent (handled)
     *
     * @param Request $request
     * @return Response
     */
    public function sentAction(Request $request)
    {
        $orderNumber = $request->cookies->get('orderFinished');

        if ($orderNumber) {
            $response = new Response();
            $response->headers->clearCookie('orderFinished');
            $response->send();

            return $this->render(
                'AppBundle:Front\Basket:sent.html.twig',
                ['orderNumber' => $orderNumber]
            );
        } else {
            throw $this->createNotFoundException('Stránka nebyla nalezena.');
        }
    }

    /**
     * Get json response with recalculated order list
     *
     * @param Request $request
     * @return Response
     */
    public function ajaxRecalculateAction(Request $request)
    {
        $orderCookie = $request->cookies->get('order');

        if ($orderCookie != null) {
            $orderCookieObject = json_decode($orderCookie);

            $orderManager = $this->get('app.order');
            $data = $orderManager->handleAjaxRecalculate($orderCookieObject);

            return (new Response())->setContent($data);
        }

        return new Response(null);
    }

    /**
     * Get json response with discount code
     *
     * @param Request $request
     * @return Response
     */
    public function ajaxDiscountAction(Request $request)
    {
        $discountCode = $request->get('discountCode');

        $discountManager = $this->get('app.discount');
        $data = $discountManager->handleAjaxDiscount($discountCode);

        return (new Response())->setContent($data);
    }
}
