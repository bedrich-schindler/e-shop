<?php

namespace AppBundle\Controller\Front;

use AppBundle\Form\Type\NewsletterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewsletterController
 * @package AppBundle\Controller\Front
 */
class NewsletterController extends Controller
{
    /**
     * Add email address to newsletter list
     *
     * @param Request $request
     * @return Response
     */
    public function addAction(Request $request)
    {
        $newsletterManager = $this->get('app.newsletter');
        $newsletter = $newsletterManager->initialize();

        $form = $this->createForm(
            new NewsletterType(),
            $newsletter
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$newsletterManager->findByEmail($newsletter->getEmail())) {
                $newsletterManager->add($newsletter);
            }

            $message = \Swift_Message::newInstance()
                ->setSubject('Odběr newsletteru')
                ->setFrom(
                    $this->container->getParameter('mailer_from'),
                    $this->container->getParameter('mailer_from_name')
                )
                ->setTo($newsletter->getEmail())
                ->setCc($this->container->getParameter('mailer_from'))
                ->setBody(
                    $this->renderView(
                        'AppBundle:General\Email:newsletterConfirmation.txt.twig',
                        ['emailEnc' => $newsletterManager->encodeEmail($newsletter->getEmail())]
                    ),
                    'text/plain'
                );
            $this->get('mailer')->send($message);

            return $this->render('AppBundle:Front\Newsletter:finish.html.twig');
        }

        return $this->render(
            'AppBundle:Front\Newsletter:add.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * Show inline block when newsletter was added to newsletter list
     *
     * @return Response
     */
    public function finishAction()
    {
        return $this->render('AppBundle:Front\Newsletter:finish.html.twig');
    }

    /**
     * Show page when newsletter was added to newsletter list
     *
     * @return Response
     */
    public function finishExtAction()
    {
        return $this->render('AppBundle:Front\Newsletter:finishExt.html.twig');
    }

    /**
     * Remove email address from newsletter list
     *
     * @param $emailEncoded
     * @return Response
     */
    public function removeAction($emailEncoded)
    {
        $newsletterManager = $this->get('app.newsletter');
        $email = $newsletterManager->decodeEmail($emailEncoded);
        $newsletter = $newsletterManager->findByEmail($email);

        if (!$newsletter) {
            return $this->render('AppBundle:Front\Newsletter:removeError.html.twig');
        }

        $newsletterManager->remove($newsletter);

        return $this->render('AppBundle:Front\Newsletter:remove.html.twig');
    }
}
