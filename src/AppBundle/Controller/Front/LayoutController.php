<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LayoutController
 * @package AppBundle\Controller\Front
 */
class LayoutController extends Controller
{
    /**
     * Render product categories navigation
     *
     * @param Request $request
     * @return Response
     */
    public function productCategoriesNavAction(Request $request)
    {
        $productCategoriesManager = $this->get('app.product_category');
        $productCategories = $productCategoriesManager->findAllSorted();

        return $this->render(
            'AppBundle:Front\Layout:productCategoriesNav.html.twig',
            [
                'productCategories' => $productCategories,
                'url' => $request->server->get('PHP_SELF')
            ]
        );
    }

    /**
     * Render pages navigation
     *
     * @return Response
     */
    public function pagesNavAction()
    {
        $pagesManager = $this->get('app.page');
        $pages = $pagesManager->findAll();

        return $this->render(
            'AppBundle:Front\Layout:pagesNav.html.twig',
            ['pages' => $pages]
        );
    }

    /**
     * Render links navigation
     *
     * @return Response
     */
    public function linksNavAction()
    {
        $linksManager = $this->get('app.link');
        $links = $linksManager->findAll();

        return $this->render(
            'AppBundle:Front\Layout:linksNav.html.twig',
            ['links' => $links]
        );
    }
}
