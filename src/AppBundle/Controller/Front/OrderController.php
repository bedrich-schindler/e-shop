<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OrderController
 * @package AppBundle\Controller\Front
 */
class OrderController extends Controller
{
    /**
     * Show status of order list
     *
     * @param $orderNumberEncoded
     * @return Response
     */
    public function statusAction($orderNumberEncoded)
    {
        $orderManager = $this->get('app.order');
        $orderNumber = $orderManager->decodeOrderNumber($orderNumberEncoded);
        $order = $orderManager->findOrderByNumber($orderNumber);

        return $this->render(
            'AppBundle:Front\Order:status.html.twig',
            ['order' => $order]
        );
    }

    /**
     * Cancel order list
     *
     * @param $orderNumberEncoded
     * @return Response
     */
    public function cancelAction($orderNumberEncoded)
    {
        $orderManager = $this->get('app.order');
        $orderNumber = $orderManager->decodeOrderNumber($orderNumberEncoded);
        $order = $orderManager->findOrderByNumber($orderNumber);

        $wasActive = $order->getActive();

        if ($order && $order->getActive()) {
            $orderManager->setStatusCanceled($order, true);
        }

        return $this->render(
            'AppBundle:Front\Order:cancel.html.twig',
            [
                'order' => $order,
                'wasActive' => $wasActive
            ]
        );
    }
}
