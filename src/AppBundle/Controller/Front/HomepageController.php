<?php

namespace AppBundle\Controller\Front;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class HomepageController
 * @package AppBundle\Controller\Front
 */
class HomepageController extends Controller
{
    /**
     * Show homepage
     *
     * @return Response
     */
    public function homepageAction()
    {
        $productManager = $this->get('app.product');
        $products = $productManager->findAllFavourite();

        $sortingManager = $this->get('app.sorting');
        $sortings = $sortingManager->findAll();

        $articleManager = $this->get('app.article');
        $article = $articleManager->findDefault();

        return $this->render(
            'AppBundle:Front\Homepage:homepage.html.twig',
            [
                'favouriteProducts' => $products,
                'sortings' => $sortings,
                'article' => $article
            ]
        );
    }

    /**
     * Show homepage with specific product in address
     *
     * @param $product
     * @return Response
     */
    public function productAction($product)
    {
        return $this->homepageAction();
    }

    /**
     * Show homepage with specific product category in address
     *
     * @param $category
     * @return Response
     */
    public function categoryAction($category)
    {
        return $this->homepageAction();
    }

    /**
     * Show page
     *
     * @param $alias
     * @return Response
     */
    public function pageAction($alias)
    {
        $pageManager = $this->get('app.page');
        $page = $pageManager->findByAlias($alias);

        if (!$page) {
            throw $this->createNotFoundException('Stránka nebyla nalezena.');
        }

        return $this->render(
            'AppBundle:Front\Homepage:page.html.twig',
            ['page' => $page]
        );
    }

    /**
     * Get json response with product details
     *
     * @param $id
     * @return Response
     */
    public function ajaxGetProductAction($id)
    {
        $productManager = $this->get('app.product');
        $productJson = $productManager->findById($id)->getJson();

        return (new Response())->setContent($productJson);
    }
}
