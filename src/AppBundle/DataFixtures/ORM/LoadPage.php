<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Page;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPage extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $pageOne = new Page();
        $pageOne->setTitle('Obchodní podmínky');
        $pageOne->setAlias('obchodni-podminky');
        $pageOne->setContent($this->getPageOneContent());
        $pageOne->setWrittenOn(new \DateTime());
        $pageOne->setWrittenBy($this->getReference('user-administrator'));

        $pageTwo = new Page();
        $pageTwo->setTitle('Kontakty');
        $pageTwo->setAlias('kontakty');
        $pageTwo->setContent($this->getPageTwoContent());
        $pageTwo->setWrittenOn(new \DateTime());
        $pageTwo->setWrittenBy($this->getReference('user-administrator'));

        $pageThree = new Page();
        $pageThree->setTitle('Informace');
        $pageThree->setAlias('informace');
        $pageThree->setContent($this->getPageThreeContent());
        $pageThree->setWrittenOn(new \DateTime());
        $pageThree->setWrittenBy($this->getReference('user-administrator'));

        $pageFour = new Page();
        $pageFour->setTitle('Soubory cookie');
        $pageFour->setAlias('soubory-cookie');
        $pageFour->setContent($this->getPageFourContent());
        $pageFour->setWrittenOn(new \DateTime());
        $pageFour->setWrittenBy($this->getReference('user-administrator'));

        $pageFive = new Page();
        $pageFive->setTitle('Doprava a platba');
        $pageFive->setAlias('doprava-a-platba');
        $pageFive->setContent($this->getPageFiveContent());
        $pageFive->setWrittenOn(new \DateTime());
        $pageFive->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($pageOne);
        $manager->persist($pageTwo);
        $manager->persist($pageThree);
        $manager->persist($pageFour);
        $manager->persist($pageFive);

        $manager->flush();

        $this->addReference('page-one', $pageOne);
        $this->addReference('page-two', $pageTwo);
        $this->addReference('page-three', $pageThree);
        $this->addReference('page-four', $pageFour);
        $this->addReference('page-five', $pageFour);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }

    public function getPageOneContent()
    {
        return '
<h1>Obchodn&iacute; podm&iacute;nky</h1>
<h2>Preambule</h2>
<p><strong>N&iacute;že uveden&eacute; odstavce v&scaron;eobecn&yacute;ch obecn&yacute;ch podm&iacute;nek jsou
smy&scaron;len&eacute;a obsahuj&iacute; smy&scaron;len&eacute; jm&eacute;na, adresy, identifikačn&iacute; č&iacute;sla
,konktakty. Jak&eacute;koliv nakoupen&eacute; zbož&iacute; na tomto e-shopu nebude doručeno. Jedn&aacute; se pouze o
smy&scaron;lenou uk&aacute;zkovou webovou aplikaci k dlouhodob&eacute; maturitn&iacute; pr&aacute;ci.</strong></p>
<p>Tyto v&scaron;eobecn&eacute; obchodn&iacute; podm&iacute;nky (d&aacute;le t&eacute;ž VOP) plat&iacute; pro
n&aacute;kup v internetov&eacute;m obchodě&nbsp;online-shop.bsnet.cz, kter&yacute; je provozov&aacute;n Jan
Nov&aacute;k, IČ:12345678, se s&iacute;dlem Obecn&aacute; 123,&nbsp;Nov&eacute; Město, 456 78. Tyto VOP
upravuj&iacute; vztahy mezi Kupuj&iacute;c&iacute;m a Prod&aacute;vaj&iacute;c&iacute;m v oblasti prodeje&nbsp;
zbož&iacute; mezi Jan Nov&aacute;k, IČ:12345678, se s&iacute;dlem Obecn&aacute; 123, Nov&eacute; Město, 456 78
(d&aacute;le&nbsp;jen &bdquo;Prod&aacute;vaj&iacute;c&iacute;&ldquo;) a jej&iacute;mi obchodn&iacute;mi partnery
(d&aacute;le jen &bdquo;Kupuj&iacute;c&iacute;&ldquo;).</p>
<p>Ve&scaron;ker&eacute; smluvn&iacute; vztahy jsou uzavřeny v souladu s pr&aacute;vn&iacute;m ř&aacute;dem
Česk&eacute;
 republiky. Kupuj&iacute;c&iacute;&nbsp;pod&aacute;n&iacute;m objedn&aacute;vky potvrzuje, že se sezn&aacute;mil s
 těmito obchodn&iacute;mi podm&iacute;nkami, jejichž&nbsp;ned&iacute;lnou souč&aacute;st tvoř&iacute;
 reklamačn&iacute;
 ř&aacute;d, a že s nimi souhlas&iacute;. Na tyto obchodn&iacute; podm&iacute;nky&nbsp;je Kupuj&iacute;c&iacute;
 dostatečn&yacute;m způsobem před vlastn&iacute;m uskutečněn&iacute;m objedn&aacute;vky upozorněn&nbsp;a m&aacute;
 možnost se s nimi sezn&aacute;mit.</p>
<h2>Definice</h2>
<h3>Prod&aacute;vaj&iacute;c&iacute;</h3>
<p>Prod&aacute;vaj&iacute;c&iacute;m je Jan Nov&aacute;k, IČ:12345678, se s&iacute;dlem Obecn&aacute; 123, Nov&eacute;
Město, 456 78.</p>
<h3>Kupuj&iacute;c&iacute;</h3>
<p>Kupuj&iacute;c&iacute;m je spotřebitel nebo podnikatel.</p>
<p><strong>Spotřebitelem</strong> je fyzick&aacute; osoba, kter&aacute; při uzav&iacute;r&aacute;n&iacute; a
plněn&iacute; kupn&iacute; Smlouvy s Prod&aacute;vaj&iacute;c&iacute;m&nbsp;nejedn&aacute; v r&aacute;mci
sv&eacute; obchodn&iacute; nebo jin&eacute; podnikatelsk&eacute; činnosti nebo v r&aacute;mci
samostatn&eacute;ho&nbsp;v&yacute;konu sv&eacute;ho povol&aacute;n&iacute;. Spotřebitel při zah&aacute;jen&iacute;
obchodn&iacute;ch vztahů před&aacute;v&aacute; Prod&aacute;vaj&iacute;c&iacute;mu&nbsp;pouze sv&eacute;
kontaktn&iacute;
&uacute;daje, nutn&eacute; pro bezprobl&eacute;mov&eacute; vyř&iacute;zen&iacute; objedn&aacute;vky, popř&iacute;padě
&uacute;daje,&nbsp;kter&eacute; chce m&iacute;t uvedeny na n&aacute;kupn&iacute;ch dokladech.</p>
<p>Pr&aacute;vn&iacute; vztahy Prod&aacute;vaj&iacute;c&iacute;ho se spotřebitelem v&yacute;slovně neupraven&eacute;
těmito VOP se ř&iacute;d&iacute;&nbsp;př&iacute;slu&scaron;n&yacute;mi ustanoven&iacute;mi z&aacute;k. č.
40/1964 Sb.,
občansk&yacute; z&aacute;kon&iacute;k a z&aacute;k. č. 634/1992 Sb.,&nbsp;o ochraně spotřebitele, oba v
platn&eacute;m
zněn&iacute;, jakož i předpisy souvisej&iacute;c&iacute;mi.</p>
<p><strong>Podnikatelem</strong> se rozum&iacute;:</p>
<ul>
	<li>osoba zapsan&aacute; v obchodn&iacute;m rejstř&iacute;ku (předev&scaron;&iacute;m obchodn&iacute;
	společnosti),</li>
	<li>
	<p>osoba, kter&aacute; podnik&aacute; na z&aacute;kladě živnostensk&eacute;ho opr&aacute;vněn&iacute;
	(živnostn&iacute;k zapsan&yacute;&nbsp;v živnostensk&eacute;m rejstř&iacute;ku),</p>
	</li>
	<li>
	<p>osoba, kter&aacute; podnik&aacute; na z&aacute;kladě jin&eacute;ho než živnostensk&eacute;ho
	opr&aacute;vněn&iacute; podle zvl&aacute;&scaron;tn&iacute;ch&nbsp;předpisů (sem patř&iacute; např.
	svobodn&aacute;
	povol&aacute;n&iacute; jako advokacie apod.), a</p>
	</li>
	<li>
	<p>osoba, kter&aacute; provozuje zemědělskou v&yacute;robu a je zaps&aacute;na do evidence podle
	zvl&aacute;&scaron;tn&iacute;ho&nbsp;předpisu.</p>
	</li>
</ul>
<p>Pr&aacute;vn&iacute; vztahy Prod&aacute;vaj&iacute;c&iacute;ho s Kupuj&iacute;c&iacute;m, kter&yacute;
je podnikatel, v&yacute;slovně neupraven&eacute; těmito&nbsp;VOP ani Smlouvou mezi
Prod&aacute;vaj&iacute;c&iacute;m a
Kupuj&iacute;c&iacute;m se ř&iacute;d&iacute; př&iacute;slu&scaron;n&yacute;mi ustanoven&iacute;mi
z&aacute;k.&nbsp;
č. 513/1991 Sb., obchodn&iacute; z&aacute;kon&iacute;k v platn&eacute;m zněn&iacute;, jakož i předpisy
souvisej&iacute;c&iacute;mi.</p>
<p>Individu&aacute;ln&iacute; Smlouva Prod&aacute;vaj&iacute;c&iacute;ho s Kupuj&iacute;c&iacute;m je nadřazena
obchodn&iacute;m podm&iacute;nk&aacute;m.</p>
<h3>Spotřebitelsk&aacute; Smlouva</h3>
<p>Smlouva kupn&iacute;, o d&iacute;lo, př&iacute;padně jin&eacute; smlouvy dle občansk&eacute;ho
z&aacute;kon&iacute;ku, pokud smluvn&iacute;mi&nbsp;stranami jsou na jedn&eacute; straně spotřebitel a na
druh&eacute;
straně dodavatel, resp. Prod&aacute;vaj&iacute;c&iacute;.</p>
<h2>Zpracov&aacute;n&iacute; osobn&iacute;ch &uacute;dajů</h2>
<p>Ve&scaron;ker&eacute; nakl&aacute;d&aacute;n&iacute; s osobn&iacute;mi &uacute;daji Kupuj&iacute;c&iacute;ch se
ř&iacute;d&iacute; z&aacute;konem č. 101/2000 Sb., o ochraně&nbsp;osobn&iacute;ch &uacute;dajů ve zněn&iacute;
pozděj&scaron;&iacute;ch předpisů a ostatn&iacute;mi pr&aacute;vn&iacute;mi předpisy platn&yacute;mi na
&uacute;zem&iacute;&nbsp;ČR. Kupuj&iacute;c&iacute; sv&yacute;m svobodn&yacute;m rozhodnut&iacute;m (stiskem
tlač&iacute;tka Odeslat objedn&aacute;vku) d&aacute;v&aacute; najevo, že si je vědom v&scaron;ech v&yacute;&scaron;e
uveden&yacute;ch skutečnost&iacute; a souhlas&iacute; s dal&scaron;&iacute;m&nbsp;zpracov&aacute;n&iacute;m
sv&yacute;ch
osobn&iacute;ch &uacute;dajů pro &uacute;čely obchodn&iacute; činnosti provozovatele tohoto eshopu.
&nbsp;Poskytnut&iacute; osobn&iacute;ch &uacute;dajů je dobrovoln&eacute;, m&aacute;te pr&aacute;vo na
př&iacute;stup k &uacute;dajům a n&aacute;lež&iacute;&nbsp;V&aacute;m ochrana pr&aacute;v v rozsahu
stanoven&eacute;m
z&aacute;konem. Tento souhlas můžete kdykoli p&iacute;semně&nbsp;odvolat. Osobn&iacute; &uacute;daje budou plně
zabezpečeny proti zneužit&iacute;&nbsp;a nebudou sd&iacute;lena s aplikacemi třet&iacute;ch osob.</p>
<p>Kupuj&iacute;c&iacute; poskytnut&iacute;m uveden&yacute;ch osobn&iacute;ch &uacute;dajů a odkliknut&iacute;m
potvrzuj&iacute;c&iacute; ikony dobrovolně&nbsp;souhlas&iacute; s t&iacute;m, aby &uacute;daje poskytnut&eacute;
v rozsahu, kter&eacute; v konkr&eacute;tn&iacute;m př&iacute;padě vyplnil či kter&eacute; byly&nbsp;o něm
z&iacute;sk&aacute;ny na z&aacute;kladě uzavřen&eacute; kupn&iacute; Smlouvy či v r&aacute;mci
prohl&iacute;žen&iacute;
internetov&yacute;ch str&aacute;nek&nbsp;provozovatele, byly zpracov&aacute;ny Provozovatelem a
Prod&aacute;vaj&iacute;c&iacute;m, jej&iacute;ž &uacute;daje jsou uvedeny&nbsp;v Preambuli těchto
podm&iacute;nek,
jakožto spr&aacute;vcem za &uacute;čelem nab&iacute;zen&iacute; služeb a produktů
spr&aacute;vce,&nbsp;zas&iacute;l&aacute;n&iacute; informac&iacute; o činnosti spr&aacute;vce, a to i
elektronick&yacute;mi prostředky (zejm&eacute;na e-mail,&nbsp;SMS zpr&aacute;vy, telemarketing) dle
z&aacute;kona č. 480/2004 Sb., na dobu do odvol&aacute;n&iacute; tohoto souhlasu&nbsp;( např. zasl&aacute;n&iacute;m
libovoln&eacute; zpr&aacute;vy na email: online-shop@bsnet.cz). Zpracov&aacute;n&iacute;m v&yacute;&scaron;e
uveden&yacute;ch
osobn&iacute;ch &uacute;dajů může spr&aacute;vce pověřit třet&iacute; osobu, jakožto zpracovatele.</p>
<p>Spr&aacute;vce t&iacute;mto informuje subjekt &uacute;dajů a poskytuje mu v&yacute;slovn&eacute; poučen&iacute;
o pr&aacute;vech&nbsp;vypl&yacute;vaj&iacute;c&iacute;ch ze z&aacute;kona č. 101/2000 Sb., o ochraně osobn&iacute;ch
&uacute;dajů, tj. zejm&eacute;na o tom,&nbsp;že poskytnut&iacute; osobn&iacute;ch &uacute;dajů spr&aacute;vci je
dobrovoln&eacute;, že subjekt &uacute;dajů m&aacute; pr&aacute;vo k jejich&nbsp;př&iacute;stupu, m&aacute;
pr&aacute;vo
v&yacute;&scaron;e uveden&yacute; souhlas kdykoliv p&iacute;semně odvolat na adrese spr&aacute;vce a
d&aacute;le&nbsp;m&aacute; pr&aacute;vo v př&iacute;padě poru&scaron;en&iacute; sv&yacute;ch pr&aacute;v
obr&aacute;tit
se na &Uacute;řad pro ochranu osobn&iacute;ch &uacute;dajů&nbsp;a požadovat odpov&iacute;daj&iacute;c&iacute;
n&aacute;pravu, kterou je např. zdržen&iacute; se takov&eacute;ho jedn&aacute;n&iacute; spr&aacute;vcem,
&nbsp;odstraněn&iacute; vznikl&eacute;ho stavu, poskytnut&iacute; omluvy, proveden&iacute; opravy či doplněn&iacute;,
zablokov&aacute;n&iacute;,&nbsp;likvidace osobn&iacute;ch &uacute;dajů, zaplacen&iacute; peněžit&eacute;
n&aacute;hrady,
jakož i využit&iacute; dal&scaron;&iacute;ch pr&aacute;v vypl&yacute;vaj&iacute;c&iacute;ch&nbsp;z &sect;
11 a 21 tohoto
z&aacute;kona. Pokud si Kupuj&iacute;c&iacute; přeje opravit osobn&iacute; &uacute;daje, kter&eacute;&nbsp;o něm
Provozovatel nebo Prod&aacute;vaj&iacute;c&iacute; zpracov&aacute;v&aacute;, může jej o to pož&aacute;dat na
emailov&eacute; adrese&nbsp;online-shop@bsnet.cz nebo na v&yacute;&scaron;e uveden&eacute; po&scaron;tovn&iacute;
adrese Provozovatele nebo Prod&aacute;vaj&iacute;c&iacute;ho.</p>
<h2>Objedn&aacute;vka a uzavřen&iacute; Smlouvy</h2>
<p>Objedn&aacute;vku je Kupuj&iacute;c&iacute; opr&aacute;vněn zaslat Prod&aacute;vaj&iacute;c&iacute;mu pouze
skrze objedn&aacute;vkov&yacute; syst&eacute;m&nbsp;eshopu.&nbsp;N&aacute;vrhem k uzavřen&iacute; kupn&iacute;
Smlouvy je um&iacute;stěn&iacute; nab&iacute;zen&eacute;ho zbož&iacute; Prod&aacute;vaj&iacute;c&iacute;m na
str&aacute;nky,&nbsp;kupn&iacute; Smlouva vznik&aacute; odesl&aacute;n&iacute;m objedn&aacute;vky
Kupuj&iacute;c&iacute;m spotřebitelem a přijet&iacute;m objedn&aacute;vky&nbsp;Prod&aacute;vaj&iacute;c&iacute;m.
Toto přijet&iacute; Prod&aacute;vaj&iacute;c&iacute; neprodleně potvrd&iacute; Kupuj&iacute;c&iacute;mu
informativn&iacute;m emailem&nbsp;na zadan&yacute; email, na vznik Smlouvy v&scaron;ak toto potvrzen&iacute;
nem&aacute; vliv. Vzniklou Smlouvu (včetně&nbsp;dohodnut&eacute; ceny) lze měnit nebo ru&scaron;it pouze na
z&aacute;kladě dohody stran nebo na z&aacute;kladě&nbsp;z&aacute;konn&yacute;ch důvodů.</p>
<p>Tyto VOP jsou vyhotoveny v česk&eacute;m jazyce, přičemž kupn&iacute; smlouvu je možn&eacute; uzavř&iacute;t
t&eacute;ž pouze&nbsp;v jazyce česk&eacute;m. Spotřebitel, kter&yacute; m&aacute; trval&eacute; bydli&scaron;tě v
člensk&eacute;m st&aacute;tě Evropsk&eacute; unie&nbsp;mimo &uacute;zem&iacute; Česk&eacute; republiky, popř.
kter&yacute; je občanem člensk&eacute;ho st&aacute;tu Evropsk&eacute; unie&nbsp;mimo &uacute;zem&iacute;
Česk&eacute; republiky, potvrzen&iacute;m objedn&aacute;vky souhlas&iacute; s uzavřen&iacute;m kupn&iacute;
smlouvy&nbsp;v česk&eacute;m jazyce. Po uzavřen&iacute; kupn&iacute; smlouvy nen&iacute; možn&eacute;
zji&scaron;ťovat, zda při zpracov&aacute;n&iacute;&nbsp;dat před pod&aacute;n&iacute;m objedn&aacute;vky vznikly
 chyby,
př&iacute;padně tyto chyby opravovat. Uzavřen&aacute; kupn&iacute;&nbsp;smlouva je Prod&aacute;vaj&iacute;c&iacute;m
archivov&aacute;na a je po vyž&aacute;d&aacute;n&iacute; Kupuj&iacute;c&iacute;mu př&iacute;stupn&aacute; ve
lhůtě&nbsp;1. let ode dne jej&iacute;ho podpisu.</p>
<h2>Cena a platba</h2>
<p>Nab&iacute;dka s ceny uv&aacute;děn&eacute; na eshopu prodejce jsou smluvn&iacute;, konečn&eacute;, vždy
aktu&aacute;ln&iacute; a platn&eacute;,&nbsp;a to po dobu, kdy jsou takto Prod&aacute;vaj&iacute;c&iacute;m v
internetov&eacute;m obchodě nab&iacute;zeny. N&aacute;klady&nbsp;na dopravu jsou uvedeny v č&aacute;sti
&bdquo;Dodac&iacute; lhůta a podm&iacute;nky dod&aacute;n&iacute;&ldquo;. N&aacute;klady na
použit&iacute;&nbsp;komunikačn&iacute;ch prostředků na d&aacute;lku nese Kupuj&iacute;c&iacute;. Konečn&aacute;
kalkulovan&aacute; cena po vyplněn&iacute;&nbsp;objedn&aacute;vkov&eacute;ho formul&aacute;ře je již uvedena i
včetně
dopravn&eacute;ho. Jako cena při uzavřen&iacute; Smlouvy&nbsp;mezi prod&aacute;vaj&iacute;c&iacute;m a
kupuj&iacute;c&iacute;m plat&iacute; cena uveden&aacute; u zbož&iacute; v době objedn&aacute;v&aacute;n&iacute;
zbož&iacute; kupuj&iacute;c&iacute;m.&nbsp;Daňov&yacute; doklad na z&aacute;kladě kupn&iacute; Smlouvy mezi
Prod&aacute;vaj&iacute;c&iacute;m a&nbsp;Kupuj&iacute;c&iacute;m slouž&iacute; z&aacute;roveň i
jako&nbsp;dodac&iacute;
list. Kupuj&iacute;c&iacute; může zbož&iacute; převz&iacute;t z&aacute;sadně až po jeho &uacute;pln&eacute;m
uhrazen&iacute;, pokud nen&iacute; toto&nbsp;dohodnuto jinak.&nbsp;</p>
<p>V př&iacute;padě, kdy Kupuj&iacute;c&iacute; provede &uacute;hradu a Prod&aacute;vaj&iacute;c&iacute;
posl&eacute;ze nen&iacute; schopen zajistit dod&aacute;n&iacute;&nbsp;zbož&iacute;, vr&aacute;t&iacute;
Prod&aacute;vaj&iacute;c&iacute; neprodleně plněn&iacute; Kupuj&iacute;c&iacute;mu dohodnut&yacute;m způsobem.
Lhůta k vr&aacute;cen&iacute;&nbsp;vynaložen&yacute;ch prostředků je z&aacute;visl&aacute; na zvolen&eacute;m
způsobu jejich vr&aacute;cen&iacute;, nesm&iacute; v&scaron;ak překročit&nbsp;dobu 30 dn&iacute; od okamžiku, kdy
tato nemožnost vznikla.</p>
<p>Zbož&iacute; zůst&aacute;v&aacute; do &uacute;pln&eacute;ho zaplacen&iacute; majetkem
prod&aacute;vaj&iacute;c&iacute;ho.</p>
<p>Prod&aacute;vaj&iacute;c&iacute; akceptuje n&aacute;sleduj&iacute;c&iacute; platebn&iacute; podm&iacute;nky:</p>
<ul>
	<li>Dob&iacute;rka</li>
	<li>Převodem</li>
</ul>
<h2>Dodac&iacute; lhůta a podm&iacute;nky dod&aacute;n&iacute;.</h2>
<p>Prod&aacute;vaj&iacute;c&iacute; spln&iacute; dod&aacute;vku zbož&iacute; před&aacute;n&iacute;m tohoto
zbož&iacute; kupuj&iacute;c&iacute;mu, nebo před&aacute;n&iacute;m zbož&iacute;&nbsp;prvn&iacute;mu dopravci,
t&iacute;m tak&eacute; přech&aacute;z&iacute; na Kupuj&iacute;c&iacute;ho nebezpeč&iacute; &scaron;kody na věci.
Dostupnost&nbsp;produktu je uvedena vždy v detailu tohoto produktu. Dodac&iacute; lhůta je z&aacute;visl&aacute;
na dostupnosti&nbsp;produktu, platebn&iacute;ch podm&iacute;nk&aacute;ch a podm&iacute;nk&aacute;ch
dod&aacute;n&iacute;, a čin&iacute; maxim&aacute;lně 14 dnů. V běžn&yacute;ch&nbsp;př&iacute;padech zbož&iacute;
 expedujeme do 3 pracovn&iacute;ch dn&iacute; od uhrazen&iacute; pln&eacute; v&yacute;&scaron;e kupn&iacute; ceny.
  Konečn&yacute;&nbsp;term&iacute;n dod&aacute;n&iacute; je vždy uveden v emailu, j&iacute;mž se potvrzuje
  objedn&aacute;vka. Souč&aacute;st&iacute; dod&aacute;vky nen&iacute;&nbsp;instalace zakoupen&eacute;ho
  zbož&iacute;.
  Společně se z&aacute;silkou obdrž&iacute; Kupuj&iacute;c&iacute; daňov&yacute; doklad/fakturu.</p>
<p>V r&aacute;mci efektivity dod&aacute;n&iacute; zbož&iacute; si Prod&aacute;vaj&iacute;c&iacute; vyhrazuje možnost
zaslat Kupuj&iacute;c&iacute;mu zbož&iacute; ve v&iacute;ce&nbsp;dod&aacute;vk&aacute;ch, přičemž n&aacute;klady na
po&scaron;tovn&eacute; hrad&iacute; Kupuj&iacute;c&iacute; pouze pro prvn&iacute; dod&aacute;vku.</p>
<p>Dodac&iacute; lhůta zač&iacute;n&aacute; u zbož&iacute;, kter&eacute; bude Kupuj&iacute;c&iacute;m placeno při
převzet&iacute;, tj. na dob&iacute;rku, běžet&nbsp;dnem platn&eacute;ho uzavřen&iacute; kupn&iacute; Smlouvy
dle čl&aacute;nku IV. těchto VOP. V př&iacute;padě, že Kupuj&iacute;c&iacute; zvolil&nbsp;jinou variantu
&uacute;hrady než zaplacen&iacute; zbož&iacute; při jeho převzet&iacute;, zač&iacute;n&aacute; dodac&iacute;
lhůta běžet&nbsp;až od &uacute;pln&eacute;ho zaplacen&iacute; kupn&iacute; ceny, tj. od přips&aacute;n&iacute;
př&iacute;slu&scaron;n&eacute; č&aacute;stky na &uacute;čet Prod&aacute;vaj&iacute;c&iacute;ho.</p>
<p>Prod&aacute;vaj&iacute;c&iacute; akceptuje n&aacute;sleduj&iacute;c&iacute; dodac&iacute; podm&iacute;nky:</p>
<ul>
	<li>Česk&aacute; po&scaron;ta - doporučen&aacute; z&aacute;silka</li>
	<li>Uloženka</li>
</ul>
<p>V př&iacute;padě v&yacute;měny zbož&iacute; ve lhůtě 14-ti dnů od zakoupen&iacute; zbož&iacute; jsou
Kupuj&iacute;c&iacute;mu &uacute;čtov&aacute;ny&nbsp;n&aacute;klady na po&scaron;tovn&eacute;, a to při
každ&eacute;m
jednotliv&eacute;m zasl&aacute;n&iacute; zbož&iacute; směrem ke Kupuj&iacute;c&iacute;mu.</p>
<p>V př&iacute;padě vr&aacute;cen&iacute; zbož&iacute; Kupuj&iacute;c&iacute;m
Prod&aacute;vaj&iacute;c&iacute;mu do
14-ti dnů hrad&iacute; n&aacute;klady na po&scaron;tovn&eacute;&nbsp;Kupuj&iacute;c&iacute;.</p>
<h2>Z&aacute;ruka a servis</h2>
<p>Při prodeji spotřebn&iacute;ho zbož&iacute; je z&aacute;ručn&iacute; doba 24 měs&iacute;ců; jde-li o prodej
potravin&aacute;řsk&eacute;ho zbož&iacute;,&nbsp;je z&aacute;ručn&iacute; doba osm dn&iacute;, u prodeje krmiv
tři t&yacute;dny a u prodeje zv&iacute;řat &scaron;est t&yacute;dnů. Je-li&nbsp;na prod&aacute;van&eacute; věci,
jej&iacute;m obalu nebo n&aacute;vodu k n&iacute; připojen&eacute;m vyznačena v souladu se
zvl&aacute;&scaron;tn&iacute;mi&nbsp;pr&aacute;vn&iacute;mi předpisy lhůta k použit&iacute; věci, skonč&iacute;
z&aacute;ručn&iacute; doba uplynut&iacute;m t&eacute;to lhůty.</p>
<p>Z&aacute;ruka se nevztahuje na opotřeben&iacute; věci způsoben&eacute; jej&iacute;m obvykl&yacute;m
už&iacute;v&aacute;n&iacute;m. U věc&iacute;&nbsp;prod&aacute;van&yacute;ch za niž&scaron;&iacute; cenu se
z&aacute;ruka nevztahuje na vady, pro kter&eacute; byla niž&scaron;&iacute; cena sjedn&aacute;na.&nbsp;Jde-li o věci
použit&eacute;, neodpov&iacute;d&aacute; prod&aacute;vaj&iacute;c&iacute; za vady odpov&iacute;daj&iacute;c&iacute;
m&iacute;ře použ&iacute;v&aacute;n&iacute; nebo&nbsp;opotřeben&iacute;, kter&eacute; měla věc při převzet&iacute;
kupuj&iacute;c&iacute;m.</p>
<p>V souladu s platn&yacute;m pr&aacute;vn&iacute;m ř&aacute;dem Česk&eacute; republiky nen&iacute;
 Kupuj&iacute;c&iacute;mu, kter&yacute; je podnikatelem&nbsp;a zbož&iacute; nakupuje v souvislosti s
 podnik&aacute;n&iacute;m, poskytov&aacute;na z&aacute;ruka na zbož&iacute; mimo obecnou&nbsp;odpovědnost
 Prod&aacute;vaj&iacute;c&iacute;ho za vady zbož&iacute; při jeho před&aacute;n&iacute;. Kupuj&iacute;c&iacute;m,
 kteř&iacute; jsou podnikateli&nbsp;a kupuj&iacute; zbož&iacute; v souvislosti se svou podnikatelskou
 činnost&iacute;,
 je poskytov&aacute;na z&aacute;ručn&iacute; doba&nbsp;12 měs&iacute;ců.</p>
<p>Pro uplatněn&iacute; z&aacute;ručn&iacute; opravy je potřebn&eacute; předložit pořizovac&iacute; doklad
(&uacute;čtenka, faktura,&nbsp;Smlouva o leasingu), popř. z&aacute;ručn&iacute; list. V př&iacute;padě
zasl&aacute;n&iacute; zbož&iacute; Prodejci je nutn&eacute; zbož&iacute; zabalit&nbsp;pro přepravu takov&yacute;m
způsobem, aby nedo&scaron;lo k jeho po&scaron;kozen&iacute; během přepravy.&nbsp;Prod&aacute;vaj&iacute;c&iacute;
neposkytuje Kupuj&iacute;c&iacute;m poz&aacute;ručn&iacute; servis.</p>
<h2>Reklamačn&iacute; ř&aacute;d</h2>
<p>V př&iacute;padě, že se v průběhu z&aacute;ručn&iacute; doby vyskytne vada, m&aacute; Kupuj&iacute;c&iacute;, v
z&aacute;vislosti na povaze&nbsp;t&eacute;to vady, při uplatněn&iacute; z&aacute;ruky
n&aacute;sleduj&iacute;c&iacute;
pr&aacute;va:</p>
<p><strong>v př&iacute;padě vady odstraniteln&eacute;:</strong></p>
<ul>
	<li>pr&aacute;vo na bezplatn&eacute;, ř&aacute;dn&eacute; a včasn&eacute; odstraněn&iacute; vady</li>
	<li>pr&aacute;vo na v&yacute;měnu vadn&eacute;ho zbož&iacute; nebo vadn&eacute; souč&aacute;sti,
	nen&iacute;-li to
	vzhledem k povaze vady&nbsp;ne&uacute;měrn&eacute;</li>
	<li>v př&iacute;padě nemožnosti postupů uveden&yacute;ch v bodech a.) a b.) m&aacute; pr&aacute;vo na
	přiměřenou
	slevuz kupn&iacute; ceny nebo odstoupen&iacute; od kupn&iacute; smlouvy</li>
</ul>
<p><strong>v př&iacute;padě vady neodstraniteln&eacute;:</strong></p>
<ul>
	<li>pr&aacute;vo na v&yacute;měnu vadn&eacute;ho zbož&iacute; nebo odstoupen&iacute; od kupn&iacute; smlouvy</li>
</ul>
<p><strong>v př&iacute;padě vady odstraniteln&eacute;, pokud Kupuj&iacute;c&iacute; nemůže pro opětovn&eacute;
vyskytnut&iacute; vady&nbsp;po opravě (tzn. zbož&iacute; bylo již 3x reklamov&aacute;no pro stejnou vadu) nebo pro
vět&scaron;&iacute; počet&nbsp;vad věc ř&aacute;dně už&iacute;vat:</strong></p>
<ul>
	<li>pr&aacute;vo na v&yacute;měnu vadn&eacute;ho zbož&iacute; nebo odstoupen&iacute; od kupn&iacute; smlouvy</li>
</ul>
<p><strong>jde-li o jin&eacute; vady neodstraniteln&eacute; a nepožaduje-li spotřebitel v&yacute;měnu věci:
</strong></p>
<ul>
	<li>pr&aacute;vo na přiměřenou slevu z kupn&iacute; ceny nebo odstoupen&iacute; od kupn&iacute; smlouvy</li>
</ul>
<p>Reklamaci lze uplatnit u Prod&aacute;vaj&iacute;c&iacute;ho, a to ve v&scaron;ech jeho provozovn&aacute;ch.</p>
<p><strong>Reklamace se nevztahuj&iacute; na př&iacute;pady:</strong></p>
<ul>
	<li>vznikla-li z&aacute;vada nebo po&scaron;kozen&iacute; prokazatelně nespr&aacute;vn&yacute;m
	už&iacute;v&aacute;n&iacute;m, v rozporu&nbsp;s n&aacute;vodem k použit&iacute; anebo jin&yacute;m
	nespr&aacute;vn&yacute;m jedn&aacute;n&iacute;m Kupuj&iacute;c&iacute;ho</li>
	<li>prokazateln&yacute;ch nedovolen&yacute;ch z&aacute;sahů do zbož&iacute;</li>
	<li>na vady, kter&eacute; vznikly běžn&yacute;m opotřeben&iacute;m spotřebn&iacute;ho zbož&iacute;
	se stanovenou lhůtou&nbsp;použit&iacute; dle zvl&aacute;&scaron;tn&iacute;ch&nbsp;po uplynut&iacute;
	t&eacute;to lhůty</li>
	<li>vady způsoben&eacute; vlivem živeln&yacute;ch katastrof</li>
</ul>
<h2>Ukončen&iacute; Smlouvy</h2>
<p>Vzhledem k charakteru uzavřen&iacute; kupn&iacute; Smlouvy prostřednictv&iacute;m komunikace na
d&aacute;lku,&nbsp;m&aacute; Kupuj&iacute;c&iacute;, kter&yacute; je spotřebitelem, pr&aacute;vo odstoupit
od t&eacute;to Smlouvy bez jak&eacute;koli sankce&nbsp;ve lhůtě 14-ti dnů od převzet&iacute; zbož&iacute;.
D&aacute;le m&aacute; Kupuj&iacute;c&iacute;, kter&yacute; je spotřebitelem pr&aacute;vo odstoupit&nbsp;od
Smlouvy v souladu s ust. &sect; 53 odst. 7 a 8 z&aacute;kona č. 40/164 Sb., občansk&yacute; z&aacute;kon&iacute;k,
ve zněn&iacute;&nbsp;pozděj&scaron;&iacute;ch předpisů. V&yacute;&scaron;e uveden&eacute; se nevztahuje na
Kupuj&iacute;c&iacute;ho, kter&yacute; je podnikatelem&nbsp;a uzav&iacute;r&aacute; kupn&iacute; Smlouvu v souvislosti
se svou podnikatelskou činnost&iacute;. V př&iacute;padě v&yacute;&scaron;e&nbsp;uveden&eacute;ho odstoupen&iacute;
od smlouvy bude Prod&aacute;vaj&iacute;c&iacute;m zasl&aacute;na kupn&iacute; cena na bankovn&iacute;
&uacute;čet&nbsp;Kupuj&iacute;c&iacute;ho, kter&yacute; za t&iacute;mto &uacute;čelem Kupuj&iacute;c&iacute;
Prod&aacute;vaj&iacute;c&iacute;mu sděl&iacute;.</p>
<p>Prod&aacute;vaj&iacute;c&iacute; m&aacute; pr&aacute;vo odstoupit od Smlouvy v př&iacute;padě, kdy
Kupuj&iacute;c&iacute; neuhrad&iacute; plnou v&yacute;&scaron;i kun&iacute;&nbsp;ceny ve lhůtě 30-ti dnů ode dne
uzavřen&iacute; kupn&iacute; Smlouvy.</p>
<h2>Z&aacute;věrečn&aacute; ustanoven&iacute;</h2>
<p>Kupuj&iacute;c&iacute; umožn&iacute; Prod&aacute;vaj&iacute;c&iacute;mu plněn&iacute; povinnost&iacute; v
souladu se Nab&iacute;dkou/Smlouvou, k čemuž&nbsp;vyvine ve&scaron;kerou potřebnou součinnost.</p>
<p>Kupuj&iacute;c&iacute; se zavazuje uhradit ve&scaron;ker&eacute; n&aacute;klady vznikl&eacute;
Prod&aacute;vaj&iacute;c&iacute;mu rozes&iacute;l&aacute;n&iacute;m upom&iacute;nek&nbsp;a n&aacute;klady
spojen&eacute; s vym&aacute;h&aacute;n&iacute;m př&iacute;padn&yacute;ch pohled&aacute;vek.</p>
<p>Kupuj&iacute;c&iacute; bere na vědom&iacute;, že Prod&aacute;vaj&iacute;c&iacute; je opr&aacute;vněn postoupit
 svou
pohled&aacute;vku ze Smlouvy&nbsp;na třet&iacute; osobu.</p>
<p>Kupuj&iacute;c&iacute; bude neprodleně informovat Prod&aacute;vaj&iacute;c&iacute;ho o změně sv&yacute;ch
identifikačn&iacute;ch &uacute;dajů,&nbsp;a to nejpozději do 5 pracovn&iacute;ch dnů ode dne, kdy takov&aacute;
změna nastala.</p>
<p>Strany se zavazuj&iacute;, že vynalož&iacute; maxim&aacute;ln&iacute; &uacute;sil&iacute; na sm&iacute;rn&eacute;
 ře&scaron;en&iacute; ve&scaron;ker&yacute;ch sporů vze&scaron;l&yacute;ch&nbsp;ze Smlouvy a/nebo VOP nebo v
 souvislosti s nimi.</p>
<p>Vz&aacute;jemn&yacute; z&aacute;vazkov&yacute; vztah smluvn&iacute;ch stran se ř&iacute;d&iacute;
pr&aacute;vn&iacute;m ř&aacute;dem Česk&eacute; republiky, zejm&eacute;na&nbsp;z&aacute;konem č. 40/1964 Sb.,
 občansk&yacute;m z&aacute;kon&iacute;kem, v platn&eacute;m zněn&iacute;. Pro &uacute;čely kontraktace</p>
<p>s mezin&aacute;rodn&iacute;m prvkem t&iacute;mto v souladu s čl. III. nař&iacute;zen&iacute; č. 593/2008 o
pr&aacute;vu rozhodn&eacute;m pro smluvn&iacute; z&aacute;vazkov&eacute; vztahy, přijat&eacute;ho Evropsk&yacute;m
parlamentem a Radou Evropsk&eacute; unie&nbsp;dne 17. června 2008 (d&aacute;le jen &bdquo;Ř&iacute;m I&ldquo;), že
zvolily rozhodn&yacute;m pr&aacute;vem pro kupn&iacute; Smlouvu a tyto&nbsp;VOP pr&aacute;vo česk&eacute;, a to s
vyloučen&iacute;m použit&iacute; &bdquo;&Uacute;mluvy OSN o smlouv&aacute;ch o mezin&aacute;rodn&iacute;
koupi&nbsp;zbož&iacute;&ldquo;. Touto volbou nen&iacute; dotčen čl. VI Ř&iacute;m I, t&yacute;kaj&iacute;c&iacute;
se
spotřebitelsk&yacute;ch smluv.</p>
<p>V př&iacute;padě, že kter&eacute;koliv ustanoven&iacute; Smlouvy a/nebo VOP je nebo se stane či bude
shled&aacute;no&nbsp;neplatn&yacute;m nebo nevymahateln&yacute;m, neovlivn&iacute; to (v
nejvy&scaron;&scaron;&iacute;m
rozsahu povolen&eacute;m pr&aacute;vn&iacute;mi&nbsp;předpisy) platnost a vymahatelnost
zb&yacute;vaj&iacute;c&iacute;ch
 ustanoven&iacute; Smlouvy a/nebo VOP. Smluvn&iacute;&nbsp;strany se v takov&yacute;ch
 př&iacute;padech zavazuj&iacute;
 nahradit neplatn&eacute; či nevymahateln&eacute; ustanoven&iacute;&nbsp;ustanoven&iacute;m platn&yacute;m a
 vymahateln&yacute;m, kter&eacute; bude m&iacute;t do nejvy&scaron;&scaron;&iacute; m&iacute;ry stejn&yacute; a
 pr&aacute;vn&iacute;mi&nbsp;předpisy př&iacute;pustn&yacute; v&yacute;znam a &uacute;činek, jako byl z&aacute;měr
 ustanoven&iacute;, jež m&aacute; b&yacute;t nahrazeno.&nbsp;Z pr&aacute;vn&iacute; opatrnosti t&iacute;mto
 smluvn&iacute; strany prohla&scaron;uj&iacute;, pro př&iacute;pady kontraktace s
 mezin&aacute;rodn&iacute;m&nbsp;prvkem
  pro jak&eacute;koliv př&iacute;pady sporů (s v&yacute;jimkou sporů u nichž je d&aacute;na v&yacute;lučn&aacute;
  pravomoc&nbsp;rozhodce a/nebo v souvislosti s nimi) či po př&iacute;pady, v nichž by bylo pravomocn&yacute;m
  rozhodnut&iacute;m&nbsp;soudu shled&aacute;no, že zde nen&iacute; d&aacute;na pravomoc rozhodce dle tohoto
  čl&aacute;nku VOP, že v souladu s čl.&nbsp;23 Nař&iacute;zen&iacute; Rady (ES) č.
  44/2001 ze dne 22. prosince 2000,
  o př&iacute;slu&scaron;nosti a uzn&aacute;v&aacute;n&iacute; a v&yacute;konu&nbsp;
  soudn&iacute;ch rozhodnut&iacute;
  v občansk&yacute;ch a obchodn&iacute;ch věcech, sjedn&aacute;vaj&iacute;c&iacute; v&yacute;lučnou
  př&iacute;slu&scaron;nost&nbsp;krajsk&eacute;ho soudu v Liberci pro rozhodov&aacute;n&iacute;
  ve&scaron;ker&yacute;ch budouc&iacute;ch sporů ze Smlouvy a/nebo&nbsp;VOP a/nebo v souvislosti s nimi.
  Takt&eacute;ž smluvn&iacute; strany t&iacute;mto zakl&aacute;daj&iacute; pro ve&scaron;ker&eacute; spory&nbsp;v
  souvislosti se Smlouvou a VOP (s v&yacute;jimkou sporů u nichž je d&aacute;na v&yacute;lučn&aacute; pravomoc
  rozhodce&nbsp;a/nebo v souvislosti s nimi) v&yacute;lučnou pravomoc krajsk&eacute;ho soudu v Liberci.</p>
<p><strong>Tyto VOP nab&yacute;vaj&iacute; platnosti a &uacute;činnosti dne 15.3.2016 a jsou k dispozici
takt&eacute;ž&nbsp;na internetov&yacute;ch str&aacute;nk&aacute;ch Prod&aacute;vaj&iacute;c&iacute;ho.
Tyto VOP je Prod&aacute;vaj&iacute;c&iacute; opr&aacute;vněn kdykoliv změnit.&nbsp;VOP pak pozb&yacute;vaj&iacute;
platnosti a &uacute;činnosti dnem nabyt&iacute; &uacute;činnosti VOP pozděj&scaron;&iacute;ch.</strong></p>
';
    }

    public function getPageTwoContent()
    {
        return '
<h1>Kontakty</h1>
<p>Jan Nov&aacute;k<br />
Obecn&aacute; 123<br />
456 78&nbsp;Nov&eacute; Město</p>
<p>Telefon: +420 987 654 321</p>
<p>E-mail: <a href="mailto:online-shop@bsnet.cz">online-shop@bsnet.cz</a></p>
<p>&nbsp;</p>
<p>IČ: 12345678<br />
Prod&aacute;vaj&iacute;c&iacute; nen&iacute; pl&aacute;tcem DPH<br />
Fyzick&aacute; osoba zaps&aacute;na v Živnostensk&eacute;m rejstř&iacute;ku M&Uacute; Nov&eacute; Město</p>
';
    }

    public function getPageThreeContent()
    {
        return '
<h1>Informace</h1>
<p>Tyto webov&eacute; str&aacute;nky slouž&iacute; k testov&aacute;n&iacute; a prezentov&aacute;n&iacute;
dlouhodob&eacute; maturitn&iacute; pr&aacute;ce Bedřich Schindlera, studuj&iacute;c&iacute;ho na Středn&iacute;
průmyslov&eacute; &scaron;kole strojn&iacute; a elektrotechn&iacute;ck&eacute; a Vy&scaron;&scaron;&iacute;
odborn&eacute; &scaron;koly v Liberci.</p>
<p>Cel&yacute; obsah webu včetně obchodn&iacute;ch podm&iacute;nek je smy&scaron;len&yacute;. V&scaron;echny
proveden&eacute; objedn&aacute;vky budou využity k testovac&iacute;m a prezentačn&iacute;m &uacute;čelům
dlouhodob&eacute; maturitn&iacute; pr&aacute;ce. Jelikož se jedn&aacute; pouze o testovac&iacute; a
prezentačn&iacute; web, tak se ve skutečnosti nejedn&aacute; o re&aacute;lnou živnost, tento smy&scaron;len&yacute;
e-shop nedisponuje ž&aacute;dn&yacute;mi re&aacute;ln&yacute;m zbož&iacute;m. Proto za zbož&iacute; neplaťte!</p>
';
    }

    public function getPageFourContent()
    {
        return '
<h1>Soubory cookie</h1>
<p>Vstupem na str&aacute;nky&nbsp;souhlas&iacute;te s instalac&iacute; souborů cookie a d&aacute;v&aacute;te
tak&eacute; souhlas s využit&iacute;m osobn&iacute;ch a dal&scaron;&iacute;ch &uacute;dajů,&nbsp;kter&eacute;
jsou v souboru cookie obsaženy.</p>
<p>Cookie jsou soubory, kter&eacute; ukl&aacute;daj&iacute; informace ve Va&scaron;em prohl&iacute;žeči a
slouž&iacute; k rozli&scaron;ov&aacute;n&iacute; jednotliv&yacute;ch uživatelů. Uživatel nen&iacute; na
z&aacute;kladě těchto informac&iacute;&nbsp;identifikovateln&yacute;. Použ&iacute;v&aacute;n&iacute; souborů cookie
si můžete nastavit ve Va&scaron;em internetov&eacute;m prohl&iacute;žeči. Lze je odm&iacute;tnout nebo nastavit
už&iacute;v&aacute;n&iacute; jen někter&yacute;ch.</p>
<p>Seznam souborů cookie na str&aacute;nk&aacute;ch:</p>
<ul>
	<li>Google Analytics</li>
</ul>
';
    }

    public function getPageFiveContent()
    {
        return '
<h1>Doprava a platba</h1>
<p><strong>Objedn&aacute;vky odes&iacute;l&aacute;me zpravidla do 3 pracovn&iacute;ch dn&iacute;</strong> od
objedn&aacute;n&iacute; (u platby předem od přijet&iacute; platby). V př&iacute;padě, že by byl term&iacute;n z
nějak&eacute;ho důvodu del&scaron;&iacute;, budeme V&aacute;s kontaktovat.</p>
<p>Česk&aacute; po&scaron;ta</p>
<ul>
	<li>platba předem 55&nbsp;Kč</li>
	<li>dob&iacute;rka 90&nbsp;Kč</li>
</ul>
<p>Uloženka</p>
<ul>
	<li>platba předem 50&nbsp;Kč</li>
	<li>dob&iacute;rka 85&nbsp;Kč</li>
</ul>
<p>Platba na &uacute;čet:&nbsp;0123456789/0123</p>
';
    }
}
