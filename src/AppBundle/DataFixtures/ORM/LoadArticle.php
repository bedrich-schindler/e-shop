<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Article;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadArticle extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $articleOne = new Article();
        $articleOne->setTitle('Upozornění pro kupující');
        $articleOne->setContent($this->getArticleOneContent());
        $articleOne->setIsDefault(true);
        $articleOne->setWrittenOn(new \DateTime());
        $articleOne->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($articleOne);

        $manager->flush();

        $this->addReference('article-one', $articleOne);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }

    public function getArticleOneContent()
    {
        return '
<p>Cel&yacute; obsah webu včetně obchodn&iacute;ch podm&iacute;nek je smy&scaron;len&yacute;. V&scaron;echny
proveden&eacute; objedn&aacute;vky budou využity k testovac&iacute;m a prezentačn&iacute;m &uacute;čelům
dlouhodob&eacute; maturitn&iacute; pr&aacute;ce. Jelikož se jedn&aacute; pouze o testovac&iacute; a
prezentačn&iacute; web, tak se ve skutečnosti nejedn&aacute; o re&aacute;lnou živnost, tento smy&scaron;len&yacute;
e-shop nedisponuje ž&aacute;dn&yacute;mi re&aacute;ln&yacute;m zbož&iacute;m. Proto za zbož&iacute; neplaťte!</p>
';
    }
}
