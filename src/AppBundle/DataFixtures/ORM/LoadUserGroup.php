<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\UserGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUserGroup extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new UserGroup('Uživatel');
        $user->addRole('ROLE_USER');

        $moderator = new UserGroup('Moderátor');
        $moderator->addRole('ROLE_ADMIN');

        $administrator = new UserGroup('Administrátor');
        $administrator->addRole('ROLE_SUPER_ADMIN');

        $manager->persist($user);
        $manager->persist($moderator);
        $manager->persist($administrator);

        $manager->flush();

        $this->addReference('user_group-user', $user);
        $this->addReference('user_group-moderator', $moderator);
        $this->addReference('user_group-administrator', $administrator);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
