<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Sorting;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSorting extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $sortingOne = new Sorting();
        $sortingOne->setSort(1);
        $sortingOne->setProductCategory($this->getReference('product_category-one'));

        $sortingTwo = new Sorting();
        $sortingTwo->setSort(2);
        $sortingTwo->setText($this->getReference('text-one'));

        $sortingThree = new Sorting();
        $sortingThree->setSort(3);
        $sortingThree->setProductCategory($this->getReference('product_category-two'));

        $sortingFour = new Sorting();
        $sortingFour->setSort(4);
        $sortingFour->setProductCategory($this->getReference('product_category-three'));

        $sortingFive = new Sorting();
        $sortingFive->setSort(5);
        $sortingFive->setProductCategory($this->getReference('product_category-four'));


        $manager->persist($sortingOne);
        $manager->persist($sortingTwo);
        $manager->persist($sortingThree);
        $manager->persist($sortingFour);
        $manager->persist($sortingFive);

        $manager->flush();

        $this->addReference('sorting-one', $sortingOne);
        $this->addReference('sorting-two', $sortingTwo);
        $this->addReference('sorting-three', $sortingThree);
        $this->addReference('sorting-four', $sortingFour);
        $this->addReference('sorting-five', $sortingFive);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 15;
    }
}
