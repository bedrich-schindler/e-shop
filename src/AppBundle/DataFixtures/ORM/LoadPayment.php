<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Payment;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPayment extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $paymentOne = new Payment();
        $paymentOne->setMethod('Dobírka');
        $paymentOne->setFee(35);

        $paymentTwo = new Payment();
        $paymentTwo->setMethod('Převodem');
        $paymentTwo->setFee(0);

        $manager->persist($paymentOne);
        $manager->persist($paymentTwo);

        $manager->flush();

        $this->addReference('payment-one', $paymentOne);
        $this->addReference('payment-two', $paymentTwo);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 10;
    }
}
