<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Delivery;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDelivery extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $deliveryOne = new Delivery();
        $deliveryOne->setMethod('Česká pošta - doporučená zásilka');
        $deliveryOne->setFee(55);

        $deliveryTwo = new Delivery();
        $deliveryTwo->setMethod('Uloženka');
        $deliveryTwo->setFormat('ulozenka');
        $deliveryTwo->setFee(50);

        $manager->persist($deliveryOne);
        $manager->persist($deliveryTwo);

        $manager->flush();

        $this->addReference('delivery-one', $deliveryOne);
        $this->addReference('delivery-two', $deliveryTwo);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9;
    }
}
