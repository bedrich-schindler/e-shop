<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUser extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $administrator = new User();
        $administrator->setFirstname('Bedřich');
        $administrator->setLastname('Schindler');
        $administrator->setEmail('bedrich.schindler@gmail.com');
        $administrator->setUsername('bedrich.schindler');
        $administrator->setPlainPassword('123456789');
        $administrator->setGroups($this->getReference('user_group-administrator'));
        $administrator->setRegisteredAt(new \DateTime());

        $administratorTwo = new User();
        $administratorTwo->setFirstname('Jiří');
        $administratorTwo->setLastname('Pliska');
        $administratorTwo->setEmail('jiri.pliska@pslib.cz');
        $administratorTwo->setUsername('jiri.pliska');
        $administratorTwo->setPlainPassword('123456789');
        $administratorTwo->setGroups($this->getReference('user_group-administrator'));
        $administratorTwo->setRegisteredAt(new \DateTime());


        $manager->persist($administrator);
        $manager->persist($administratorTwo);

        $manager->flush();

        $this->addReference('user-administrator', $administrator);
        $this->addReference('user-administrator-two', $administratorTwo);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
