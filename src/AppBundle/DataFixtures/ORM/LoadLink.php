<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Link;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadLink extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $linkOne = new Link();
        $linkOne->setTitle('Facebook');
        $linkOne->setLink('https://www.facebook.com/');
        $linkOne->setExtension('png');
        $linkOne->setWrittenOn(new \DateTime());
        $linkOne->setWrittenBy($this->getReference('user-administrator'));

        $linkTwo = new Link();
        $linkTwo->setTitle('Twitter');
        $linkTwo->setLink('https://www.twitter.com/');
        $linkTwo->setExtension('png');
        $linkTwo->setWrittenOn(new \DateTime());
        $linkTwo->setWrittenBy($this->getReference('user-administrator'));

        $linkThree = new Link();
        $linkThree->setTitle('Pinterest');
        $linkThree->setLink('https://www.pinterest.com/');
        $linkThree->setExtension('png');
        $linkThree->setWrittenOn(new \DateTime());
        $linkThree->setWrittenBy($this->getReference('user-administrator'));

        $linkFour = new Link();
        $linkFour->setTitle('Instagram');
        $linkFour->setLink('https://www.instragram.com/');
        $linkFour->setExtension('png');
        $linkFour->setWrittenOn(new \DateTime());
        $linkFour->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($linkOne);
        $manager->persist($linkTwo);
        $manager->persist($linkThree);
        $manager->persist($linkFour);

        $manager->flush();

        $this->addReference('link-one', $linkOne);
        $this->addReference('link-two', $linkTwo);
        $this->addReference('link-three', $linkThree);
        $this->addReference('link-four', $linkFour);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }
}
