<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ProductCategory;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProductCategory extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $productCategoryOne = new ProductCategory();
        $productCategoryOne->setTitle('Sady');
        $productCategoryOne->setAlias('sady');
        $productCategoryOne->setDescription(
            '<p>V&scaron;echny n&iacute;že uveden&eacute; sady n&aacute;dob&iacute; jsou k dost&aacute;n&iacute; v
            prodejn&aacute;ch <a href="http://www.ikea.com/">IKEA</a>. N&aacute;dob&iacute; t&eacute;to značky
            přin&aacute;&scaron;&iacute; do Va&scaron;ich domovů velmi kvalitn&iacute; n&aacute;dob&iacute;,
            kter&eacute; se bude l&iacute;bit jak V&aacute;m, tak Va&scaron;im př&aacute;telům. V&scaron;echny sady
            n&aacute;dob&iacute; obsahuj&iacute; vždy mělk&yacute; tal&iacute;ř, hlubok&yacute; tal&iacute;ř a
            dezertn&iacute; tal&iacute;ř, vždy v sadě po 6 kusech, Jednotliv&eacute; č&aacute;sti sady se
            daj&iacute; zakoupit tak&eacute; samostatně.</p>'
        );
        $productCategoryOne->setWrittenOn(new \DateTime());
        $productCategoryOne->setWrittenBy($this->getReference('user-administrator'));

        $productCategoryTwo = new ProductCategory();
        $productCategoryTwo->setTitle('Mísy');
        $productCategoryTwo->setAlias('misy');
        $productCategoryTwo->setDescription(
            '<p>V&scaron;echny n&iacute;že uveden&eacute; sady n&aacute;dob&iacute; jsou k dost&aacute;n&iacute; v
            prodejn&aacute;ch <a href="http://www.ikea.com/">IKEA</a>. N&aacute;dob&iacute; t&eacute;to značky
            přin&aacute;&scaron;&iacute; do Va&scaron;ich domovů velmi kvalitn&iacute; n&aacute;dob&iacute;,
            kter&eacute; se bude l&iacute;bit jak V&aacute;m, tak Va&scaron;im př&aacute;telům. V&scaron;echny
            m&iacute;sy se prod&aacute;vaj&iacute; vždy po 1 kuse.</p>'
        );
        $productCategoryTwo->setWrittenOn(new \DateTime());
        $productCategoryTwo->setWrittenBy($this->getReference('user-administrator'));

        $productCategoryThree = new ProductCategory();
        $productCategoryThree->setTitle('Hluboké talíře');
        $productCategoryThree->setAlias('hluboke-talire');
        $productCategoryThree->setDescription(
            '<p>V&scaron;echny n&iacute;že uveden&eacute; sady n&aacute;dob&iacute; jsou k dost&aacute;n&iacute; v
            prodejn&aacute;ch <a href="http://www.ikea.com/">IKEA</a>. N&aacute;dob&iacute; t&eacute;to značky
            přin&aacute;&scaron;&iacute; do Va&scaron;ich domovů velmi kvalitn&iacute; n&aacute;dob&iacute;,
            kter&eacute; se bude l&iacute;bit jak V&aacute;m, tak Va&scaron;im př&aacute;telům. V&scaron;echny
            hlubok&eacute; se prod&aacute;vaj&iacute; vždy po 1 kuse.</p>'
        );
        $productCategoryThree->setWrittenOn(new \DateTime());
        $productCategoryThree->setWrittenBy($this->getReference('user-administrator'));

        $productCategoryFour = new ProductCategory();
        $productCategoryFour->setTitle('Mělké talíře');
        $productCategoryFour->setAlias('melke-talire');
        $productCategoryFour->setDescription(
            '<p>V&scaron;echny n&iacute;že uveden&eacute; sady n&aacute;dob&iacute; jsou k dost&aacute;n&iacute; v
            prodejn&aacute;ch <a href="http://www.ikea.com/">IKEA</a>. N&aacute;dob&iacute; t&eacute;to značky
            přin&aacute;&scaron;&iacute; do Va&scaron;ich domovů velmi kvalitn&iacute; n&aacute;dob&iacute;,
            kter&eacute; se bude l&iacute;bit jak V&aacute;m, tak Va&scaron;im př&aacute;telům. V&scaron;echny
            mělk&eacute; tal&iacute;ře&nbsp;se prod&aacute;vaj&iacute; vždy po 1 kuse.</p>'
        );
        $productCategoryFour->setWrittenOn(new \DateTime());
        $productCategoryFour->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($productCategoryOne);
        $manager->persist($productCategoryTwo);
        $manager->persist($productCategoryThree);
        $manager->persist($productCategoryFour);
        $manager->flush();

        $this->addReference('product_category-one', $productCategoryOne);
        $this->addReference('product_category-two', $productCategoryTwo);
        $this->addReference('product_category-three', $productCategoryThree);
        $this->addReference('product_category-four', $productCategoryFour);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 12;
    }
}
