<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\GlobalConfiguration;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGlobalConfiguration extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $globalConfiguration = new GlobalConfiguration();

        $globalConfiguration->setId(1);
        $globalConfiguration->setInvoiceName('Jan Novák');
        $globalConfiguration->setInvoiceStreet('Obecná 123');
        $globalConfiguration->setInvoiceCity('Nové město');
        $globalConfiguration->setInvoiceZipCode('45678');
        $globalConfiguration->setInvoiceIc('12345678');
        $globalConfiguration->setInvoiceBank('0123456789/0123');
        $globalConfiguration->setInvoiceText('Zapsán v živnostenském rejstříku MÚ Nové Město');
        $globalConfiguration->setInvoicePhone('+420 987 654 321');
        $globalConfiguration->setInvoiceEmail('online-shop@bsnet.cz');

        $manager->persist($globalConfiguration);
        $manager->flush();

        $this->addReference('global_configuration-one', $globalConfiguration);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
