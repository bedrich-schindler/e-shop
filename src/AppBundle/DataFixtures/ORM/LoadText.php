<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Text;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadText extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $textOne = new Text();
        $textOne->setTitle('Zboží z IKEA');
        $textOne->setContent(
            'Tento e-shop nabízí nádobí firmy IKEA pocházející ze Švédska, která nabízí tu
            nejvyšší možnou kvalitu, jak si můžete doma běžně dopřát.'
        );
        $textOne->setWrittenOn(new \DateTime());
        $textOne->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($textOne);

        $manager->flush();

        $this->addReference('text-one', $textOne);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
