<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Discount;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDiscount extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $discountOne = new Discount();
        $discountOne->setCode('ONLINESHOP50');
        $discountOne->setDiscount(50);
        $discountOne->setCurrentUse(0);
        $discountOne->setMaximumUse(10);
        $discountOne->setWrittenOn(new \DateTime());
        $discountOne->setWrittenBy($this->getReference('user-administrator'));

        $manager->persist($discountOne);

        $manager->flush();

        $this->addReference('discount-one', $discountOne);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 11;
    }
}
