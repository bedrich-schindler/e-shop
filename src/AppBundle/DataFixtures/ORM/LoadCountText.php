<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CountText;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCountText extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $countTextOne = new CountText();
        $countTextOne->setTitle('ks');

        $countTextTwo = new CountText();
        $countTextTwo->setTitle('pár');

        $countTextThree = new CountText();
        $countTextThree->setTitle('');

        $manager->persist($countTextOne);
        $manager->persist($countTextTwo);
        $manager->persist($countTextThree);

        $manager->flush();

        $this->addReference('count_text-one', $countTextOne);
        $this->addReference('count_text-two', $countTextTwo);
        $this->addReference('count_text-three', $countTextThree);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }
}
