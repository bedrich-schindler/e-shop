#!/bin/bash

sudo composer install
sudo npm update
sudo bower update --allow-root
sudo grunt

mkdir -p web/upload/files/invoices/
mkdir -p web/upload/files/share/
mkdir -p web/upload/images/header/
mkdir -p web/upload/images/links/
mkdir -p web/upload/images/products/
mkdir -p web/upload/images/texts/

sudo chmod -R 777 app/config/parameters.yml
sudo chmod -R 777 var
sudo chmod -R 777 web
